<?php

include_once __DIR__ . '/bootstrap.php';

include_once __DIR__ . '/views/partials/header.php';
?>

<div id="wrapper">
    <div id="container" class="text-center">
        <h1>404 Page Not Found</h1>

        <h3>
            Click <a href="<?php echo url('/'); ?>">here</a> to go home.
        </h3>
    </div>
</div>

<?php include_once __DIR__ . '/views/partials/footer.php'; ?>
