<?php


class FixedValue {

    private static $fallbackLanguage = 'en_GB';
    private static $translations = [
        'uk' => [
            'en_GB' => [
                'country' => 'UK',
                'city' => 'London',
                'readMore' => 'Read more',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Accessibility',
                'legals' => 'Privacy Notice',
                'siteMap' => 'Site&nbsp;Map',
                'allRightsReserved' => 'All rights reserved',
                'getInTouch' => 'Want to hear<br/>how our services',
                'getInTouch2' => 'can work for you?',
                'getInTouch3' => 'Get in touch',
                'summary' => 'Summary',
                'challenge' => 'Challenge',
                'strategy' => 'Strategy',
                'results' => 'Results',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiCom',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Work with us','Join the team']
            ],
        ],
        'se' => [
            'en_GB' => [
                'country' => 'SVERIGE',
                'city' => 'Stockholm',
                'readMore' => 'Read more',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Accessibility',
                'legals' => 'Privacy Notice',
                'siteMap' => 'Site&nbsp;Map',
                'allRightsReserved' => 'All rights reserved',
                'getInTouch' => 'Want to hear<br/>how our services',
                'getInTouch2' => 'can work for you?',
                'getInTouch3' => 'Get in touch',
                'summary' => 'Summary',
                'challenge' => 'Challenge',
                'strategy' => 'Strategy',
                'results' => 'Results',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiComSTHLM',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Work with us','Join the team']
            ],
            'sv_SE' => [
                'country' => 'SVERIGE',
                'city' => 'Stockholm',
                'readMore' => 'Läs mer',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Tillgänglighet',
                'legals' => 'Juridisk information',
                'siteMap' => 'Sajtkarta',
                'allRightsReserved' => 'All rights reserved',
                'getInTouch' => 'Vill du veta <br />hur våra tjänster',
                'getInTouch2' => 'kan hjälpa dig?',
                'getInTouch3' => 'Get in touch',
                'summary' => 'Sammanfattning',
                'challenge' => 'Utmaning',
                'strategy' => 'Strategi',
                'results' => 'Utfall',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiComSTHLM',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Jobba med oss','Anslut till vårt team']
            ],
        ],
        'fr' => [
            'en_GB' => [
                'country' => 'FRANCE',
                'city' => 'Paris',
                'readMore' => 'Read more',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Accessibility',
                'legals' => 'Privacy Notice',
                'siteMap' => 'Site&nbsp;Map',
                'allRightsReserved' => 'All rights reserved',
                'getInTouch' => 'Want to hear<br/>how our services',
                'getInTouch2' => 'can work for you?',
                'getInTouch3' => 'Get in touch',
                'summary' => 'Summary',
                'challenge' => 'Challenge',
                'strategy' => 'Strategy',
                'results' => 'Results',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiComFR',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Work with us','Join the team']
            ],
            'fr_FR' => [
                'country' => 'FRANCE',
                'city' => 'Paris',
                'readMore' => 'Lire la suite',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Accessibilité',
                'legals' => 'Mentions légales',
                'siteMap' => 'Plan du site web',
                'allRightsReserved' => 'Tous droits réservés',
                'getInTouch' => 'Vous souhaitez savoir<br/>comment nous',
                'getInTouch2' => 'pouvons vous aider?',
                'getInTouch3' => 'Contactez-nous',
                'summary' => 'Résumé',
                'challenge' => 'Contexte',
                'strategy' => 'Stratégie',
                'results' => 'Résultats',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiComFR',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Contact','Rejoignez-nous']
            ],
        ],
        'de' => [
            'en_GB' => [
                'country' => 'GERMANY',
                'city' => 'Munich',
                'readMore' => 'Read more',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Accessibility',
                'legals' => 'Privacy Notice',
                'siteMap' => 'Site&nbsp;Map',
                'allRightsReserved' => 'All rights reserved',
                'getInTouch' => 'Want to hear<br/>how our services',
                'getInTouch2' => 'can work for you?',
                'getInTouch3' => 'Get in touch',
                'summary' => 'Summary',
                'challenge' => 'Challenge',
                'strategy' => 'Strategy',
                'results' => 'Results',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/AxiComGmbH',
                'twitterLink' => 'https://twitter.com/AxiComGmbH',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Work with us','Join the team']
            ],
            'de_DE' => [
                'country' => 'DEUTSCHLAND',
                'city' => 'München',
                'readMore' => 'Lesen Sie mehr',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Barrierefreiheit',
                'legals' => 'Gesetzliche Bestimmungen',
                'siteMap' => 'Sitemap',
                'allRightsReserved' => 'Alle Rechte vorbehalten',
                'getInTouch' => 'Sie möchten wissen, <br/>wie wir Sie ',
                'getInTouch2' => 'unterstützen können?',
                'getInTouch3' => 'Kontaktieren Sie uns!',
                'summary' => 'Zusammenfassung',
                'challenge' => 'Herausforderung',
                'strategy' => 'Strategie',
                'results' => 'Ergebnisse',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/AxiComGmbH',
                'twitterLink' => 'https://twitter.com/AxiComGmbH',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Arbeiten Sie mit uns','Werden Sie Teil des Teams']
            ],
        ],
        'it' => [
            'en_GB' => [
                'country' => 'Italy',
                'city' => 'Milan',
                'readMore' => 'Read more',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Accessibility',
                'legals' => 'Privacy Notice',
                'siteMap' => 'Site&nbsp;Map',
                'allRightsReserved' => 'All rights reserved',
                'getInTouch' => 'Want to hear<br/>how our services',
                'getInTouch2' => 'can work for you?',
                'getInTouch3' => 'Get in touch',
                'summary' => 'Summary',
                'challenge' => 'Challenge',
                'strategy' => 'Strategy',
                'results' => 'Results',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiCom_Italy',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Work with us','Join the team']
            ],
            'it_IT' => [
                'country' => 'Italia',
                'city' => 'Milano',
                'readMore' => 'Leggi di più',
                'privacyAndCookies' => 'Cookie',
                'accessibility' => 'Accessibilità',
                'legals' => 'Informazioni legali',
                'siteMap' => 'Mappa del sito',
                'allRightsReserved' => 'Tutti i diritti riservati',
                'getInTouch' => 'Vuoi scoprire <br/>come possiamo',
                'getInTouch2' => 'supportarti?',
                'getInTouch3' => 'Contattaci',
                'summary' => 'Introduzione',
                'challenge' => 'La sfida',
                'strategy' => 'La strategia',
                'results' => 'I risultati',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiCom_Italy',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Lavora con noi','Entra nel team']
            ],
        ],
        'nl' => [
            'en_GB' => [
                'country' => 'Netherlands',
                'city' => 'Amsterdam',
                'readMore' => 'Read more',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Accessibility',
                'legals' => 'Privacy Notice',
                'siteMap' => 'Site&nbsp;Map',
                'allRightsReserved' => 'All rights reserved',
                'getInTouch' => 'Want to hear<br/>how our services',
                'getInTouch2' => 'can work for you?',
                'getInTouch3' => 'Get in touch',
                'summary' => 'Summary',
                'challenge' => 'Challenge',
                'strategy' => 'Strategy',
                'results' => 'Results',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiComBNL',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Work with us','Join the team']
            ],
            'nl_NL' => [
                'country' => 'Netherlands',
                'city' => 'Amsterdam',
                'readMore' => 'Lees meer',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Toegankelijkheid',
                'legals' => 'Algemene voorwaarden',
                'siteMap' => 'Site map',
                'allRightsReserved' => 'Alle rechten voorbehouden',
                'getInTouch' => 'Wilt u weten<br/>hoe wij u verder',
                'getInTouch2' => 'kunnen helpen?',
                'getInTouch3' => 'Get in touch',
                'summary' => 'Samenvatting',
                'challenge' => 'Uitdaging',
                'strategy' => 'Strategie',
                'results' => 'Resultaten',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiComBNL',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Werk met ons samen','Sluit je aan bij het team']
            ],
        ],
        'es' => [
            'en_GB' => [
                'country' => 'Spain',
                'city' => '',
                'readMore' => 'Read more',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Accessibility',
                'legals' => 'Privacy Notice',
                'siteMap' => 'Site&nbsp;Map',
                'allRightsReserved' => 'All rights reserved',
                'getInTouch' => 'Want to hear<br/>how our services',
                'getInTouch2' => 'can work for you?',
                'getInTouch3' => 'Get in touch',
                'summary' => 'Summary',
                'challenge' => 'Challenge',
                'strategy' => 'Strategy',
                'results' => 'Results',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiComSpain',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Work with us','Join the team']
            ],
            'es_ES' => [
                'country' => 'España',
                'city' => '',
                'readMore' => 'Leer más',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Accesibilidad',
                'legals' => 'Aviso Legal y Politica de Privacidad',
                'siteMap' => 'Mapa Web',
                'allRightsReserved' => 'Todos los derechos reservados',
                'getInTouch' => '¿Quieres saber cómo<br/>pueden ayudarte',
                'getInTouch2' => 'nuestros servicios?',
                'getInTouch3' => 'Contáctanos',
                'summary' => 'Resumen',
                'challenge' => 'Reto',
                'strategy' => 'Estrategia',
                'results' => 'Resultados',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiComSpain',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Trabaja con nosotros','Únete a nosotros']
            ],
        ],
        'us' => [
            'en_US' => [
//                'country' => 'United States',
//                'city' => 'New York, Los Angeles, Austin, San Francisco',
                'readMore' => 'Read more',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Accessibility',
                'legals' => 'Privacy Notice',
                'siteMap' => 'Site&nbsp;Map',
                'allRightsReserved' => 'All rights reserved',
                'getInTouch' => 'Want to hear<br/>how our services',
                'getInTouch2' => 'can work for you?',
                'getInTouch3' => 'Get in touch',
                'summary' => 'Summary',
                'challenge' => 'Challenge',
                'strategy' => 'Strategy',
                'results' => 'Results',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiCom',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Work with us','Join the team']
            ],
            'en_GB' => [ // for fallback
//                'country' => 'United States',
//                'city' => 'New York, Los Angeles, Austin, San Francisco',
                'readMore' => 'Read more',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Accessibility',
                'legals' => 'Privacy Notice',
                'siteMap' => 'Site&nbsp;Map',
                'allRightsReserved' => 'All rights reserved',
                'getInTouch' => 'Want to hear<br/>how our services',
                'getInTouch2' => 'can work for you?',
                'getInTouch3' => 'Get in touch',
                'summary' => 'Summary',
                'challenge' => 'Challenge',
                'strategy' => 'Strategy',
                'results' => 'Results',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiCom',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Work with us','Join the team']
            ],
        ],
        'mx' => [
            'en_GB' => [
                'country' => 'Mexico',
                'city' => 'Mexico City',
                'readMore' => 'Read more',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Accessibility',
                'legals' => 'Privacy Notice',
                'siteMap' => 'Site&nbsp;Map',
                'allRightsReserved' => 'All rights reserved',
                'getInTouch' => 'Want to hear<br/>how our services',
                'getInTouch2' => 'can work for you?',
                'getInTouch3' => 'Get in touch',
                'summary' => 'Summary',
                'challenge' => 'Challenge',
                'strategy' => 'Strategy',
                'results' => 'Results',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiComSpain',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Work with us','Join the team']
            ],
            'es_MX' => [
                'country' => 'Mexico',
                'city' => 'Mexico City',
                'readMore' => 'Leer más',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Accesibilidad',
                'legals' => 'Legal',
                'siteMap' => 'Mapa Web',
                'allRightsReserved' => 'Todos los derechos reservados',
                'getInTouch' => '¿Quieres saber cómo<br/>pueden ayudarte',
                'getInTouch2' => 'nuestros servicios?',
                'getInTouch3' => 'Contáctanos',
                'summary' => 'Resumen',
                'challenge' => 'Reto',
                'strategy' => 'Estrategia',
                'results' => 'Resultados',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiComSpain',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Trabaja con nosotros','Únete a nosotros']
            ],
            'es_ES' => [ // for fallback
                'country' => 'Mexico',
                'city' => 'Mexico City',
                'readMore' => 'Leer más',
                'privacyAndCookies' => 'Cookies',
                'accessibility' => 'Accesibilidad',
                'legals' => 'Legal',
                'siteMap' => 'Mapa Web',
                'allRightsReserved' => 'Todos los derechos reservados',
                'getInTouch' => '¿Quieres saber cómo<br/>pueden ayudarte',
                'getInTouch2' => 'nuestros servicios?',
                'getInTouch3' => 'Contáctanos',
                'summary' => 'Resumen',
                'challenge' => 'Reto',
                'strategy' => 'Estrategia',
                'results' => 'Resultados',
                'downloadResource' => 'Download Resource',
                'facebookLink' => 'https://www.facebook.com/axicompr',
                'twitterLink' => 'https://twitter.com/AxiComSpain',
                'linkedinLink' => 'https://www.linkedin.com/company/axicom/',
                'instagramLink' => 'https://www.instagram.com/axicom/',
                'contactUsTitles' => ['Trabaja con nosotros','Únete a nosotros']
            ],
        ],
    ];


    public static function get($itemKey) {

        $sitePath = get_site()->path;
        $locale = get_locale();
        foreach(self::$translations as $siteName => $siteNameArray) {
            $siteUriEnd = "{$siteName}/";
            if (substr($sitePath, -strlen($siteUriEnd)) === $siteUriEnd) {
                // return here
                return isset($siteNameArray[$locale]) ? $siteNameArray[$locale][$itemKey] : $siteNameArray[self::$fallbackLanguage][$itemKey];
            }
        }

        // global site
        return self::$translations['uk'][self::$fallbackLanguage][$itemKey];
    }

}
