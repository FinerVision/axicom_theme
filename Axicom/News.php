<?php

class News
{

    public static function all()
    {
        return Post::all('news');
    }

    public static function getAllCategories($news_items = [])
    {
        $news_categories = [];

        foreach ($news_items as $news_item) {
            if (!isset($news_item['category'])) {
                continue;
            }

            foreach (explode(', ', $news_item['category']) as $category) {
                // Ignore blank category names as we already hard-code
                // this as a default "label" for the select input.
                if ($category === '') {
                    continue;
                }

                $news_categories[$category] = $category;
            }
        }

        return array_unique($news_categories);
    }

    public static function getCatTitles($postId, $asString = false)
    {
        $categories = get_the_category($postId);
        $titles = [];
        foreach ($categories as $cat) {
            array_push($titles, $cat->cat_name);
        }

        if ($asString) {
            return join(', ', $titles);
        }

        return $titles;
    }

    public static function getRelated($posts)
    {
        if (!is_array($posts)) return [];

        return array_map(function ($post) {
            $var = [
                'image' => get_field('image', $post->ID),
                'name' => $post->post_name,
                'title' => $post->post_title,
                'category' => static::getCatTitles($post->ID, true),
                'date' => $post->post_date
            ];

            return $var;
        }, $posts);
    }


    public static function masonryNewsItems()
    {
        $latestFeaturedPicked = false;
        $newsItems = [];
        $featuredNews = Post::getFeaturedNews();
        foreach (static::all() as $newsItem) {
            $author = isset($newsItem['author']) ? $newsItem['author'] : null;
            $newsPost = $newsItem['the_post'];
            $isFeatured = $newsPost->ID === $featuredNews['the_post']->ID;

            if (!$latestFeaturedPicked && $isFeatured) {
                $latestFeaturedPicked = true;
                continue;
            }

            array_push($newsItems, [
                'name' => $newsItem['the_post']->post_name,
                'author' => [
                    'name' => !is_object($author) ? null : $author->post_title
                ],
                'image' => isset($newsItem['image']) ? $newsItem['image'] : null,
                'title' => $newsPost->post_title,
                'category' => self::getCatTitles($newsItem['the_post']->ID, true),
                'date' => $newsPost->post_date,
                'content' => $newsPost->post_content,
                'relatedNews' => static::getRelated(isset($newsItem['related']) ? $newsItem['related'] : null)
            ]);
        }

        return $newsItems;
    }

    public static function masonryNewsItemsFilterBy($params)
    {
        $news = get_posts(array(
            'posts_per_page'	=> -1,
            'post_type'			=> 'news',
            's' => $params
        ));


        $items = [];
        foreach ($news as $post) {
            $fields = get_fields($post);
            $fields['the_post'] = $post;
            array_push($items, $fields);
        }

        $newsItems = [];
        foreach ($items as $newsItem) {

            $author = isset($newsItem['author']) ? $newsItem['author'] : null;
            $newsPost = $newsItem['the_post'];

            array_push($newsItems, [
                'name' => $newsItem['the_post']->post_name,
                'author' => [
                    'name' => !is_object($author) ? null : $author->post_title
                ],
                'image' => isset($newsItem['image']) ? $newsItem['image'] : null,
                'title' => $newsPost->post_title,
                'category' => self::getCatTitles($newsItem['the_post']->ID, true),
                'date' => $newsPost->post_date,
                'content' => $newsPost->post_content,
                'relatedNews' => static::getRelated(isset($newsItem['related']) ? $newsItem['related'] : null)
            ]);
        }

        return $newsItems;
    }
}
