<?php

class Field
{
    public static $fields;

    public static function get($path)
    {
        if ($path === '*') {
            return self::$fields;
        }

        $value = self::$fields;
        $keys = explode('.', $path);

        foreach ($keys as $key) {
            if ($key === '*') {
                return $value;
            }

            if (!isset($value[$key])) {
                $value = null;
                break;
            }

            $value = $value[$key];
        }

        return $value;
    }
}
