<?php

class Post
{
    public static function all($type, $page = 0, $limit = -1)
    {
        $items = [];
        $posts = get_posts([
            'post_type' => $type,
            'posts_per_page' => $limit,
            'page' => $page
        ]);

        foreach ($posts as $post) {
            $fields = get_fields($post);
            $fields['the_post'] = $post;
            array_push($items, $fields);
        }
        return $items;
    }

    public static function find($type, $ids, $limit = -1)
    {
        if($ids === NULL) return null;
        if (count($ids) === 0) {
            return null;
        }

        $items = [];
        $posts = get_posts([
            'post_type' => $type,
            'posts_per_page' => $limit,
            'post__in' => $ids,
            'orderby' => 'post__in',
        ]);

        foreach ($posts as $post) {
            $fields = get_fields($post);
            $fields['the_post'] = $post;
            array_push($items, $fields);
        }

        return $limit === 1 ? $items[0] : $items;
    }

    public static function findByPost($postId)
    {
        $post = get_post($postId);
        $fields = null;

        if ($post) {
            $fields = get_fields($post);
            $fields['the_post'] = $post;
        }

        return $fields;
    }

    public static function homeFields() {

        $result = get_posts([
            'name'        => 'home',
            'post_type'   => 'page',
            'numberposts' => 1
        ]);

        return get_fields($result->ID) ?: null;
    }

    public static function getFeaturedNews() {

        $result = get_posts([
            'name'        => 'home',
            'post_type'   => 'page',
            'numberposts' => 1
        ]);

        if (!count($result)) return null;

        $fields = get_fields($result->ID);

        if (!$fields || !$fields['featured_news']) return null;

        return Post::findByPost($fields['featured_news']);

    }
}
