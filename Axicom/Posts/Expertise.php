<?php

class Expertise
{
    public static function all()
    {
        $expertises = Post::all('expertise');

        foreach ($expertises as $index => $expertise) {
            $expertises[$index]['order'] = $index;
            $expertises[$index]['id'] = str_slug($expertise['title'], '_');
            $expertises[$index]['work_post'] = Post::findByPost($expertise['work']->ID);
            $person = $expertises[$index]['person'];
            if (!is_object($person) || is_null($person->ID)) {
                continue;
            }

            $postPerson = Post::findByPost($person->ID);

            $expertises[$index]['person'] = [
                'name' => $person->post_title,
                'image' => $postPerson['image'],
                'job' => $postPerson['title'],
            ];
        }

        if (count($expertises) > 1) {
            $expertises = array_chunk($expertises, ceil(count($expertises) / 2));
            $expertises[1] = array_reverse($expertises[1]);
            $expertises = array_merge($expertises[0], $expertises[1]);
        }

        return $expertises;
    }
}
