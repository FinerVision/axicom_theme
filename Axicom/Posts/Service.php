<?php

class Service
{
    public static function all()
    {
        $services = Post::all('service');

        foreach ($services as $index => $service) {

            $services[$index]['order'] = $index;
            $services[$index]['id'] = str_slug($service['title'], '_');
            $services[$index]['work_post'] = [];

            if($service['work'] && (is_array($service['work']))){
                $workIds = array_map(function ($office) {
                    return $office->ID;
                }, $service['work']);
                $services[$index]['work_post'] = Post::find('work',$workIds);
            }

            $person = $services[$index]['person'];

            if (!is_object($person) || is_null($person->ID)) {
                continue;
            }

            $postPerson = Post::findByPost($person->ID);

            $services[$index]['person'] = [
                'name' => $person->post_title,
                'image' => $postPerson['image'],
                'job' => $postPerson['title'],
            ];
        }

        if (count($services) > 1) {
            $services = array_chunk($services, ceil(count($services) / 2));
            $services[1] = array_reverse($services[1]);
            $services = array_merge($services[0], $services[1]);
        }

        return $services;
    }
}
