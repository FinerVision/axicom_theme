<?php

/**
 * Class Image
 *
 * refer to the attachment post type in wordpress system
 *
 * usage:
 *     Image::getSize($url, $size) for the specific size of the image url ($size can be one of 'thumbnail', 'medium', 'medium_large', 'large' or 'fullsize')
 *     Image::getObject($url) is to convert the whole image to be an object
 *
 */
class Image implements JsonSerializable {

    /**
     * Note : this caching is just to avoid the same database request to do twice in one http request,
     *        this won't be kept for more than one request
     */
    private static $cachedImageObjects;

    const IMPORT_IMAGE_URL = 'url';
    const IMPORT_IMAGE_ID = 'id';

    /**
     * @see https://codex.wordpress.org/Function_Reference/get_intermediate_image_sizes
     */
    const IMAGE_SIZES = 'thumbnail|medium|medium_large|large|fullsize';

    private $imageId;
    private $imageSet;
    private $imageMeta;

    public static function getAttachmentId($url) {
        $attachment_id = 0;
        $dir = wp_upload_dir();
        if ( false !== strpos( $url, $dir['baseurl'] . '/' ) ) { // Is URL in uploads directory?
            $file = basename( $url );
            $query_args = array(
                'post_type'   => 'attachment',
                'post_status' => 'inherit',
                'fields'      => 'ids',
                'meta_query'  => array(
                    array(
                        'value'   => $file,
                        'compare' => 'LIKE',
                        'key'     => '_wp_attachment_metadata',
                    ),
                )
            );
            $query = new WP_Query( $query_args );
            if ( $query->have_posts() ) {
                foreach ( $query->posts as $post_id ) {
                    $meta = wp_get_attachment_metadata( $post_id );
                    $original_file       = basename( $meta['file'] );
                    $cropped_image_files = wp_list_pluck( $meta['sizes'], 'file' );
                    if ( $original_file === $file || in_array( $file, $cropped_image_files ) ) {
                        $attachment_id = $post_id;
                        break;
                    }
                }
            }
        }
        return $attachment_id;
    }

    /**
     *
     * to get the different image size by an images URL
     *
     * @param $url
     * @param string $size, one of : thumbnail, medium, medium_large, large, fullsize
     * @return mixed
     *
     */
    public static function getImageWithSize($imageId, $size = 'thumbnail') {
        $imageDetails = wp_get_attachment_image_src($imageId, $size);
        return $imageDetails[0];
    }

    public static function getAttachment($imageId) {
        $imageSet = new stdClass();

        foreach(explode('|', self::IMAGE_SIZES) as $size) {
            $imageSet->$size = self::getImageWithSize($imageId, $size);
        }

        return $imageSet;
    }

    public static function getAttachmentMeta($imageId) {
        $imageMeta = new stdClass();

        foreach(explode('|', self::IMAGE_SIZES) as $size) {
            $imageMeta->$size = wp_get_attachment_metadata($imageId, $size);
        }

        return $imageMeta;
    }

    public static function getObject($url) {

        $imageObject = null;

        if(isset(self::$cachedImageObjects) && isset(self::$cachedImageObjects->url) && isset(self::$cachedImageObjects->url->$url)) {
            $imageObject = self::$cachedImageObjects->url->$url;
        } else {

            // store cache
            if (!isset(self::$cachedImageObjects)) {
                self::$cachedImageObjects = new stdClass();
            }
            if (!isset(self::$cachedImageObjects->url)) {
                self::$cachedImageObjects->url = new stdClass();
            }

            $imageObject = self::$cachedImageObjects->url->$url = new Image($url);
        }

        return $imageObject;
    }

    public static function getSize($url, $size = 'thumbnail') {
        if (!$url) {
            return null;
        }
        return self::getObject($url)->$size ?: $url;
    }

    public static function getSizeMeta($url, $size = 'thumbnail') {
        if (!$url) {
            return null;
        }

        $imageMeta = self::getObject($url)->imageMeta;

        return isset($imageMeta->$size) ? $imageMeta->$size : null;

    }

    public function __construct($reference, $importOption = self::IMPORT_IMAGE_URL) {

        $imageId = null;

        switch($importOption) {
            case self::IMPORT_IMAGE_URL:
                $imageId = self::getAttachmentId($reference);
                break;
            case self::IMPORT_IMAGE_ID:
                $imageId = $reference;
                break;
        }

        $this->imageId = $imageId;
        $this->imageSet = self::getAttachment($imageId);
        $this->imageMeta = self::getAttachmentMeta($imageId);

    }

    /**
     * @override
     * @return array
     */
    public function jsonSerialize() {
        return $this->imageSet;
    }

    public function __toString() {
        return htmlentities(json_encode($this->imageSet));
    }

    /**
     * note that nothing in the object is editable
     * @param $name
     * @param $value
     * @return null
     */
    public function __set($name, $value) {
        return null;
    }

    public function __get($name) {
        if (isset($this->imageSet->$name)) {
            return $this->imageSet->$name;
        }
        if (strtolower($name) === 'id') {
            return $this->imageId;
        }
        if (property_exists($this, $name)) {
            return $this->$name;
        }
        return null;
    }

}
