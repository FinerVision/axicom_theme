<?php

require_once __DIR__ . '/axicom.functions.php';
require_once __DIR__ . '/Axicom/Field.php';
require_once __DIR__ . '/Axicom/Post.php';
require_once __DIR__ . '/Axicom/News.php';
require_once __DIR__ . '/Axicom/Image.php';
require_once __DIR__ . '/Axicom/FixedValue.php';
require_once __DIR__ . '/Axicom/Posts/Service.php';
require_once __DIR__ . '/Axicom/Posts/Expertise.php';
include_once __DIR__ . '/globals.php';

global $mainMenu, $currentPage, $pages;

// MENUS -------------------------------------------------------------------
// global menu
$globalMenuItems = wp_get_nav_menu_items('global-menu');
$countries = [];
if ($globalMenuItems) {
    foreach ($globalMenuItems as $item) {
        $countries[] = (object)['title' => $item->title, 'url' => $item->url];
    }
}

// main menu
$mainMenuName = wp_get_nav_menu_name('main-menu');
$mainMenuItems = wp_get_nav_menu_items($mainMenuName);
$menuItems = [];

if (is_null($post) && isLocalHomePage()) {
    $post = getHomePage();
}

if ($mainMenuItems) {
    foreach ($mainMenuItems as $item) {
        $menuItems[] = (object)[
            'title' => $item->title,
            'url' => $item->url,
            'active' => $item->object_id == $post->ID
        ];
    }
}

$defaultPage = 'global';

$basePath = rtrim(dirname($_SERVER['SCRIPT_NAME']), '/\\');
$page = trim(str_replace($basePath, '', $_SERVER['REQUEST_URI']), '/');
// check if it has parameters
$page = explode('\?',$page)[0];
$page = explode('?',$page)[0];

if (strpos($page, '/') !== false) {
    $pageElements = explode('/', $page);
    array_shift($pageElements);
    $page = join('/', $pageElements);
}

// If the first part of the pathname contains "en", then override the $page variable.
$pathParts = explode('/', $page);

if (count($pathParts) > 0 && $pathParts[0] === 'en') {
    unset($pathParts[0]);
    $page = implode('/', $pathParts);
}

$currentPage = in_array($page, $pages) ? $page : $defaultPage;

if ($page != '' && $currentPage == 'global') {
    $currentPage = 'home';
}

$siteInfo = get_site(); // 1 is the Global page, >1 is a country page page
$pageName = $post->post_name; // this is the current page name related the post

if ((int)$siteInfo->blog_id == 1) {
    $mainMenu = $countries;
} else {
    $mainMenu = $menuItems;
}
