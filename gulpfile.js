const elixir = require('laravel-elixir');
const Task = elixir.Task;
const postcss = require('gulp-postcss');
const pxtorem = require('postcss-pxtorem');
require('laravel-elixir-browserify-official');

elixir.config.assetsPath = 'src';
elixir.config.css.autoprefix.options.browsers = ['last 15 versions'];

elixir.extend('postcss', () => {
    new Task('postcss', () => {
        return gulp.src('./public/css/app.css')
            .pipe(postcss([
                pxtorem({
                    rootValue: 14,
                    replace: false,
                    selectorBlackList: ['html'],
                    propList: ['*'],
                })
            ]))
            .pipe(gulp.dest('./public/css'));
    });
});


elixir(mix => {
    mix.sass('app.scss');
    mix.browserify('app.js');
    mix.postcss();
});
