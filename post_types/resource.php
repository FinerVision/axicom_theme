<?php

// Resource
/**
 * Register a custom post type called "resource".
 *
 * @see get_post_type_labels() for label keys.
 */
function resource_post_type_init() {
	$labels = array(
		'name'                  => _x( 'Resources', 'Resources', 'textdomain' ),
		'singular_name'         => _x( 'Resource', 'Resource', 'textdomain' ),
		'menu_name'             => _x( 'Resources', 'Resources', 'textdomain' ),
		'name_admin_bar'        => _x( 'Resources', 'Resources', 'textdomain' ),
		'add_new'               => __( 'Add New', 'textdomain' ),
		'add_new_item'          => __( 'Add New Resource', 'textdomain' ),
		'new_item'              => __( 'New Resource', 'textdomain' ),
		'edit_item'             => __( 'Edit Resource', 'textdomain' ),
		'view_item'             => __( 'View Resource', 'textdomain' ),
		'all_items'             => __( 'All Resources', 'textdomain' ),
		'search_items'          => __( 'Search Resources', 'textdomain' ),
		'parent_item_colon'     => __( 'Parent Resource:', 'textdomain' ),
		'not_found'             => __( 'No Resources found.', 'textdomain' ),
		'not_found_in_trash'    => __( 'No Resources found in Trash.', 'textdomain' ),
		'featured_image'        => _x( 'Resource Picture', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'set_featured_image'    => _x( 'Set Resource Picture', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'remove_featured_image' => _x( 'Remove Resource Picture', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'use_featured_image'    => _x( 'Use as Resource Image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'archives'              => _x( 'Resource archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
		'insert_into_item'      => _x( 'Insert into Resources', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this Resource', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
		'filter_items_list'     => _x( 'Filter Resources list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
		'items_list_navigation' => _x( 'Resources list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
		'items_list'            => _x( 'Resources list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'resource' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'thumbnail' ),
		'menu_icon'          => 'dashicons-media-spreadsheet'
	);

	register_post_type( 'Resource', $args );
}