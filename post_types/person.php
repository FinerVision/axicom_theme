<?php

// Person
/**
 * Register a custom post type called "person".
 *
 * @see get_post_type_labels() for label keys.
 */
function person_post_type_init() {
	$labels = array(
		'name'                  => _x( 'People', 'People', 'textdomain' ),
		'singular_name'         => _x( 'Person', 'Person', 'textdomain' ),
		'menu_name'             => _x( 'People', 'People', 'textdomain' ),
		'name_admin_bar'        => _x( 'People', 'People', 'textdomain' ),
		'add_new'               => __( 'Add New', 'textdomain' ),
		'add_new_item'          => __( 'Add New Person', 'textdomain' ),
		'new_item'              => __( 'New Person', 'textdomain' ),
		'edit_item'             => __( 'Edit Person', 'textdomain' ),
		'view_item'             => __( 'View Person', 'textdomain' ),
		'all_items'             => __( 'All People', 'textdomain' ),
		'search_items'          => __( 'Search People', 'textdomain' ),
		'parent_item_colon'     => __( 'Parent Person:', 'textdomain' ),
		'not_found'             => __( 'No People found.', 'textdomain' ),
		'not_found_in_trash'    => __( 'No People found in Trash.', 'textdomain' ),
		'featured_image'        => _x( 'Person Picture', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'set_featured_image'    => _x( 'Set Person Picture', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'remove_featured_image' => _x( 'Remove Person Picture', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'use_featured_image'    => _x( 'Use as Person Image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'archives'              => _x( 'Person archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
		'insert_into_item'      => _x( 'Insert into People', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this Person', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
		'filter_items_list'     => _x( 'Filter People list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
		'items_list_navigation' => _x( 'People list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
		'items_list'            => _x( 'People list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'person' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title' ),
		'menu_icon'          => 'dashicons-universal-access'
	);

	register_post_type( 'Person', $args );
}