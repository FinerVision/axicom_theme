<?php

require_once __DIR__ . '/person.php';
require_once __DIR__ . '/news.php';
require_once __DIR__ . '/expertise.php';
require_once __DIR__ . '/service.php';
require_once __DIR__ . '/office.php';
require_once __DIR__ . '/work.php';
require_once __DIR__ . '/resource.php';

add_action( 'init', 'resource_post_type_init' );
add_action( 'init', 'news_post_type_init' );
add_action( 'init', 'work_post_type_init' );
add_action( 'init', 'expertise_post_type_init' );
add_action( 'init', 'service_post_type_init' );
add_action( 'init', 'person_post_type_init' );
add_action( 'init', 'office_post_type_init' );

add_theme_support('post-thumbnails', ['post', 'expertise', 'service', 'work', 'news']);

add_filter('enter_title_here', 'wpb_change_title_text');

function wpb_change_title_text($title)
{
    $screen = get_current_screen();

    if ('person' == $screen->post_type) {
        $title = 'Person name';
    }

    if ('expertise' == $screen->post_type) {
        $title = 'Expertise name';
    }

    if ('service' == $screen->post_type) {
        $title = 'Service name';
    }

    if ('office' == $screen->post_type) {
        $title = 'Office name';
    }

    if ('work' == $screen->post_type) {
        $title = 'Work title';
    }

    return $title;
}

