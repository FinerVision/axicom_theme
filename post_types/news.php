<?php

// News
/**
 * Register a custom post type called "news".
 *
 * @see get_post_type_labels() for label keys.
 */
function news_post_type_init() {
	$labels = array(
		'name'                  => _x( 'News', 'News', 'textdomain' ),
		'singular_name'         => _x( 'News', 'News', 'textdomain' ),
		'menu_name'             => _x( 'News', 'News', 'textdomain' ),
		'name_admin_bar'        => _x( 'News', 'News', 'textdomain' ),
		'add_new'               => __( 'Add New', 'textdomain' ),
		'add_new_item'          => __( 'Add New News', 'textdomain' ),
		'new_item'              => __( 'New News', 'textdomain' ),
		'edit_item'             => __( 'Edit News', 'textdomain' ),
		'view_item'             => __( 'View News', 'textdomain' ),
		'all_items'             => __( 'All News', 'textdomain' ),
		'search_items'          => __( 'Search News', 'textdomain' ),
		'parent_item_colon'     => __( 'Parent News:', 'textdomain' ),
		'not_found'             => __( 'No News found.', 'textdomain' ),
		'not_found_in_trash'    => __( 'No News found in Trash.', 'textdomain' ),
		'featured_image'        => _x( 'News Picture', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'set_featured_image'    => _x( 'Set News Picture', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'remove_featured_image' => _x( 'Remove News Picture', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'use_featured_image'    => _x( 'Use as News Image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'archives'              => _x( 'News archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
		'insert_into_item'      => _x( 'Insert into News', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this News', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
		'filter_items_list'     => _x( 'Filter News list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
		'items_list_navigation' => _x( 'News list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
		'items_list'            => _x( 'News list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'news' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
        'taxonomies'         => array( 'category' ),
		'supports'           => array( 'title', 'editor' ),
		'menu_icon'          => 'dashicons-megaphone'
	);

	register_post_type( 'News', $args );
}