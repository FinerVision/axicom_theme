<?php

// Expertise
/**
 * Register a custom post type called "expertise". => But it shows itself as Sector!!!!
 *
 * @see get_post_type_labels() for label keys.
 */
function expertise_post_type_init() {
	$labels = array(
		'name'                  => _x( 'Sectors', 'Sectors', 'textdomain' ),
		'singular_name'         => _x( 'Sector', 'Sector', 'textdomain' ),
		'menu_name'             => _x( 'Sectors', 'Sectors', 'textdomain' ),
		'name_admin_bar'        => _x( 'Sectors', 'Sectors', 'textdomain' ),
		'add_new'               => __( 'Add New', 'textdomain' ),
		'add_new_item'          => __( 'Add New Sector', 'textdomain' ),
		'new_item'              => __( 'New Sector', 'textdomain' ),
		'edit_item'             => __( 'Edit Sector', 'textdomain' ),
		'view_item'             => __( 'View Sector', 'textdomain' ),
		'all_items'             => __( 'All Sectors', 'textdomain' ),
		'search_items'          => __( 'Search Sectors', 'textdomain' ),
		'parent_item_colon'     => __( 'Parent Sector:', 'textdomain' ),
		'not_found'             => __( 'No Sectors found.', 'textdomain' ),
		'not_found_in_trash'    => __( 'No Sectors found in Trash.', 'textdomain' ),
		'featured_image'        => _x( 'Sector Picture', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'set_featured_image'    => _x( 'Set Sector Picture', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'remove_featured_image' => _x( 'Remove Sector Picture', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'use_featured_image'    => _x( 'Use as Sector Image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'archives'              => _x( 'Sector archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
		'insert_into_item'      => _x( 'Insert into Sectors', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this Sector', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
		'filter_items_list'     => _x( 'Filter Sectors list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
		'items_list_navigation' => _x( 'Sectors list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
		'items_list'            => _x( 'Sectors list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'expertise' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'thumbnail' ),
		'menu_icon'          => 'dashicons-image-filter'
	);

	register_post_type( 'Expertise', $args );
}