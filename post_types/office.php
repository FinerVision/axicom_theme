<?php

// Office
/**
 * Register a custom post type called "office".
 *
 * @see get_post_type_labels() for label keys.
 */
function office_post_type_init() {
	$labels = array(
		'name'                  => _x( 'Offices', 'Offices', 'textdomain' ),
		'singular_name'         => _x( 'Office', 'Office', 'textdomain' ),
		'menu_name'             => _x( 'Offices', 'Offices', 'textdomain' ),
		'name_admin_bar'        => _x( 'Offices', 'Offices', 'textdomain' ),
		'add_new'               => __( 'Add New', 'textdomain' ),
		'add_new_item'          => __( 'Add New Office', 'textdomain' ),
		'new_item'              => __( 'New Office', 'textdomain' ),
		'edit_item'             => __( 'Edit Office', 'textdomain' ),
		'view_item'             => __( 'View Office', 'textdomain' ),
		'all_items'             => __( 'All Offices', 'textdomain' ),
		'search_items'          => __( 'Search Offices', 'textdomain' ),
		'parent_item_colon'     => __( 'Parent Office:', 'textdomain' ),
		'not_found'             => __( 'No Offices found.', 'textdomain' ),
		'not_found_in_trash'    => __( 'No Offices found in Trash.', 'textdomain' ),
		'featured_image'        => _x( 'Office Picture', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'set_featured_image'    => _x( 'Set Office Picture', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'remove_featured_image' => _x( 'Remove Office Picture', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'use_featured_image'    => _x( 'Use as Office Image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
		'archives'              => _x( 'Office archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
		'insert_into_item'      => _x( 'Insert into Offices', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this Office', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
		'filter_items_list'     => _x( 'Filter Offices list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
		'items_list_navigation' => _x( 'Offices list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
		'items_list'            => _x( 'Offices list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'office' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title' ),
		'menu_icon'          => 'dashicons-admin-site'
	);

	register_post_type( 'Office', $args );
}