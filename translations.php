<?php
    $strings = [
//        example
        [
            'key'=>'footer privacy',
            'value'=>'Privacy & Cookies'
        ],
        [
            'key'=>'footer accessibility',
            'value'=>'Accessibility'
        ],
        [
            'key'=>'footer legals',
            'value'=>'Legals'
        ],
        [
            'key'=>'footer site map',
            'value'=>'Site map'
        ]
    ];

    foreach ($strings as $string) {
        if (function_exists('pll_register_string')) {
            pll_register_string($string['key'],$string['value']);
        }
    }

?>