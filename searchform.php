<form action="<?php echo site_url('/news'); ?>" method="get">
    <div class="SearchBox__container mt-14px">
        <input type="text" name="news_search" id="search" class="SearchBox__input" value="<?php the_search_query(); ?>" />
        <input type="image" alt="Search" class="SearchBox__button" src="<?php bloginfo( 'template_url' ); ?>/public/img/maginifier-overlay.png" />
    </div>
</form>