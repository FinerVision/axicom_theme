<?php

$countries = [
    (object)['page' => '', 'title' => 'France', 'url' => url('/home')],
    (object)['page' => '', 'title' => 'Germany', 'url' => url('/home')],
    (object)['page' => '', 'title' => 'Italy', 'url' => url('/home')],
    (object)['page' => '', 'title' => 'Netherlands', 'url' => url('/home')],
    (object)['page' => '', 'title' => 'Spain', 'url' => url('/home')],
    (object)['page' => '', 'title' => 'Sweden', 'url' => url('/home')],
    (object)['page' => '', 'title' => 'UK', 'url' => url('/home')],
    (object)['page' => '', 'title' => 'US', 'url' => url('/home')],
];

// TODO : check and remove this
$menuItems = [
    (object)['page' => 'home', 'title' => 'Home', 'url' => url('/home')],
    (object)['page' => 'about', 'title' => 'About', 'url' => url('/about')],
    (object)['page' => 'services', 'title' => 'Services', 'url' => url('/services')],
    (object)['page' => 'sectors', 'title' => 'Sectors', 'url' => url('/sectors')],
    (object)['page' => 'our-work', 'title' => 'Our Work', 'url' => url('/our-work')],
    (object)['page' => 'news', 'title' => 'News & Views', 'url' => url('/news')],
    (object)['page' => 'careers', 'title' => 'Careers', 'url' => url('/careers')],
    (object)['page' => 'contact-us', 'title' => 'Contact Us', 'url' => url('/contact-us')],
    (object)['page' => 'global', 'title' => 'Global', 'url' => url('/')],
];

$pages = [
    'about',
    'careers',
    'case-study',
    'contact-us',
    'sectors',
    'global',
    'news',
    'our-work',
    'services',
    'home',
    'cookie-policy',
    'accessibility',
    'legals',
    'site-map',
    'resources'
];

$homePageIdMap = [

    // uk
    '2|en_GB' => 637,

    // se
    '3|en_GB' => 449,
    '3|sv_SE' => 485,

    // es
    '4|en_GB' => 455,
    '4|es_ES' => 487,

    // nl
    '5|en_GB' => 438,
    '5|nl_NL' => 468,

    // it
    '6|en_GB' => 441,
    '6|it_IT' => 472,

    // de
    '7|en_GB' => 468,
    '7|de_DE' => 1133,

    // fr
    '8|en_GB' => 458,
    '8|fr_FR' => 488,

    // us
    '9|en_GB' => 637,
    '9|en_US' => 637, // for fallback

    // es
    '10|en_GB' => 455,
    '10|es_ES' => 487,
    '10|es_MX' => 487,

];


$homePageUris = [
    '/uk/',
    '/uk/en/',
    '/uk/home/',
    '/uk/en/home/',
    '/se/',
    '/se/en/',
    '/se/home/',
    '/se/en/home/',
    '/es/',
    '/es/en/',
    '/es/home/',
    '/es/en/home/',
    '/nl/',
    '/nl/en/',
    '/nl/home/',
    '/nl/en/home/',
    '/it/',
    '/it/en/',
    '/it/home/',
    '/it/en/home/',
    '/de/',
    '/de/en/',
    '/de/home/',
    '/de/en/home/',
    '/fr/',
    '/fr/en/',
    '/fr/home/',
    '/fr/en/home/',
    '/us/',
    '/us/en/',
    '/us/home/',
    '/us/en/home/',
    '/mx/',
    '/mx/en/',
    '/mx/home/',
    '/mx/en/home/',
];



$services = Service::all();
$expertises = Expertise::all();
