<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="preload" as="script" href="<?php echo assetVersion('js/app.js'); ?>">

    <?php wp_head(); ?>

    <title><?php echo customTitle(' - '); ?></title>

    <meta charset="UTF-8">
    <meta name="author" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <?php
    if (inProduction()) { ?>
        <meta name="google-site-verification" content="e7pywRl1yVMXJulsSfq-c7kagAlK2GnIMbV0jsKCryQ" />
    <?php }
    ?>

    <?php echo getOgImageTag(); ?>


    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo asset('apple-icon-57x57.png'); ?>?v2">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo asset('apple-icon-60x60.png'); ?>?v2">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo asset('apple-icon-72x72.png'); ?>?v2">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo asset('apple-icon-76x76.png'); ?>?v2">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo asset('apple-icon-114x114.png'); ?>?v2">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo asset('apple-icon-120x120.png'); ?>?v2">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo asset('apple-icon-144x144.png'); ?>?v2">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo asset('apple-icon-152x152.png'); ?>?v2">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo asset('apple-icon-180x180.png'); ?>?v2">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo asset('android-icon-192x192.png'); ?>?v2">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo asset('favicon-32x32.png'); ?>?v2">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo asset('favicon-96x96.png'); ?>?v2">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo asset('favicon-16x16.png'); ?>?v2">
    <link rel="manifest" href="<?php echo asset('manifest.json'); ?>">

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
</head>

<body style="opacity: 0;">

<?php include __DIR__ . '/../components/preload-overlay.php' ?>

<noscript id="deferred-styles">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500,900" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo assetVersion('css/app.css'); ?>"/>
<!--    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />-->
</noscript>

<script>
    var loadDeferredStyles = function() {
        var addStylesNode = document.getElementById('deferred-styles');
        var replacement = document.createElement('div');
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement);
        addStylesNode.parentElement.removeChild(addStylesNode);

        setTimeout(function() {
            document.body.style.opacity = '1';
        }, 300);
    };
    var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
        window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
    if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
    else window.addEventListener('load', loadDeferredStyles);
</script>