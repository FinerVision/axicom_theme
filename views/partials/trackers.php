<?php if (inProduction()): ?>
    <script type="text/javascript">
        _linkedin_data_partner_id = "46293";
    </script>
    <script type="text/javascript">
        (function () {
            var s = document.getElementsByTagName("script")[0];
            var b = document.createElement("script");
            b.type = "text/javascript";
            b.async = true;
            b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
            s.parentNode.insertBefore(b, s);
        })();
    </script>
    <noscript>
        <img height="1" width="1" style="display:none;" alt=""
             src="https://dc.ads.linkedin.com/collect/?pid=46293&fmt=gif"/>
    </noscript>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-59354468-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'UA-59354468-1');
    </script>

    <!-- hubspot tracking code -->
    <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/2880038.js"></script>

<?php endif; ?>
