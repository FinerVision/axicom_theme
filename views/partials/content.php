<?php global $currentPage; ?>
<div id="wrapper" class="Page--<?php echo isLocalHomePage() ? 'home' : $currentPage; ?>">
    <?php
        // Only hubspot page hasn't get the nav.
        if ($currentPage !== 'hubspot') {
            include_once __DIR__ . '/../components/nav.php';
        }
    ?>

    <div id="container">
        <?php

        if (is_single()) { // 2. check if it is single page
            switch ($post->post_type) {
                case 'work' :
                    include_once __DIR__ . "/../pages/case-study.php";
                    break;
                case 'news' :

                    include_once __DIR__ . "/../pages/news/show-news-article.php";
                    break;
            };
        } else if (isLocalHomePage()) { // 3. check if it is local home page
            include_once __DIR__ . "/../pages/home.php";
        } else {
            Field::$fields = get_fields();
            include_once __DIR__ . "/../pages/{$currentPage}.php";
        }
        ?>
    </div>
</div>
