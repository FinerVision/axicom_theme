<?php

$slider_images = Field::get('hero_header.slider_images');
$hero_header = Field::get('hero_header');

?>

<section class="Carousel Carousel--hero">
    <div class="Carousel__slides">
        <?php if (!is_null($slider_images)): ?>
            <?php foreach ($slider_images as $index => $slider_image): ?>
                <div class="Carousel__slides__slide <?php echo $index === 0 ? 'active' : ''; ?>">
                    <div class="Carousel__slides__slide__img"
                         style="background-image: url(<?php echo Image::getSize($slider_image['image'], 'fullsize'); ?>)"></div>
                    <div class="Carousel__slides__slide__overlay"
                         style="background-image: url(<?php echo Image::getSize($slider_image['overlay'], 'fullsize'); ?>)"></div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</section>