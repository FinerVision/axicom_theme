<?php

Field::$fields = get_fields();
$slider_images = Field::get('hero_header.slider_images');

$preloadOverlayImages = [];

if (isset($slider_images) && $slider_images) {
    foreach (array_splice($slider_images, 0, 4) as $slider_image) {
        if (isset($slider_image['overlay']) && $slider_image['overlay']) {
            array_push($preloadOverlayImages, $slider_image['overlay']);
        }

        if (isset($slider_image['image']) && $slider_image['image']) {
            array_push($preloadOverlayImages, $slider_image['image']);
        }
    }
}

// if there isn't any images need to load, skip this
if (count($preloadOverlayImages) === 0) {
    return;
}

?>
<div class="PreloadOverlay">
    <div class="PreloadOverlay__inner">
        <div class="PreloadOverlay__logo" style="background-image: url(<?= asset('img/preloading-logo.jpg'); ?>);">
            <div class="PreloadOverlay__cover"></div>
        </div>
    </div>

    <script type="text/javascript">

        (function () {

            //
            window.pageHasPreloading = true;

            // constants
            var IMAGES = <?php echo json_encode($preloadOverlayImages); ?>;

            var root = document.querySelector('.PreloadOverlay');
            var preloadOverlay = {
                root: root,
                logo: root.querySelector('.PreloadOverlay__logo'),
                cover: root.querySelector('.PreloadOverlay__cover'),
            };
            var loadedImages = 0;
            var cachedImages = 0;
            var length = IMAGES.length;
            var i, image;
            var readyToFade = false;
            var faded = false;

            var triggerPreloadFinishEvent = function() {

                var event;
                if (window.CustomEvent) {
                    event = new CustomEvent('preloadfinish');
                } else {
                    event = document.createEvent('preloadfinish');
                    event.initCustomEvent('preloadfinish', true, true);
                }
                window.dispatchEvent && window.dispatchEvent(event);

            };

            var onAllImagesLoaded = function () {

                if (faded) {
                    return;
                }

                faded = true;

                triggerPreloadFinishEvent();

                preloadOverlay.root.style.opacity = '0';
                window.setTimeout(function () {
                    preloadOverlay.root.style.display = 'none';
                    root.parentNode.removeChild(root);
                }, 400);
            };

            var onImageLoaded = function () {

                // counting
                loadedImages++;
//            preloadOverlay.cover.style.left = (120 * loadedImages / length) + '%';

                if (loadedImages >= 1) {
                    preloadOverlay.cover.style.left = '120%';

                    window.setTimeout(function () {
                        readyToFade = true;
                        if (loadedImages === length) {
                            onAllImagesLoaded();
                        }
                    }, 1200); // 1.2 seconds

                }

                // when all the images are loaded
                if (loadedImages === length && readyToFade) {
                    onAllImagesLoaded();
                }
            };

            var checkImagesCaching = function () {
                if (cachedImages === length) {
                    triggerPreloadFinishEvent();
                    preloadOverlay.root.style.display = 'none';
                }
            };

            for (i = 0; i < length; i++) {

                image = new Image();
                image.src = IMAGES[i];

                <?php // check if the images are cached ?>
                if (image.complete || (image.width + image.height) > 0) {
                    cachedImages++;
                }

                image.onload = onImageLoaded;
            }
            checkImagesCaching();

        })();
    </script>
    <?php // these styles must be here ?>

    <style type="text/css">
        .PreloadOverlay {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 99999;
        <?php // make sure it's the highest, nothing else should be showing before the carousel images are loaded ?> background-color: #fff;
            -webkit-transition: opacity .8s;
            -o-transition: opacity .8s;
            transition: opacity .8s;
        }

        .PreloadOverlay__inner {
            position: absolute;
            bottom: 50%;
            width: 100%;
            text-align: center;
        }

        .PreloadOverlay__logo {
            display: inline-block;
            bottom: 50%;
            width: 326px;
            width: <?php echo 326 / 14 ?>rem;
        <?php // 14px = 1rem ?> max-width: 90%;
            background-size: contain;
            background-repeat: no-repeat;
            -webkit-transform: translateY(50%);
            -ms-transform: translateY(50%);
            transform: translateY(50%);
        }

        .PreloadOverlay__logo:before {
            content: '';
            position: relative;
            display: block;
            padding-top: <?= (114 / 1008) * 100 ?>%;
        <?php // calculate the image size's ratio ?>

        }

        .PreloadOverlay__cover {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: #fff;
            -webkit-transition: left 1.2s;
            -o-transition: left 1.2s;
            transition: left 1.2s;
        }

        .PreloadOverlay__cover:before {
            content: '';
            position: absolute;
            top: 0;
            right: 100%;
            width: 20%;
            height: 100%;
            background: transparent;
            background: -moz-linear-gradient(left, rgba(255, 255, 255, 0) 0%, rgba(255, 255, 255, 1) 100%);
            background: -webkit-linear-gradient(left, rgba(255, 255, 255, 0) 0%, rgba(255, 255, 255, 1) 100%);
            background: linear-gradient(to right, rgba(255, 255, 255, 0) 0%, rgba(255, 255, 255, 1) 100%);
        }
    </style>

</div>