<?php

$slider_images = Field::get('hero_header.slider_images');
$hero_header = Field::get('hero_header');

?>

<section class="HeaderCarousel">
<img src="<?php echo asset('/img/scroll-arrow.png')?>" class="Hero__scroll-arrow" style="z-index: 9999999">

    <div class="HeaderCarousel__wrapper">
        <div class="HeaderCarousel__button HeaderCarousel__button-prev"></div>

        <img src="<?php echo asset('/img/a-overlay-1v32.png')?>" class="HeaderCarousel__overlay"/>

        <div class="swiper-wrapper">

            <!-- Slides -->
            <div class="swiper-slide">
                <div class="HeaderCarousel__slide active">

                    <div class="content HeaderCarousel__content">
                        <div class="HeaderCarousel__message Hero__message--left text-left" style="text-align: left">
                            <img src="<?php echo asset('/img/axicom_svg_logo.svg')?>" class="Hero__logo img-responsive">

                            <h2 class="HeaderCarousel__title"><?php echo $hero_header['title'] ?></h2>
                            <h3 class="HeaderCarousel__sub_title"><?php echo $hero_header['subtitle']?></h3>

                        </div>

                    </div>
                    <div class="HeaderCarousel__img"
                         style="background-image: url(<?php echo $hero_header['image'] ?>)"></div>

                </div>
            </div>
            <?php if (!is_null($slider_images)): ?>
                <?php foreach ($slider_images as $index => $slider_image): ?>
                    <div class="swiper-slide">
                        <div class="HeaderCarousel__slide <?php echo $index === 0 ? 'active' : ''; ?>">

                            <div class="content HeaderCarousel__content">
                                <div class="HeaderCarousel__message Hero__message--left text-left" style="text-align: left">

                                        <h2 class="HeaderCarousel__title--type2"><?php echo $slider_image['title'] ?></h2>
                                        <div class="HeaderCarousel__sub_title--type2"><?php echo $slider_image['description'] ?></div>
                                    <?php if(!empty($slider_image['link'])):?>
                                    <a href="<?php echo $slider_image['link'] ?>" class="HeaderCarousel__read_more">
                                        <div class="HeaderCarousel__read_more_text">
                                            READ MORE
                                        </div>
                                    </a>
                                    <?php endif; ?>

                                </div>

                            </div>
                            <div class="HeaderCarousel__img"
                                 style="background-image: url(<?php echo Image::getSize($slider_image['image'], 'fullsize') ?>)"></div>

                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

        </div>
        <div class="HeaderCarousel__button HeaderCarousel__button-next"></div>

    </div>


</section>
