<?php

global $currentPage;

$our_works = Field::get('our_works');
$items = Post::find('work', $our_works['works'], 5);
?>

<?php if (!is_null($items) && (is_array($items) && count($items) > 0)): ?>



<?php if ($currentPage === 'global'): ?>
<div
    data-component="CaseStudyCarousel"
    data-props="<?php json([
        'title' => $our_works['title'],
        'items' => array_map(function($item) {
            return [
                'image' => Image::getSize($item['image'], 'medium_large'),
                'overlay' => $item['overlay'],
                'title' => $item['title'],
                'content' => $item['content'],
                'subtitle' => $item['subtitle'],
                'bannerHeaderTitle' => $item['head_title'],
                'remarkText' => 'To see more of our work visit the country sites',
            ];
        }, $items),
    ]); ?>"
></div>
<?php endif; ?>

<?php if ($currentPage === 'home'): ?>
<?php
    $homePageFields = Post::homeFields();
?>
<div
    data-component="CaseStudyCarousel"
    data-props="<?php json([
        'title' => $our_works['title'],
        'items' => array_map(function($item) use ($homePageFields) {
            return [
                'image' => Image::getSize($item['image'], 'medium_large'),
                'overlay' => $item['overlay'],
                'title' => $item['title'],
                'content' => $item['content'],
                'subtitle' => $item['subtitle'],
                'bannerHeaderTitle' => $item['head_title'],
                'link' => get_permalink($item['the_post']->ID),
                'remarkText' => $homePageFields['work_link_text'],
            ];
        }, $items),
        'readMoreText' => FixedValue::get('readMore'),
    ]); ?>"
></div>
<?php endif; ?>


<?php endif; ?>
