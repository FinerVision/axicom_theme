<?php

if (!isset($background_image)) {
    $background_image = Field::get('hero_parallax.background_image');
}

if (!isset($overlay_1)) {
    $overlay_1 = Field::get('hero_parallax.overlay_1');
}

if (!isset($overlay_2)) {
    $overlay_2 = Field::get('hero_parallax.overlay_2');
}

if (!isset($scroll_arrow)) {
    $scroll_arrow = Field::get('hero_parallax.scroll_arrow');
}

if (!isset($hero_parallax_title)) {
    $hero_parallax_title = Field::get('hero_parallax.title');
}

if (!isset($subtitle)) {
    $subtitle = Field::get('hero_parallax.subtitle');
}


?>

<div class="Header">
    <?php if (isset($background_image)): ?>
        <div class="HeroParallaxImageSafariFix" style="background-image: url(<?php echo Image::getSize($background_image, 'fullsize'); ?>);"></div>
    <?php endif; ?>
    <div class="HeroParallax">
        <?php if (isset($background_image)): ?>
            <div class="HeroParallax__background-image" style="background-image: url(<?php echo Image::getSize($background_image, 'fullsize'); ?>);"></div>
        <?php endif; ?>
        <?php if (isset($overlay_1)): ?>
            <div class="HeroParallax__overlay-1" style="background-image: url(<?php echo Image::getSize($overlay_1, 'fullsize'); ?>);opacity:0.8"></div>
        <?php endif; ?>
        <?php if (isset($overlay_2)): ?>
            <div class="HeroParallax__overlay-2" style="background-image: url(<?php echo Image::getSize($overlay_2, 'fullsize'); ?>);opacity:0.8"></div>
        <?php endif; ?>

        <div class="HeroParallax__inner">
            <div class="container-fluid">
                <div class="content">
                    <?php if (isset($hero_parallax_title)): ?>
                        <span class="HeroParallax__subtitle"><?php echo $hero_parallax_title; ?></span>
                        <br/>
                    <?php endif; ?>

                    <?php if (isset($subtitle)): ?>
                        <h3 class="text-black"><?php echo $subtitle; ?></h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php if (isset($scroll_arrow)): ?>
            <img src="<?php echo Image::getSize($scroll_arrow, 'medium'); ?>" class="HeroParallax__scroll-arrow"/>
        <?php endif; ?>
    </div>
</div>
