<?php

global $mainMenu, $currentPage, $siteInfo;

$translations = pll_the_languages(['raw' => 1]);

?>

<nav class="Nav Nav--sticky">
    <div class="Nav__content">
        <?php // show the language in top menu if the site is not global page and is not in UK site ?>
        <?php if ((int)$siteInfo->blog_id != 1 && count($translations) > 1 && $countryCode !== 'UK'): ?>
            <div class="Nav__language">
                <div class="Nav__language__globe"></div>
                <div class="Nav__language__items" style="color: black;">
                    <?php foreach ($translations as $translation): ?>
                        <a href="<?php echo $translation['url']; ?>"
                           class="Nav__language__item <?php echo $translation['current_lang'] ? 'active' : '' ?>"
                        >
                            <?php echo $translation['slug']; ?>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif ?>
        <a href="javascript:void(0)" class="Nav__logo">
            <div class="Nav__logo-scroll">
                <div class="Nav__logo-image Nav__logo-image-a Nav__logo-image--active"></div>
                <div class="Nav__logo-image Nav__logo-image-x"></div>
                <div class="Nav__logo-image Nav__logo-image-i"></div>
                <div class="Nav__logo-image Nav__logo-image-c"></div>
                <div class="Nav__logo-image Nav__logo-image-o"></div>
                <div class="Nav__logo-image Nav__logo-image-m"></div>
            </div>
        </a>
        <div class="Nav__left">
            <div class="Nav__mobile-toggle">
                <div></div>
                <div></div>
                <div></div>
            </div>

            <div class="Nav__links">
                <?php foreach ($mainMenu as $menuItem): ?>
                    <a
                            href="<?php echo $menuItem->url; ?>"
                            class="Nav__links-link <?php echo $menuItem->active ? 'active' : ''; ?>
                        <?php echo $menuItem->page === 'global' ? 'Nav__links-link--highlight' : ''; ?>"
                    >
                        <?php echo $menuItem->title; ?>
                    </a>
                <?php endforeach; ?>
                <nav class="Footer__social-links NavBarSocials">
                    <a href="<?php echo FixedValue::get('facebookLink'); ?>" target="_blank">
                        <img src="<?php echo asset('img/icons/facebook.png'); ?>"/>
                    </a>
                    <a href="<?php echo FixedValue::get('twitterLink'); ?>" target="_blank">
                        <img src="<?php echo asset('img/icons/twitter.png'); ?>"/>
                    </a>
                    <a href="<?php echo FixedValue::get('linkedinLink'); ?>" target="_blank">
                        <img src="<?php echo asset('img/icons/linkedin.png'); ?>"/>
                    </a>
                    <a href="<?php echo FixedValue::get('instagramLink'); ?>" target="_blank">
                        <img src="<?php echo asset('img/icons/instagram.png'); ?>"/>
                    </a>
                </nav>
            </div>

        </div>


    </div>
</nav>
