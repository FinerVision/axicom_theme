<?php

global $currentPage, $title, $subtitle;

$title = Field::get('hero_header.title');
$subtitle = Field::get('hero_header.subtitle');
$location_title = Field::get('hero_header.subtitle');

$countryCode = FixedValue::get('country');
$countryCity = FixedValue::get('city');

$countryContentArray = [];

if ($countryCode) {
    $countryContentArray[] = $countryCode;
}

if ($countryCity) {
    $countryContentArray[] = "<span class=\"Hero__message__city\">$countryCity</span>";
}
?>


<?php if ($currentPage === 'global'): ?>
    <?php include_once __DIR__ . '/hero-carousel.php'; ?>
<?php else : ?>
    <section class="Hero">
        <div class="content">
            <div class="Hero__message Hero__message--left">
                <?php if ($currentPage === 'home' && isset($countryCode, $countryCity)): ?>
                    <div class="Hero__message__nation">
                        <?php echo implode(' / ', $countryContentArray); ?>
                    </div>
                <?php endif; ?>

                <?php if (isset($heroText)): ?>
                    <h2 class="text-thin">
                        <?php echo $heroText; ?>
                    </h2>
                <?php endif; ?>

                <?php if (in_array($currentPage, ['global', 'home'])): ?>
                    <img src="<?php echo asset('img/logo.png'); ?>" class="Hero__logo img-responsive"/>

                    <?php if (!is_null($title)): ?>
                        <h2 class="Hero__title"><?php echo $title; ?></h2>
                    <?php endif; ?>

                    <?php if (!is_null($subtitle)): ?>
                        <h3 class="text-black"><?php echo $subtitle; ?></h3>
                    <?php endif; ?>
                <?php endif; ?>
            </div>

            <img src="<?php echo asset('img/scroll-arrow.png'); ?>" class="Hero__scroll-arrow"/>
        </div>

        <?php include_once __DIR__ . '/hero-carousel-pages.php'; ?>
    </section>

<?php endif; ?>