<footer class="Footer">
    <div class="container-fluid">
        <div class="content">
            <div class="Footer__info">
                <img src="<?php echo asset('img/logo_footer.png'); ?>" class="Footer__logo"/>
                <p class="Footer__copyright">
                    &copy; <?php echo date('Y', time()); ?> Axicom Ltd. <?php echoCF('all_rights_reserved', 'All rights reserved.'); ?>
                </p>
            </div>

            <div class="Footer__links">
                <nav class="Footer__social-links">
                    <a href="<?php echo FixedValue::get('facebookLink'); ?>" target="_blank">
                        <img src="<?php echo asset('img/icons/facebook.png'); ?>"/>
                    </a>
                    <a href="<?php echo FixedValue::get('twitterLink'); ?>" target="_blank">
                        <img src="<?php echo asset('img/icons/twitter.png'); ?>"/>
                    </a>
                    <a href="<?php echo FixedValue::get('linkedinLink'); ?>" target="_blank">
                        <img src="<?php echo asset('img/icons/linkedin.png'); ?>"/>
                    </a>
                    <a href="<?php echo FixedValue::get('instagramLink'); ?>" target="_blank">
                        <img src="<?php echo asset('img/icons/instagram.png'); ?>"/>
                    </a>
                </nav>

                <nav class="Footer__misc-links">
                    <!-- OneTrust Cookies Settings button start -->

                    <!-- OneTrust Cookies Settings button end -->
                    <a href="javascript:void(0)" id="ot-sdk-btn" class="ot-sdk-show-settings">Cookie Settings</a>
                    <a href="<?php echo url('cookie-policy'); ?>"><?php echo FixedValue::get('privacyAndCookies'); ?></a>
                    <a href="<?php echo url('legals'); ?>"><?php echo FixedValue::get('legals'); ?></a>
                    <a href="<?php echo url('site-map'); ?>"><?php echo FixedValue::get('siteMap'); ?></a>
                </nav>
            </div>
        </div>
    </div>
</footer>
