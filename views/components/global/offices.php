<?php

$offices = Field::get('our_offices.offices');

$officeIds = array_map(function ($office) {
    return $office->ID;
}, $offices);

$officesSrc = Post::find('office', $officeIds);
$offices = [];

foreach ($officesSrc as $office) {
    $weather = getWeatherInfo($office['weather_city'],'kelvin');
    array_push($offices, (object)[
        'name' => $office['the_post']->post_title,
        'city' => $office['city'],
        'weather_city' => $office['weather_city'],
        'timeZone' => $office['time_zone'],
        'address' => $office['address'],
        'temp' => is_array($weather) ? ($weather['temp']) : '-',
        'email' => $office['email'],
        'phone' => $office['phone'],
        'link' => $office['link'],
//        'map' => getImageWithSize($office['map_image'], 'large'),
        'map' => Image::getSize($office['map_image'], 'large'),
        'short_code' => $office['short_code']
    ]);
}
?>

<div data-component="Offices" data-params="<?php json([
    'sectionTitle' => 'Our Offices',
    'items' => $offices,
    'arrowImage' => asset('img/map_arrow.png'),
]); ?>"></div>
