<?php

$title = Field::get('global_team.title');
$description = Field::get('global_team.description');
$people = Field::get('global_team.people');
$peopleIds = array_map(function ($person) {
    return $person->ID;
}, $people);

$people = Post::find('person', $peopleIds);

?>

<section class="section section--people Global__people">
    <div class="content">
        <div class="container-fluid">
            <div class="row reverse-md-max section-content">
                <div class="col-sm-12 col-md-4 col-md-offset-6">
                    <span class="text-dark-olive">
                        <?php echo $description; ?>
                    </span>
                </div>

                <div class="col-sm-12 col-md-2">
                    <h3 class="section__title section__title--stick-top">
                        <?php echo $title; ?>
                    </h3>
                </div>
            </div>
        </div>

        <div class="Gap"></div>
    </div>

    <?php if (!is_null($people)): ?>
        <div data-component="PeopleTeam" data-params="<?php json([
            'items' => array_map(function($item) {
                return array_merge($item, [
                    'image' => [
                        'default' => Image::getSize($item['image'], 'medium_large'),
                    ],
                ]);
            }, $people),
        ]); ?>"></div>
    <?php endif; ?>
</section>
