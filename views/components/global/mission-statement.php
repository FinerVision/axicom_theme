<?php

$title = Field::get('mission_statement.title');
$column_left = Field::get('mission_statement.content.column_left');
$column_right = Field::get('mission_statement.content.column_right');

?>

<section class="section section--mission-statement Global__mission-statement">
    <div class="content">
        <div class="container-fluid">
            <div class="row reverse-md-max section__content">
                <div>
                    <div class="col-sm-12 col-md-5">
                        <?php if (!is_null($column_left)): ?>
                            <div class="Global__mission-statement__content">
                                <p>
                                    <?php echo $column_left; ?>
                                </p>
                            </div>
                        <?php endif; ?>
                    </div>

                    <div class="col-sm-12 col-md-5">
                        <div class="Global__mission-statement__content">
                            <?php if (!is_null($column_right)): ?>
                                <div class="Global__mission-statement__content">
                                    <p>
                                        <?php echo $column_right; ?>
                                    </p>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-2">
                    <?php if (!is_null($title)): ?>
                        <h3 class="section__title Global__remit-title">
                            <?php echo $title; ?>
                        </h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
