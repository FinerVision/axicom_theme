<?php

$left_title = Field::get('resources.left_title');
$left_text = Field::get('resources.left_text');
$right_title = Field::get('resources.right_title');
$right_text = Field::get('resources.right_text');
$image = Field::get('resources.image');
$description = Field::get('resources.description');
$link = Field::get('resources.link');
$resources = Post::all('resource');

$resources = array_map(function ($resource) {
    return '
        <div class="Overlay__list-item">
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <p>
                        ' . $resource['the_post']->post_title . '
                    </p>
                    <div class="media">
                      <div class="media-left ' . ((!$resource['preview'] || $resource['preview'] == '') ? 'hidden' : '') . '" >
                        <div>
                          <img src="' . $resource['preview'] . '" class="media-object" alt="preview" style="max-width:80px; max-height: 100px">
                        </div>
                      </div>
                      <div class="media-body">
                      <p>
                        ' . $resource['description'] . '
                        </p>
                      </div>
                    </div>
                </div>
                
                <div class="col-sm-12 col-md-3">
                    <div class="link-underline" style="padding-top:2px">
                        <a target="_blank" href="' . $resource['hubSpotLink'] . '" style="text-decoration:none !important">
                            ' . FixedValue::get('downloadResource') . ' &gt;
                        </a>
                    </div>
                </div>
            </div>
        </div>
    ';
}, $resources);

?>

<section class="section section--resource Global__resource">
    <div class="content">
        <div class="container-fluid">
            <div class="row reverse-md-max section__content">

                <div>

                    <div class="col-sm-12 col-md-5">
                        <h3><?php echo $left_title; ?></h3>
                        <?php echo $left_text; ?>

                        <div class="Gap Gap--half"></div>
                        <a class="LinkButton" href="<?php echo get_site_url() . '/resources' ?>">
                            View all Resources &gt;
                        </a>
                    </div>

                    <div class="col-sm-12 col-md-5">
                        <h3><?php echo $right_title; ?></h3>
                        <?php echo $right_text; ?>

                        <img class="img-responsive Global__resource-image"
                             src="<?php echo Image::getSize($image, 'medium_large'); ?>"/>

                        <?php if (isset($link)): ?>
                            <a target="_blank" class="LinkButton" href="<?php echo $link; ?>">
                                Download the latest resource here &gt;
                            </a>
                        <?php endif; ?>
                    </div>

                </div>

                <div class="col-sm-12 col-md-2">
                    <h3 class="section__title Global__remit-title">
                        Resources
                    </h3>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="Overlay" data-name="resources">
    <div class="Overlay__modal">
        <div class="Overlay__modal-content">
            <div class="Overlay__close">
                Close
            </div>

            <h4 class="text-underline text-underline--red text-white">
                Resources
            </h4>

            <?php if (is_null($description)): ?>
                <p class="text-white" style="max-width: 400px;">
                    <?php echo $description; ?>
                </p>
            <?php endif; ?>

            <br/>
            <br/>

            <div data-component="paginated-list"
                 data-props="<?php json(['items' => $resources, 'perPage' => 8]); ?>"></div>
        </div>
    </div>
</div>
