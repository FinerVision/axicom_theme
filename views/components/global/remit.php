<?php

$title = Field::get('global_remit.title');
$content = Field::get('global_remit.content');
$background = Field::get('global_remit.background');
$getInTouch = Field::get('global_remit.get_in_touch');

?>

<div class="content">
    <div class="container-fluid">
        <div class="row reverse-md-max section-content">
            <div class="col-sm-12 col-md-4">
                &nbsp;
                <img src="<?php echo Image::getSize($background, 'medium_large'); ?>" class="Global__remit-background"/>
                <br />
                <br />
                <div class="Global__remit-intro">
                    <?php if (!is_null($content)): ?>
                        <?php echo $content; ?>
                    <?php endif; ?>
                    <div class="Global__remit-intro paragraph-window">
                        <?php if (!is_null($getInTouch)): ?>
                            <p><?php echo $getInTouch; ?></p>
                        <?php endif; ?>
                    </div>

                </div>

            </div>
            <div class="col-sm-12 col-md-8">
                <div class="row">
                    <div class="col-sm-12 col-md-3 col-md-offset-9">
                        <div class="row">
                            <?php if (!is_null($title)): ?>
                                <h3 class="section__title Global__remit-title">
                                    <?php echo $title; ?>
                                </h3>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>
