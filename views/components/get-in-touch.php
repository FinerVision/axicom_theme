<div class="GreenStrip GreenStrip--large">
    <div class="GreenStrip__col-center">
        <div class="container-fluid">
            <div class="content">
                <div class="row row-md-height">
                    <div class="col-sm-12 col-md-8 col-md-height col-md-middle">
                        <h3 class="font-light">
                            <?php echo FixedValue::get('getInTouch'); ?>
                            <br/>
                            <span class="font-black text-uppercase">
                                <?php echo FixedValue::get('getInTouch2'); ?>
                            </span>
                        </h3>
                    </div>
                    <div class="col-sm-12 col-md-4 col-md-height col-md-middle text-center">
                        <a href="<?php echo url('contact-us') ?>" class="font-black text-uppercase">
                            <?php echo FixedValue::get('getInTouch3'); ?> &nbsp;&nbsp;&nbsp; <img src="<?php echo asset('img/right-arrow.png'); ?>"/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="GreenStrip__col-left"></div>
</div>
