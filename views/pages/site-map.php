
<div class="Page Site-Map">
    <?php include_once __DIR__ . '/../components/hero-parallax.php'; ?>

    <main>

        <div class="Gap"></div>
        <!-- our work -->
        <section class="section section--no-top-padding section--no-bottom-padding site-map-section">
            <div class="content">
                <div class="container-fluid">
                    <ul>
                        <?php wp_list_pages(array('title_li'=>'')) ?>
                    </ul>
                    <div class="Gap">

                    </div>
                </div>
            </div>
        </section>

        <div class="Gap"></div>
    </main>

    <?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>



