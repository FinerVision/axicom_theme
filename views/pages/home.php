<?php
global $featuredNews;

if (is_null($post) && isLocalHomePage()) {
    $post = getHomePage();
    $page = $post;
}

$page = isset($pages[0]) ? $pages[0] : $post;
Field::$fields = get_fields($page->ID);

$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri_segments = explode('/', $uri_path);

$expertises = [];
$column_left = Field::get('mission_statement.content.column_left');
$quoteTitle = Field::get('mission_statement.title');
$quote = Field::get('mission_statement.content.quote');
$quoteImage = isset($quote['image']) ? Image::getSize($quote['image'], 'medium') : '';
$sectorIntro = Field::get('sector_intro');
$serviceIntro = Field::get('service_intro');
$expertiseSnippet = Post::findByPost(Field::get('expertise'));
$servicesSnippet = Post::findByPost(Field::get('services_2'));
$featuredNews = Post::getFeaturedNews();
$newsItems = array_slice(News::masonryNewsItems(), 0, 3); // only showing first 4 in home page
?>

<div class="Page Home">
    <?php include_once __DIR__ . '/../components/header.php'; ?>

    <main>
        <div class="section Home__mission-statement">
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-2 col-md-offset-10">
                            <?php if (isset($quoteTitle)): ?>
                                <h3 class="section__title">
                                    <?php echo $quoteTitle; ?>
                                </h3>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-11">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="Home__mission-statement-text">
                                        <?php echo $column_left; ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="Home__mission-statement-card">
                                        <div class="Home__mission-statement-card-item Home__mission-statement-card-item--image">
                                            <div class="Home__mission-statement-image"
                                                 style="background-image: url(<?php echo $quoteImage; ?>);"></div>
                                        </div>
                                        <div class="Home__mission-statement-card-item Home__mission-statement-card-item--text">
                                            <?php if (isset($quote['text'])): ?>
                                                <div class="Home__mission-statement-quote">
                                                    <p>
                                                        <?php echo $quote['text']; ?>
                                                    </p>
                                                </div>
                                            <?php endif; ?>

                                            <?php if (isset($quote['name'])): ?>
                                                <div class="Home__mission-statement-name">
                                                    <b><?php echo $quote['name']; ?></b>
                                                </div>
                                            <?php endif; ?>

                                            <?php if (isset($quote['title'])): ?>
                                                <div class="Home__mission-statement-title">
                                                    <b><?php echo $quote['title']; ?></b>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once __DIR__ . '/../components/case-studies-carousel.php'; ?>

        <div class="section section--no-top-padding section--no-bottom-padding Home__expertises">

            <?php if (isset($servicesSnippet) && !is_null($servicesSnippet) && isset($servicesSnippet['background']) && isset($servicesSnippet['description'])): ?>
                <!-- read more expertise -->
                <div data-component="ReadMoreServices" data-props="<?php json([
                    'scrollParent' => '#container',
                    'backgroundImage' => Image::getSize($servicesSnippet['background'], 'large'),
                    'title' => cf('services_title', 'Services'),
                    'content' => html_entity_decode(strip_tags($serviceIntro)), // NOTE : this content is always fixed
                    'linkText' => FixedValue::get('readMore'). '  >',
                    'linkUrl' => url('/services'),
                ]); ?>"></div>
            <?php endif; ?>

            <?php if (isset($expertiseSnippet) && !is_null($expertiseSnippet) && isset($expertiseSnippet['background']) && isset($expertiseSnippet['description'])): ?>
                <!-- read more expertise -->
                <div data-component="ReadMoreExpertise" data-props="<?php json([
                    'scrollParent' => '#container',
                    'backgroundImage' => Image::getSize($expertiseSnippet['background'], 'large'),
                    'title' => cf('expertise_title', 'Expertise'),
                    'content' => html_entity_decode(strip_tags($sectorIntro)), // NOTE : this content is always fixed
                    'linkText' => FixedValue::get('readMore'). '  >',
                    'linkUrl' => url('/sectors'),
                ]); ?>"></div>
            <?php endif; ?>
        </div>

        <!-- news -->
        <div class="section Home__news">

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-2 col-md-offset-10">
                            <h3 class="section__title Home__news-title">
                                <?php echoCF('news_and_views_title', 'News & Views'); ?>
                            </h3>
                        </div>
                        <div class="col-md-12">
                            <?php
                                include_once __DIR__ . '/news/featured.php';
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section News__row">
            <div class="content">
                <div class="container-fluid">
                    <div
                        data-component="NewsGrid"
                        data-params="<?php json([
                            'items' => array_map(function($item) {
                                $item['image'] = [
                                    'default' => Image::getSize($item['image'], 'medium'),
                                    'md' => Image::getSize($item['image'], 'medium_large'),
                                ];
                                $item['relatedNews'] = array_map(function($item) {
                                    $item['image'] = [
                                        'default' => Image::getSize($item['image'], 'thumbnail'),
                                    ];
                                    return $item;
                                }, $item['relatedNews']);
                                return $item;
                            }, $newsItems),
                            'autoScroll' => true,
                            'scrollableElement' => '#container',
                            'navElement' => '.Nav',
                            'animationDuration' => 400,
                            'disableHoverAnimation' => true,
                            'newsPageUri' => '/'.$uri_segments[1].'/news'
                        ]); ?>"
                    ></div>
                </div>
            </div>
        </div>
    </main>
    <?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>
