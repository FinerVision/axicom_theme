<?php

global $countries;

$pages = get_posts([
    'post_type' => 'page',
    'pagename' => 'global',
]);

$page = isset($pages[0]) ? $pages[0] : $post;
Field::$fields = get_fields($page->ID);

?><div class="Page Global">
    <?php include_once __DIR__ . '/../components/header.php'; ?>

    <main>
        <?php if (isset($countries) && !is_null($countries) && count($countries) > 0): ?>
            <div class="hidden-md hidden-lg">
                <div data-component="NavSlider" data-params="<?php json(['items' => $countries]); ?>"></div>
            </div>
        <?php endif; ?>

        <?php include_once __DIR__ . '/../components/global/mission-statement.php'; ?>
        <?php include_once __DIR__ . '/../components/case-studies-carousel.php'; ?>
        <?php include_once __DIR__ . '/../components/global/resource.php'; ?>
        <?php include_once __DIR__ . '/../sections/sectors_and_services.php'; ?>

        <section class="section Global__remit">
            <?php include_once __DIR__ . '/../components/global/remit.php'; ?>
            <?php include_once __DIR__ . '/../components/global/people.php'; ?>
        </section>

        <?php include_once __DIR__ . '/../components/global/people.php'; ?>
        <?php include_once __DIR__ . '/../components/global/offices.php'; ?>
    </main>

    <?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>
