<script src="<?php echo asset('js/masonry_news.js'); ?>"></script>

<?php
    $page_post = get_page_by_path('news');
    setup_postdata($page_post);
    $post = $page_post;
    Field::$fields = get_fields();
?>

<div class="Page News">
    <?php include_once __DIR__ . '/../components/hero-parallax.php'; ?>

    <main>
        <div class="Gap"></div>
        <?php include_once __DIR__ . '/news/news-single.php'; ?>

        <div class="Gap"></div>
        <?php include_once __DIR__ . '/../components/get-in-touch.php'; ?>

    </main>

    <?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>
