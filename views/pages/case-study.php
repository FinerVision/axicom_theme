<?php

// NOW you are on the single item
Field::$fields = get_fields();

$title = Field::get('title');
$subtitle = Field::get('subtitle');
$background_image = Field::get('image');
$targets_image = Field::get('targets_image');
$results_image = Field::get('results_image');
$youtubeId = Field::get('youtube_video_id');
$case_study = Field::get('*');
$hero_parallax_title = Field::get('head_title');

// Page
$page_post = get_page_by_path('our-work');
setup_postdata($page_post);
$post = $page_post;
Field::$fields = get_fields();
?>

<div class="Page CaseStudy">
    <?php
        include_once __DIR__ . '/../components/hero-parallax.php';
        wp_reset_query();
        // NOW you ar on the single item again
        Field::$fields = get_fields();
    ?>

    <main>
        <!-- title and brief -->
        <div class="section CaseStudy__intro">
            <div class="container-fluid">
                <div class="content content-970">
                    <div class="row reverse-md-max">

                        <div class="col-md-9 col-md-offset-1">

                            <?php if (isset($case_study['summary'])): ?>
                                <div class="CaseStudy__intro-text">
                                    <p>
                                        <?php echo $case_study['summary']; ?>
                                    </p>
                                    <br/>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-2">
<!--                            <h3 class="CaseStudy__intro-title section__title">-->
<!--                                --><?php //echo FixedValue::get('summary'); ?>
<!--                            </h3>-->
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .section.CaseStudy__intro -->

        <?php if ($background_image): ?>
        <!-- full width image -->
        <div class="content-970">
            <div id="marker" style="height: 100px;width:20px;position: absolute;bottom: 100px"></div>
            <div class="CaseStudy__full-width-image" style="background-image: url(<?php echo($background_image); ?>);"></div>
        </div>

        <?php elseif ($youtubeId): ?>
        <!-- video -->
        <div class="container-fluid">
            <div class="content">
                <div class="CaseStudy__video">
                    <iframe src="https://www.youtube.com/embed/<?php echo htmlentities($youtubeId); ?>?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <?php endif; ?>


        <!-- contents with background -->
        <div class="CaseStudy__background">
            <div class="CaseStudy__background-decoration"></div>
            <div id="waypoint" class="section CaseStudy__strategy">
                <div class="Gap"></div>
                <div class="container-fluid">
                    <div class="content content-970">
                        <div class="row row-equal-columns px-25px mobile-row-no-padding">
                            <div class="col-xs-12 col-md-6 col-md-push-6 col-lg-push-6">
                                <div class="CaseStudyTargets__wrapper">
                                    <div class="CaseStudyTargets__top">
                                        <div class="CaseStudyTargets__menu" style="margin-bottom: 50px">
                                            <?php if (isset($case_study['challenge'])): ?>
                                                <div id="first" class="CaseStudyTargets__link CaseStudyTargets__link--current" data-tab="tab-challenge" data-title="title-challenge"><span>Challenge</span></div>
                                            <?php endif; ?>
                                            <?php if (isset($case_study['strategy'])): ?>
                                                <div id="second" class="CaseStudyTargets__link" data-tab="tab-strategy" data-title="title-strategy"><span>Strategy</span></div>
                                            <?php endif; ?>
                                            <?php if (isset($case_study['results'])): ?>
                                                <div id="last" class="CaseStudyTargets__link" data-tab="tab-results" data-title="title-results"><span>Results</span></div>
                                            <?php endif; ?>
                                        </div>
                                        <?php if (isset($case_study['challenge'])): ?>
                                            <div id="tab-challenge" class="CaseStudyTargets__content CaseStudyTargets--show">
                                                <?php echo $case_study['challenge']; ?>
                                            </div>
                                        <?php endif; ?>

                                        <?php if (isset($case_study['strategy'])): ?>
                                            <div id="tab-strategy" class="CaseStudyTargets__content">
                                                <?php echo $case_study['strategy']; ?>
                                            </div>
                                        <?php endif; ?>

                                        <?php if (isset($case_study['results'])): ?>
                                            <div id="tab-results" class="CaseStudyTargets__content">
                                                <?php echo $case_study['results']; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>



                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-pull-6 col-lg-pull-6">
                                <div class="Rectangle">
                                    <?php if (isset($targets_image)): ?>
                                        <img src="<?php echo($targets_image); ?>" class="Rectangle__image">
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                    </div><!-- .content -->
                </div><!-- .container-fluid -->
            </div><!-- .section -->

            <!-- statistics -->
            <?php if (
                (isset($case_study['status_1_number']) && trim($case_study['status_1_number'])) &&
                (isset($case_study['status_1_title']) && trim($case_study['status_1_title'])) &&
                (isset($case_study['status_2_number']) && trim($case_study['status_2_number'])) &&
                (isset($case_study['status_2_title']) && trim($case_study['status_2_title'])) &&
                (isset($case_study['status_3_number']) && trim($case_study['status_3_number'])) &&
                (isset($case_study['status_3_title']) && trim($case_study['status_3_title']))
            ): ?>

            <div class="KeyResults">
                <div class="KeyResults__text KeyResults__text-left">
                    <div class="KeyResults__text-inner">
                <span>
                    <?php if (isset($case_study['header'])): ?>
                        <?php echo $case_study['header']; ?>
                    <?php endif; ?>
                </span>
                    </div>
                </div>
                <div class="KeyResults__text KeyResults__text-right">
                    <div class="KeyResults__text-inner">
                        <div class="row">
                            <div class="col-xs-4">
                                <?php if (isset($case_study['status_1_number'])): ?>
                                    <strong><?php echo $case_study['status_1_number']; ?></strong>
                                    <br/>
                                    <span class="font-light"><?php echo $case_study['status_1_title']; ?></span>
                                <?php endif; ?>
                            </div>
                            <div class="col-xs-4">
                                <?php if (isset($case_study['status_2_number'])): ?>
                                    <strong><?php echo $case_study['status_2_number']; ?></strong>
                                    <br/>
                                    <span class="font-light"><?php echo $case_study['status_2_title']; ?></span>
                                <?php endif; ?>
                            </div>
                            <div class="col-xs-4">
                                <?php if (isset($case_study['status_3_number'])): ?>
                                    <strong><?php echo $case_study['status_3_number']; ?></strong>
                                    <br/>
                                    <span class="font-light"><?php echo $case_study['status_3_title']; ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="KeyResults__middle-decoration"></div>
            </div><!-- .KeyResults -->
            <?php endif; ?>

            <!-- strategy -->
            <div class="section CaseStudy__results">
                <div class="container-fluid">
                    <div class="content-970 pb-70px">

                        <br class="visible-md visible-lg"/>
                        <br class="visible-md visible-lg"/>
                        <br/>
                        <?php if ($case_study['quote']['quote'] !== ''): ?>
                        <div class="Rectangle--horizontal">
                            <?php if (isset($case_study['quote']['logo'])): ?>
                            <div class="Rectangle__logo">
                                <img src="<?php echo $case_study['quote']['logo']; ?>" class="Rectangle__logo_img">
                            </div>
                            <?php endif; ?>
                            <div class="Rectangle__text">
                                <?php if (isset($case_study['quote']['quote'])): ?>
                                    <div class="Rectangle__quote">
                                        <?php echo $case_study['quote']['quote']; ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (isset($case_study['quote']['name'])): ?>
                                    <div class="Rectangle__name">
                                        <?php echo $case_study['quote']['name']; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if (isset($results_image)): ?>
                            <div class="CaseStudy__results-image">
                                <div class="CaseStudy__results-image-content">
                                    <img src="<?php echo $results_image ?>" class="CaseStudy__results_img"/>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="Gap"></div>

        <?php include_once __DIR__ . '/../components/get-in-touch.php'; ?>

    </main>

    <?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>
