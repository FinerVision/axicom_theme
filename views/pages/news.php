<?php

$pages = get_posts([
    'post_type' => 'page',
    'pagename' => 'news',
]);


$page = isset($pages[0]) ? $pages[0] : $post;
Field::$fields = get_fields($page->ID);

$newsPageUrl = get_permalink($page);
$newsPageUri = parse_url($newsPageUrl, PHP_URL_PATH);
$currentNewsSlug = $post->post_type === 'news' ? $post->post_name : '';
$featuredNews = Post::getFeaturedNews();
$newsItems = News::masonryNewsItems();
$news_categories = News::getAllCategories($newsItems);
$newsIntro = Field::get('intro');

$params = get_query_var('news_search');
if(!empty($params)) {
    $newsItems = News::masonryNewsItemsFilterBy($params);
}
?>

<div class="Page News">
    <?php include_once __DIR__ . '/../components/hero-parallax.php'; ?>

    <main id="main">
        <?php include_once __DIR__ . '/news/header.php'; ?>

        <?php
            include_once __DIR__ . '/news/featured.php';
        ?>

        <div class="Gap"></div>
        <div class="section News__row overflow-hidden">
            <div class="container-fluid">
                <div class="content">
                    <div class="row content-posts">
                        <div class="col-xs-12 col-sm-12 col-md-3 col-md-push-9 mt-14px" style="flex:1;">
                            <div class="sidebar" style="width: 227.5px">
                                <?php get_search_form() ?>
                                <br/>
                                <div data-component="CustomSelect" data-params="<?php json([
                                    'filterEvent' => 'news-filter-category',
                                    'name' => 'category',
                                    'items' => array_merge([
                                        '' => 'All Categories',
                                    ], isset($news_categories) ? $news_categories : []),
                                    'defaultValue' => '',
                                ]); ?>"></div>


                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9 col-md-pull-3">
                            <h2 class="mt-14px text-dark-olive"><?php echo (empty($newsItems))?"No posts found":"" ?></h2>
                            <div
                                    data-component="NewsGrid"
                                    data-params="<?php json([
                                        'items' => array_map(function($item) {
                                            $item['image'] = [
                                                'default' => Image::getSize($item['image'], 'medium_large'),
                                            ];
                                            $item['relatedNews'] = array_map(function($item) {
                                                $item['image'] = [
                                                    'default' => Image::getSize($item['image'], 'thumbnail'),
                                                ];
                                                return $item;
                                            }, $item['relatedNews']);
                                            return $item;
                                        }, $newsItems),
                                        'autoScroll' => true,
                                        'scrollableElement' => '#container',
                                        'navElement' => '.Nav',
                                        'animationDuration' => 400,
                                        'filteringEvent' => 'news-filter-category',
                                        'isUriUpdating' => true,
                                        'currentNewsSlug' => $currentNewsSlug,
                                        'newsPageUri' => $newsPageUri
                                    ]); ?>"
                            ></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once __DIR__ . '/../components/get-in-touch.php'; ?>

    </main>

    <?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>
