<?php

$result = getJobsAndDepartment(Field::get('search_header.category_search'), Field::get('search_header.location_search'));
$jobs = $result['jobs'];

if (isset($result) && !is_null($result)) {
    $categories = $result['categories'];
    $locations = $result['locations'];
}


$introImage = Field::get('intro.image');
$youtubeId = Field::get('intro.youtube_video_id');

?>

<div class="Page Careers">
    <?php include_once __DIR__ . '/../components/hero-parallax.php'; ?>

    <main>
        <!-- intro -->
        <div class="section Careers__intro">
            <div class="container-fluid">
                <div class="content">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="text-dark-olive">
                                <?php echo Field::get('intro.content'); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <?php if ($introImage): ?>
                                <img class="img-responsive Careers__graph-img" src="<?php echo Image::getSize($introImage, 'medium_large'); ?>">
                            <?php endif; ?>

                            <?php if ($youtubeId && !empty($youtubeId)): ?>
                                <!-- video -->
                                <div class="CaseStudy__video">
                                    <iframe src="https://www.youtube.com/embed/<?php echo htmlentities($youtubeId); ?>?autoplay=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .section.Careers__intro -->
        

        <!-- job list -->
        <div class="section Careers__job-list">
            <div class="container-fluid">
                <div class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (isset($jobs) && !is_null($jobs) && count($jobs) > 0): ?>
                            <div data-component="JobList"
                                 data-params="<?php json(['applyText' => Field::get('apply_button'), 'items' => $jobs]); ?>"
                            ></div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>
