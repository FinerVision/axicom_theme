<?php

$pattern = [3, 2, 3, 3, 2, 2];
$patternIndex = 0;
$patternCounter = 1;
$imageIndex = 0;

$works = [];
foreach(Post::all('work') as $work) {
    $works[] = (object)[
        'name' => $work['title'],
        'backgroundImage' => $work['image'],
        'url' => get_permalink($work['the_post']->ID),
    ];
}

?>
<div class="Page Work">
    <?php include_once __DIR__ . '/../components/hero-parallax.php'; ?>

    <main>
        <div class="Gap"></div>
        <!-- our work -->
        <section class="section section--no-top-padding section--no-bottom-padding Work__our-work">
            <div class="container-fluid">
                <div class="Work__case-studies-content">
                    <div class="row no-gutter">
                        <?php foreach($works as $work): ?>
                            <?php if ($pattern[$patternIndex] == 3): ?>
                                <div class="col-xs-12 col-sm-6 col-md-4">
                                    <a href="<?php echo $work->url; ?>">
                                        <div class="hidden-md hidden-lg">
                                            <div class="Work__case-studies-image" style="background-image: url('<?php echo Image::getSize($work->backgroundImage, 'medium'); ?>')">
                                                <div class="HoveringCutOff">
                                                    <div class="HoveringCutOff__bg"></div>
<!--                                                    <div class="HoveringCutOff__cut-off"></div>-->
                                                    <div class="HoveringCutOff__inner"><?php echo $work->name; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="visible-md visible-lg">
                                            <div class="Work__case-studies-image" style="background-image: url('<?php echo Image::getSize($work->backgroundImage, 'medium_large'); ?>')">
                                                <div class="HoveringCutOff">
                                                    <div class="HoveringCutOff__bg"></div>
<!--                                                    <div class="HoveringCutOff__cut-off"></div>-->
                                                    <div class="HoveringCutOff__inner"><?php echo $work->name; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php elseif ($pattern[$patternIndex] == 2): ?>
                                <div class="col-xs-12 col-sm-6">
                                    <a href="<?php echo $work->url; ?>">
                                        <div class="hidden-md hidden-lg">
                                            <div class="Work__case-studies-image" style="background-image: url(<?php echo Image::getSize($work->backgroundImage, 'medium'); ?>)">
                                                <div class="HoveringCutOff">
                                                    <div class="HoveringCutOff__bg"></div>
<!--                                                    <div class="HoveringCutOff__cut-off"></div>-->
                                                    <div class="HoveringCutOff__inner"><?php echo $work->name; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="visible-md visible-lg">
                                            <div class="Work__case-studies-image" style="background-image: url(<?php echo Image::getSize($work->backgroundImage, 'medium_large'); ?>)">
                                                <div class="HoveringCutOff">
                                                    <div class="HoveringCutOff__bg"></div>
<!--                                                    <div class="HoveringCutOff__cut-off"></div>-->
                                                    <div class="HoveringCutOff__inner"><?php echo $work->name; ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            <?php endif; ?>

                            <?php
                            $imageIndex++;
                            $patternCounter++;

                            if ($patternIndex >= count($pattern)) {
                                $patternIndex = 0;
                            }

                            if ($patternCounter > $pattern[$patternIndex]) {
                                $patternCounter = 1;
                                $patternIndex++;
                            }
                            ?>

                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </section>
        <div class="Gap"></div>
    </main>

    <?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>
