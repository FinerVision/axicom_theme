
<div class="Page Site-Map">
    <?php include_once __DIR__ . '/../components/hero-parallax.php';

    ?>
    <main>
    <?php
    $resources = Post::all('resource');

    $resources = array_map(function ($resource) {
        return '
        <div class="Overlay__list-item">
            <div class="row">
                <div class="col-sm-12 col-md-9">
                    <p>
                        ' . $resource['the_post']->post_title . '
                    </p>
                    <div class="media">
                      <div class="media-left ' . ((!$resource['preview'] || $resource['preview'] == '') ? 'hidden' : '') . '" >
                        <div>
                          <img src="' . $resource['preview'] . '" class="media-object" alt="preview" style="max-width:80px; max-height: 100px">
                        </div>
                      </div>
                      <div class="media-body">
                      <p>
                        ' . $resource['description'] . '
                        </p>
                      </div>
                    </div>
                </div>
                
                <div class="col-sm-12 col-md-3">
                    <div class="link-underline" style="padding-top:2px">
                        <a target="_blank" href="' . $resource['hubSpotLink'] . '" style="text-decoration:none !important">
                            ' . FixedValue::get('downloadResource') . ' &gt;
                        </a>
                    </div>
                </div>
            </div>
        </div>
    ';
    }, $resources);

    ?>




    <div class="content">
        <div class="container-fluid">

            <div data-component="paginated-list"
                 data-props="<?php json(['items' => $resources, 'perPage' => 8]); ?>"></div>
            <div class="Gap"></div>
        </div>
    </div>

    </main>

    <?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>



