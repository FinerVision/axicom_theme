<?php

$intro = Field::get('intro');
$vision = Field::get('vision');
$experience = Field::get('experience');
$left_column_text = Field::get('left_column_text');
$right_column_text = Field::get('right_column_text');

?>

<div class="Page About">
    <?php include_once __DIR__ . '/../components/hero-parallax.php'; ?>

    <main>
        <!-- super section of axicom london and our vision -->
        <div class="About__axicom-london">

            <!-- gray shadow -->
            <div class="About__axicom-london-decoration"></div>

            <!-- axicom london -->
            <section class="section">
                <div class="container-fluid">
                    <div class="content">
                        <div class="row large-gutter section-content">
                            <div class="col-md-2 col-md-offset-10">
                                <?php if (isset($intro['title'])): ?>
                                    <h3 class="section__title">
                                        <?php echo $intro['title']; ?>
                                    </h3>
                                <?php endif; ?>
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <div class="About__axicom-london-intro">
                                    <?php if (isset($intro['text'])): ?>
                                        <p>
                                            <?php echo $intro['text']; ?>
                                        </p>
                                    <?php endif; ?>
                                </div>
                                <br/>
                            </div>
                            <div class="col-sm-12 col-md-7">
                                <p>
                                    <?php if (isset($intro['image'])): ?>
                                        <img class="img-responsive full-width" src="<?php echo Image::getSize($intro['image'], 'medium_large'); ?>"/>
                                    <?php endif; ?>
                                </p>
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-6">
                                        <?php if (isset($intro['quote'])): ?>
                                            <p class="text-right about-us-quote">
                                                <?php echo $intro['quote']; ?>
                                            </p>
                                        <?php endif; ?>

                                        <p class="text-right">
                                            <?php if (isset($intro['name'])): ?>
                                                <strong><?php echo $intro['name']; ?></strong><br/>
                                            <?php endif; ?>
                                            <?php if (isset($intro['job_title'])): ?>
                                                <span class="text-red"><?php echo $intro['job_title']; ?></span>
                                            <?php endif; ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- our vision -->
            <section class="section section--no-bottom-padding">
                <div class="QuoteSection"
                     style="background-image: url(<?php echo asset('img/about/our-vision-background.png'); ?>);">
                    <div class="QuoteSection__text">
                        <div class="QuoteSection__text-inner">
                            <div class="container-fluid">
                                <div class="content">
                                    <div class="row large-gutter section-contnet">
                                        <div class="col-md-2 col-md-offset-10">
                                            <?php if (isset($vision['title'])): ?>
                                                <h3 class="section__title">
                                                    <?php echo $vision['title']; ?>
                                                </h3>
                                            <?php endif; ?>

                                            <div class="Gap visible-md visible-lg"></div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="QuoteSection__quote">
                                                <?php if (isset($vision['text'])): ?>
                                                    <p>
                                                        <?php echo $vision['text']; ?><br/>

                                                        <?php if (isset($vision['bold_text'])): ?>
                                                            <span class="QuoteSection__text-black">
                                                                <?php echo $vision['bold_text']; ?>
                                                            </span>
                                                        <?php endif; ?>
                                                    </p>
                                                <?php endif; ?>
                                            </div>
                                            <div class="Gap hidden-md hidden-lg"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div><!-- super section -->


        <!-- content -->
        <section class="section About__content">
            <div class="container-fluid">
                <div class="content">
                    <div class="content">
                        <div class="row">
                            <div class="col-md-5">
                                <?php if (isset($left_column_text)): ?>
                                    <p><?php echo $left_column_text; ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-5">
                                <?php if (isset($right_column_text)): ?>
                                    <p><?php echo $right_column_text; ?></p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php if (is_array($experience['images']) && count($experience['images'])): ?>
            <!-- our experience -->
            <section class="section About__our-experience">
                <div class="container-fluid">
                    <div class="content">
                        <div class="row reverse-md-max section-content">
                            <div class="col-md-10">
                                <br/>
                                <div class="row" style="display:block; position:relative; height: 200px">
                                    <?php
                                        $logosCount = 0;
                                        foreach ($experience['images'] as $image){
                                            if(isset($image['image']) && !empty($image['image']))
                                                ++$logosCount;
                                        }
                                    ?>
                                    <?php foreach($experience['images'] as $index => $image): ?>
                                        <?php if (isset($image['image']) && $image['image']): ?>

                                            <?php if($index !== $logosCount - 1) : ?>
                                                <div class="col-xs-4 col-sm-4 About__our-experience-logo text-center">
                                                    <img class="img-responsive" style="margin: auto;" data-img-greyscale="<?php echo Image::getSize($image['image'], 'medium'); ?>" data-brightness="67" />
                                                </div>
                                            <?php else: ?>
                                                <div class="col-xs-4 col-sm-4 About__our-experience-logo text-center hidden-sm">
                                                    <img class="img-responsive" style="margin: auto;" data-img-greyscale="<?php echo Image::getSize($image['image'], 'medium'); ?>" data-brightness="67" />
                                                </div>
                                            <?php endif; ?>

                                        <?php endif; ?>

                                        <?php if (!(($index + 1) % 3)): ?>
                                            <div class="visible-sm clearfix"></div>
                                        <?php elseif (!(($index + 1) % 5)): ?>
                                            <div class="visible-md visible-lg clearfix"></div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <?php if (isset($experience['title'])): ?>
                                    <h3 class="section__title About__our-experience-title">
                                        <?php echo $experience['title']; ?>
                                    </h3>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php endif; ?>

        <div class="Gap"></div>
        <?php include_once __DIR__ . '/../components/get-in-touch.php'; ?>

    </main>

    <?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>
