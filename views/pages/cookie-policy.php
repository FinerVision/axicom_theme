
<div class="Page CookiePolicy">

    <?php include_once __DIR__ . '/../components/hero-parallax.php'; ?>

    <div class="CookiePolicy__inner">

        <div class="content">
            <div class="container-fluid">
                <h2><?php echo the_title(); ?></h2>
                <br />
                <?php
                while (have_posts()) {
                    the_post();
                    the_content();
                }; // End of the loop.
                ?>
                <br />
                <br />
                <br />
            </div>
        </div>
    </div>

    <?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>
