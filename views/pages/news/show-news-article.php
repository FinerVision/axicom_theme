<?php

$pages = get_posts([
    'post_type' => 'page',
    'pagename' => 'news',
]);

$page = isset($pages[0]) ? $pages[0] : $post;

Field::$fields = get_fields($page->ID);

$overlay_1 = Field::get('hero_parallax.overlay_1');
$overlay_2 = Field::get('hero_parallax.overlay_2');
$scroll_arrow = Field::get('hero_parallax.scroll_arrow');


wp_reset_query();
Field::$fields = get_fields();
$background_image = Field::get('image');
$related = Field::get('related');
$relatedNews = [];


$postFields = Post::findByPost($post);
$hero_parallax_title = $post->post_title;
$subtitle = Field::get('subtitle');
if(!empty($related)){
    foreach ($related as $r) {
        $rFull = Post::findByPost($r);

        array_push($relatedNews, [
            'image' => $rFull['image'],
            'title' => $rFull['the_post']->post_title,
            'category' => '',
            'date' => $rFull['post_date'],
            'post_name' => $rFull['the_post']->post_name,
        ]);
    }
}

?>

<div class="Page News">
    <?php include_once __DIR__ . './../../components/news-single-hero-parallax.php'; ?>

    <main id="main">

        <?php
        include_once __DIR__ . './../news/featured.php';
        ?>

        <div class="Gap"></div>
        <div class="section News__row overflow-hidden">
            <div class="container-fluid ">

                <div class="content content-660 content-news social-reference">
                    <?php echo add_social_sharing() ?>
                    <div class="NewsSingle">

                        <?php if (isset($hero_parallax_title)): ?>
                            <span class="HeroParallax__subtitle HeroParallax--subtitle-news mb-70px" style="display:block;color: #3fafa6;"><?php echo $hero_parallax_title; ?></span>
                        <?php endif; ?>
                        <?php if ($postFields['author']->ID !== null): ?>
                            <div class="NewsSingle__head">
                                <div class="NewsSingle__member_image">
                                    <img src="<?php echo ($postFields['author']->ID !== null)?get_field('image', $postFields['author']->ID):'' ?>"
                                         class="NewsSingle__member_img">
                                </div>
                                <div class="NewsSingle__member_description">
                                    <div><?php echo $postFields['author']->post_title ?></div>
                                    <div><?php echo get_field('title', $postFields['author']->ID) ?>
                                    </div>
                                    <div>
                                        <?php echo date("d/m/Y", strtotime($post->post_date)); ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if (isset($background_image)): ?>
                            <img src="<?php echo Image::getSize($background_image, 'fullsize'); ?>" class="NewsSingle__cover"/>
                        <?php endif; ?>
                        <br/>
                        <br/>
                        <div class="NewsSingle__content"><?php echo $post->post_content ?></div>
                        <div class="mt-14px">
                            <div class="NewsSingle__back NewsSingle__back--type-2">
                                <a href="<?php echo url('/news') ?>">< Back to News & Views</a>
                            </div>
                            <ul class="SocialIcons__vertical_list d-inline-block d-md-none" style="top: inherit; bottom: 0;">
                                <li class="SocialIcons__element"><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Flocalhost%2Ftestsite&amp;t=Test%20site" title="Share on Facebook" target="_blank" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(document.URL) + '&amp;t=' + encodeURIComponent(document.URL)); return false;"><img src="https://axicom.finervision.com/uk/wp-content/themes/axicom/public/img/facebook.png"></a></li>
                                <li class="SocialIcons__element"><a href="https://twitter.com/intent/tweet?source=http%3A%2F%2Flocalhost%2Ftestsite&amp;text=Test%20site:%20http%3A%2F%2Flocalhost%2Ftestsite" target="_blank" title="Tweet" onclick="window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent(document.title) + ':%20'  + encodeURIComponent(document.URL)); return false;"><img src="https://axicom.finervision.com/uk/wp-content/themes/axicom/public/img/twitter.png"></a></li>
                                <li class="SocialIcons__element"><a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http%3A%2F%2Flocalhost%2Ftestsite&amp;title=Test%20site&amp;summary=Test%20site&amp;source=http%3A%2F%2Flocalhost%2Ftestsite" target="_blank" title="Share on LinkedIn" onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&amp;url=' + encodeURIComponent(document.URL) + '&amp;title=' +  encodeURIComponent(document.title)); return false;"><img src="https://axicom.finervision.com/uk/wp-content/themes/axicom/public/img/linkedin.png"></a></li>
                            </ul>
                        </div>
                    </div>


                    <?php if (is_array($relatedNews) && !empty($relatedNews)): ?>

                        <div class="News__related">
                            <div class="News__related-head">You may also like</div>
                            <div class="ThickLine ThickLine--stick"></div>
                            <div class="NewsGrid NewsGrid--related">
                                <?php foreach ($relatedNews as $index => $news): ?>
                                    <div class="NewsGrid__item NewsGrid__item--related">
                                        <div class="NewsCard NewsCard--related">
                                            <div class="NewsCard__image-wrap">
                                                <a class="NewsCard__image" href="<?php echo get_site()->path.'news/'.$news['post_name'] ?>">
                                                    <div class="NewsCard__image-inner"
                                                         style="background-image: url(<?php echo $news['image']; ?>);"></div>
                                                    <div class="HoveringCutOff HoveringCutOff--small">
                                                        <div class="HoveringCutOff__cut-off"></div>
                                                        <div class="HoveringCutOff__inner">AxiOnline</div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="ThickLine ThickLine--stick"></div>
                                            <br>
                                            <div class="NewsCard__title NewsCard__title--related">
                                                <?php echo $news['title']; ?>
                                            </div>

                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>

                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php include_once __DIR__ . './../../components/get-in-touch.php'; ?>

    </main>

    <?php include_once __DIR__ . './../../components/footer.php'; ?>
</div>
