<?php
global $newsIntro, $news_categories;
?><!-- news header text -->
<div class="section News__intro">
	<div class="container-fluid">
        <div class="content">
            <div class="row reverse-md-max">
                <div class="col-md-6">
                    <div class="text-dark-olive">
                        <?php echo $newsIntro; ?>
                    </div>
                </div>
                <div class="col-md-2 col-md-offset-4">
                    <h3 class="section__title">
                        <?php echoCF('news_intro_title', 'News'); ?>
                    </h3>
                </div>
            </div>
		</div>
	</div>
</div><!-- .section.News__intro -->
