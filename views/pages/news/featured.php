<?php if ($featuredNews) : ?>

    <div class="section section--no-top-padding section--no-bottom-padding">
        <div class="content">
            <a href="<?php echo get_site_url() .'/news/'. $featuredNews['the_post']->post_name ?>">
                <div class="FeaturedNews__inner">
                    <div class="FeaturedNews__left">
                        <div class="row">
                            <div class="col-sm-8 col-sm-offset-2">
                                <div class="FeaturedNews__title">
                                    <?php echo $featuredNews['the_post']->post_title ?>
                                </div>
                                <div class="FeaturedNews__brief">
                                    <?php echo str_limit(strip_tags($featuredNews['the_post']->post_content), 120) ?>
                                    <br/>
                                    <div class="text-underline text-underline--red">
                                        <?php echo $featuredNews['author']->post_title ?>
                                    </div>
                                </div>
                                <div class="RedLine"></div>
                                <!--
                                NOTE: Removed for now from client request.
                                <div class="FeaturedNews__date">
                                18th November 2017
                                </div>
                                -->
                            </div>
                        </div>
                    </div>
                    <div class="FeaturedNews__right">
                        <div class="FeaturedNews__image"
                             style="background-image: url(<?php echo $featuredNews['image'] ? Image::getSize($featuredNews['image'], 'medium_large') : '' ?>);"></div>
                        <div class="HoveringCutOff HoveringCutOff--pseudo-hover">
                            <div class="HoveringCutOff__cut-off"></div>
                            <div class="HoveringCutOff__inner-corner"></div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

<?php endif; ?>