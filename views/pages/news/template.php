<div class="NewsMasonry__item" data-category="-">
    <div class="NewsMasonry__item__preview">
        <!-- news card -->
        <div class="NewsCard">
            <div class="NewsCard__image-wrap">
                <div class="NewsCard__image">
                    <div class="NewsCard__image-inner __image" style=""></div>
                    <div class="HoveringCutOff HoveringCutOff--small">
                        <div class="HoveringCutOff__cut-off"></div>
                        <div class="HoveringCutOff__inner __category"></div>
                    </div>
                </div>
            </div>
            <div class="ThickLine ThickLine--stick"></div>
            <br/>
            <div class="NewsCard__title __title"></div>
            <br/>
            <!--
            NOTE: Removed for now from client request.
            <div class="NewsCard__detail __date"> 18th November 2017</div>
            <div class="NewsCard__author __author">
                <span class="__author__0__name">xxx</span>
            </div>
            -->
        </div><!-- .NewsCard -->

    </div>
    <div class="NewsMasonry__item__content">
        <div class="News__row-content">
            <div class="News__content-close">&times;</div>
            <div class="__content"></div>
            <div class="News__related">
                <div class="News__related-head"><?php echoCF('news_related_title', 'Related News & Views'); ?></div>
                <div class="News__related-body">
                    <div class="News__related-body-inner">
                        <div class="__relatedNews">
                            <div class="News__related-item">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="News__related-image __relatedNews__0__image"></div>
                                        <div class="ThickLine ThickLine--stick-top ThickLine--thin ThickLine--short"></div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="News__related-title __relatedNews__0__title"></div>
                                        <br />
                                        <!--
                                        NOTE: Removed for now from client request.
                                        <div class="News__related-date __relatedNews__0__date"></div>
                                        -->
                                    </div>
                                </div>
                            </div>
                            <div class="News__related-item">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="News__related-image __relatedNews__1__image"></div>
                                        <div class="ThickLine ThickLine--stick-top ThickLine--thin ThickLine--short"></div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="News__related-title __relatedNews__1__title"></div>
                                        <br />
                                        <!--
                                        NOTE: Removed for now from client request.
                                        <div class="News__related-date __relatedNews__1__date"></div>
                                        -->
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

