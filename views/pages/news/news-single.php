<?php
wp_reset_query();
Field::$fields = get_fields();

$related = Field::get('related');
$relatedNews = [];

$postFields = Post::findByPost($post);

foreach ($related as $r) {
    $rFull = Post::findByPost($r);
    array_push($relatedNews, [
        'image' => $rFull['image'],
        'title' => $rFull['the_post']->post_title,
        'category' => '',
        'date' => $rFull['post_date'],
    ]);
}
?>

<div class="section section--no-top-padding section--no-bottom-padding FeaturedNews">
    <div class="container-fluid">
        <div class="content">
            <div class="FeaturedNews__inner">
                <div class="FeaturedNews__left">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="FeaturedNews__title">
                                <?php echo $post->post_title ?>
                            </div>
                            <div class="FeaturedNews__brief">
                                <?php echo str_limit(strip_tags($post->post_content), 120) ?>
                                <br/>
                                <br/>
                                <div class="text-underline text-underline--red">
                                    <?php echo Field::get('author')->post_title ?>
                                </div>
                            </div>
                            <div class="RedLine"></div>
                            <!--
                            NOTE: Removed for now from client request.
                            <div class="FeaturedNews__date">
                            18th November 2017
                            </div>
                            -->
                        </div>
                    </div>
                </div>
                <div class="FeaturedNews__right">
                    <div class="FeaturedNews__image"
                         style="background-image: url(<?php echo Image::getSize($postFields['image'], 'medium_large'); ?>);"></div>
                    <div class="HoveringCutOff HoveringCutOff--pseudo-hover">
                        <div class="HoveringCutOff__cut-off"></div>
                        <div class="HoveringCutOff__inner-corner"></div>
                    </div>
                </div>
            </div>

            <div class="FeaturedNews__content" style="display: block;">
                <div class="News__row-content">

                    <div class="__content">
                        <?php echo $post->post_content ?>
                    </div>

                    <?php if (is_array($relatedNews) && !empty($relatedNews)): ?>

                    <div class="News__related">
                        <div class="News__related-head">Related News</div>
                        <div class="News__related-body">
                            <div class="News__related-body-inner">
                                <div class="__relatedNews">
                                    <?php foreach ($relatedNews as $index => $news): ?>
                                        <div class="News__related-item">
                                            <div class="row">
                                                <div class="col-xs-5">
                                                    <div class="News__related-image __relatedNews__<?php echo $index; ?>__image"
                                                         style="background-image: url(<?php echo $news['image']; ?>);"></div>
                                                    <div class="ThickLine ThickLine--stick-top ThickLine--thin ThickLine--short"></div>
                                                </div>
                                                <div class="col-xs-7">
                                                    <div class="News__related-title __relatedNews__<?php echo $index; ?>__title">
                                                        <?php echo $news['title']; ?>
                                                    </div>
                                                    <br/>
                                                    <!--
                                                            NOTE: Removed for now from client request.
                                                            <div class="News__related-date __relatedNews__<?php echo $index; ?>__date">
                                                            <?php echo $news['date']; ?>
                                                            </div>
                                                            -->
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>

                                </div>
                            </div>
                        </div>
                    </div>

                    <?php endif; ?>

                </div>
            </div>
            <div class="Gap"></div>
            <a class="LinkButton" href="<?php echo get_permalink($page_post) ?>">< News</a>
        </div>

    </div>
</div>
