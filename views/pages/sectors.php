<?php

global $expertises;

$services = [];

?>

<div class="Page ExpertisePage">
	<?php include_once __DIR__ . '/../components/hero-parallax.php'; ?>

    <main>
        <?php include_once __DIR__ . '/../sections/sectors_and_services.php'; ?>

        <div class="Gap visible-md visible-lg"></div>

		<?php include_once __DIR__ . '/../components/get-in-touch.php'; ?>

    </main>

	<?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>
