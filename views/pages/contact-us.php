<?php

$country = FixedValue::get('country');
$officeField = Field::get('office');
$officeField2 = Field::get('office2');
$officeField3 = Field::get('office3');
$officeField4 = Field::get('office4');

$contactFormOffices = array_map(function($officeField) {
    if (!$officeField) { return null; }

    $officeObj = Post::findByPost($officeField);
    if ($officeObj) {

        $weather = getWeatherInfo($officeObj['weather_city'],(get_site()->path == '/us/')?'fahrenheit':'kelvin');
        return [
            'title' => $officeObj['the_post']->post_title,
            'city' => $officeObj['city'],
            'timeZone' => $officeObj['time_zone'],
            'temp' => is_array($weather) ? ($weather['temp']) : '-',
            'email' => $officeObj['email'],
            'phone' => $officeObj['phone'],
            'shortCode' => $officeObj['short_code'],
            'mapImage' => [
                'default' => Image::getSize($officeObj['map_image'], 'fullsize'),
            ],
            'address' => $officeObj['address'],
        ];
    }
    return null;
}, [$officeField, $officeField2, $officeField3, $officeField4]);

$contactsField = Field::get('contact_persons');

$contacts = [];

if ($contactsField) {
    $n = 0;
    foreach ($contactsField as $field) {
        $contactObj = Post::findByPost($field);
        array_push($contacts, [
            'image' => [
                'default' => Image::getSize($contactObj['image'], 'medium'),
            ],
            'contact_title' => isset($contactObj['contact_title']) ? $contactObj['contact_title'] : '',
            'contact_title_secondary' => ($n<2) ?  FixedValue::get('contactUsTitles')[$n]  : '', // TODO : will probably need to be able to change this in CMS
            'name' => $contactObj['the_post']->post_title,
            'email' => $contactObj['email'],
            'linkedin' => $contactObj['linkedin'],
        ]);
        $n++;
    }
}

?>

<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>

<div class="Page Contact">
    <?php include_once __DIR__ . '/../components/hero-parallax.php'; ?>
    <main>
        <div data-component="ContactForm" data-params="<?php json([
            'country' => $country,
            'office' => $contactFormOffices[0] ?: null,
            'office2' => $contactFormOffices[1] ?: null,
            'office3' => $contactFormOffices[2] ?: null,
            'office4' => $contactFormOffices[3] ?: null,
            'contacts' => $contacts,
            'arrowImage' => asset('img/theme-arrow-down-yellow.png'),
            'formText' => [
                'title' => Field::get('form_elements.title'),
            ],
            'hubspot' => [
                'portal_id' => Field::get('hubspot.portal_id'),
                'form_id' => Field::get('hubspot.form_id'),
            ]
        ]); ?>"></div>

    </main>

    <?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>
