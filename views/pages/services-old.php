<?php
global $services;
?>

<div class="Page Services">
    <?php include_once __DIR__ . '/../components/hero-parallax.php'; ?>

    <main>

        <?php if (isset($services) && !is_null($services) && count($services) > 0): ?>
            <div data-component="ServicesPageSlider" data-params="<?php json([
                'title' => cf('our_services_title', 'Our Services'),
                'showSwitch' => true,
                'defaultSection' => 'service',
                'subtitles' => [
                    'serviceTitle' => cf('service_title', 'Service'),
                ],
                'serviceItems' => array_map(function($item) {

                    $imageUrl = $item['work_post']['image'];
                    $postId = isset($item['work_post']) && isset($item['work_post']->ID) ? $item['work_post']->ID : (
                        isset($item['work']) && isset($item['work']->ID) ? $item['work']->ID : ''
                    );
                    $workUrl = get_permalink($postId);

                    return [
                        'icon' => isset($item['icon']) ? $item['icon'] : '',
                        'title' => $item['title'],
                        'description' => $item['description'],
                        'person' => $item['person'] ?array_merge($item['person'], [
                            'image' => [
                                'default' => Image::getSize($item['person']['image'], 'medium'),
                            ],
                        ]) : (object)[],
                        'imageSlider' => [
                            'image' => [
                                'default' => $imageUrl ? Image::getSize($imageUrl, 'medium_large') : null,
                            ],
                            'image_title' => $item['work_post']['title'],
                            'url' => $workUrl,
                        ],
                        'slug' => $item['the_post']->post_name
                    ];
                }, $services),
                'slideArrows' => [
                    'prev' => asset('img/expertises/arrow_prev.png'),
                    'next' => asset('img/expertises/arrow_next.png'),
                ],
            ]);?>"></div>
        <?php endif; ?>

        <div class="Gap visible-md visible-lg"></div>

        <?php include_once __DIR__ . '/../components/get-in-touch.php'; ?>

    </main>

    <?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>
