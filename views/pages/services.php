<?php
$countryCode = FixedValue::get('country');
$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri_segments = explode('/', $uri_path);


global $services;

array_map(function($item) {

    $imageUrl = $item['work_post']['image'];
    $postId = isset($item['work_post']) && isset($item['work_post']->ID) ? $item['work_post']->ID : (
    isset($item['work']) && isset($item['work']->ID) ? $item['work']->ID : ''
    );
    $workUrl = get_permalink($postId);

    return [
        'icon' => isset($item['icon']) ? $item['icon'] : '',
        'title' => $item['title'],
        'description' => $item['description'],
        'person' => $item['person'] ?array_merge($item['person'], [
            'image' => [
                'default' => Image::getSize($item['person']['image'], 'medium'),
            ],
        ]) : (object)[],
        'imageSlider' => [
            'image' => [
                'default' => $imageUrl ? Image::getSize($imageUrl, 'medium_large') : null,
            ],
            'image_title' => $item['work_post']['title'],
            'url' => $workUrl,
        ],
        'slug' => $item['the_post']->post_name,
        'work_post' => $item['work_post']
    ];
}, $services);

?>

<div class="Page Services">
    <?php include_once __DIR__ . '/../components/hero-parallax.php'; ?>

    <main>
        <?php if (isset($services) && !is_null($services) && count($services) > 0): ?>

        <?php foreach($services as $key => $service) {?>
            <div id="popup<?php echo $key ?>" class="Modal__box<?php echo (!empty($service['work_post']))?' Modal__box--big':''?>" data-pop-up="<?php echo $service['the_post']->post_name?>">
                <div class="Modal__close"><img src="<?php echo get_bloginfo( 'template_directory' ); ?>/public/img/nav/x.png" class="img-responsive"> </div>

                <div class="Modal__wrapper">

                        <div class="row same-height-columns mt-55px">
                            <div class="col-xs-12<?php echo (!empty($service['work_post']))?' col-md-6':''?>">
                                <div class="Modal__left-column">
                                    <div class="Modal__header">
                                        <div class="Modal__icon">
                                            <img src="<?php echo $service['icon']; ?>" class="Modal__icon">
                                        </div>
                                        <div class="Modal__title"><?php echo $service['title']?></div>
                                    </div>
                                    <div class="Modal__body">
                                        <?php echo $service['description']?>
                                    </div>
                                    <div class="Modal__footer<?php echo (!empty($service['work_post']))?' mt-auto':''?>">
                                        <div class="Modal__profile">
                                            <img src="<?php echo $service['person']['image'] ?>">
                                        </div>
                                        <div class="Modal__profile_description">
                                            <?php echo $service['person']['name'] ?> <br/>
                                            <?php echo $service['person']['job'] ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12<?php echo (!empty($service['work_post']))?' col-md-6':''?>">
                                <?php if(!empty($service['work_post'])) :?>

                                <?php foreach($service['work_post'] as $work) {?>

                                        <div class="ModalCaseStudy" style="padding-bottom: 20px">
                                            <a href="<?php echo get_permalink($work['the_post']->ID) ?>">
                                                <div class="ModalCaseStudy__wrapper">
                                                    <div class="ModalCaseStudy__preview">
                                                        <div class="ModalCaseStudy__border"></div>
                                                        <div class="ModalCaseStudy__title"><?php echo $work['title'] ?></div>

                                                        <div class="ModalCaseStudy__more">+</div>
                                                    </div>
                                                    <div class="ModalCaseStudy__background">

                                                        <img src="<?php echo $work['image']?>" class="ModalCaseStudy__element_img">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                <?php } endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
        <?php } endif; ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-2 text-left"><h3 class="section__title section__title--left">Our <br/>Services</h3></div>
            </div>
        </div>
        <div class="Services__elements">

            <div class="container">
                <div class="row row-equal-columns">
                    <?php if (isset($services) && !is_null($services) && count($services) > 0): ?>

                    <?php foreach($services as $key => $service) {?>
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <?php
                                $modal_data = 'data-modal-id="popup'.$key.'"';
                                if($uri_segments[1] === 'us'){
                                    $modal_data = '';
                                    echo "
                                    <style>
                                        .Service__wrapper:hover .Service__preview::after {
                                            transition: none !important;
                                        }
                                        .Service__wrapper:hover .Service__element_img {

                                            transform: none !important;
                                        }
                                    </style>
                                    ";
                                }
                                ?>
                                <div class="Service" <?php echo $modal_data ?> data-slug="<?php echo $service['the_post']->post_name?>">

                                    <div class="Service__wrapper" style="cursor: default !important;">
                                        <div class="Service__preview">
                                            <div class="Service__container">
                                                <div class="Service__title"><?php echo $service['title']?></div>
                                                <div class="Service__icon">
                                                    <img src="<?php echo $service['icon']; ?>" class="">
                                                </div>
                                                <?php if($uri_segments[1] !== 'us') {?>
                                                    <div class="Service__more" style="z-index: 20">Find out more</div>
                                                <?php }else{ ?>
                                                    <div class="Service__more" style="z-index: 20"></div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <div class="Service__background">
                                            <img src="<?php echo $service['background']?>" class="Service__element_img">
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php } endif; ?>
                </div>
            </div>
        </div>

        <div class="Gap visible-md visible-lg"></div>

        <?php include_once __DIR__ . '/../components/get-in-touch.php'; ?>

    </main>

    <?php include_once __DIR__ . '/../components/footer.php'; ?>
</div>
