<?php
global $currentPage, $expertises, $services;
$expertisesRotator = array_slice($expertises, 0, 6);

if ($currentPage === 'home') {
    global $serviceIntro;
}

array_map(function($item) {

    $imageUrl = $item['work_post']['image'];
    $postId = isset($item['work_post']) && isset($item['work_post']->ID) ? $item['work_post']->ID : (
    isset($item['work']) && isset($item['work']->ID) ? $item['work']->ID : ''
    );
    $workUrl = get_permalink($postId);



    return [
        'icon' => isset($item['icon']) ? $item['icon'] : '',
        'title' => $item['title'],
        'description' => $item['description'],
        'person' => $item['person'] ?array_merge($item['person'], [
            'image' => [
                'default' => Image::getSize($item['person']['image'], 'medium'),
            ],
        ]) : (object)[],
        'imageSlider' => [
            'image' => [
                'default' => $imageUrl ? Image::getSize($imageUrl, 'medium_large') : null,
            ],
            'image_title' => $item['work_post']['title'],
            'url' => $workUrl,
        ],
        'slug' => $item['the_post']->post_name
    ];
}, $expertises);

?>

<?php if ($currentPage === 'global'): ?>
    <div data-component="ExpertiseRotator" data-params="<?php json([
        'title' => cf('expertise_title', 'Expertise'),
        'mobileShowSwitch' => true,
        'showSwitch' => true,
        'defaultSection' => 'service',
        'subtitles' => [
            'serviceTitle' => cf('service_title', 'Service'),
            'sectorTitle' => cf('sector_title', 'Sector'),
        ],
        'currentPage' => 'global',
        'serviceItems' => array_map(function ($item) {
            return [
                'icon' => isset($item['icon']) ? $item['icon'] : '',
                'title' => $item['title'],
                'description' => $item['description'],
                'person' =>
                    isset($item['person']) ? array_merge($item['person'], [
                        'image' => [
                            'md' => Image::getSize($item['person']['image'], 'medium_large'),
                            'default' => Image::getSize($item['person']['image'], 'medium'),
                        ]
                    ]) : (object)[],
            ];
        }, $services),
        'sectorItems' => array_map(function ($item) {

            $clientLogos = array_map(function($item) {
                return Image::getSize($item['image'], 'medium');
            }, $item['image']['images']);

            return [
                'icon' => isset($item['icon']) ? $item['icon'] : '',
                'title' => $item['title'],
                'description' => $item['description'],
                'clientExperience' => [
                    'title' => $item['image']['title'],
                    //'images' => $clientLogos,
                ],
            ];
        }, $expertisesRotator),
        'slideArrows' => [
            'prev' => asset('img/expertises/arrow_prev.png'),
            'next' => asset('img/expertises/arrow_next.png'),
        ],
        'extendBottom' => true,
        'pushBottom' => true,
    ]); ?>"></div>
<?php endif; ?>

<?php if ($currentPage === 'home'): ?>
    <?php if (is_array($services) && !empty($services)): ?>
        <div data-component="ExpertiseRotator" data-params="<?php json([
            'title' => cf('service_title', 'Service'),
            'showSwitch' => false,
            'descriptionAbove' => true,
            'defaultSection' => 'service',
            'subtitles' => [
                'serviceTitle' => cf('service_title', 'Service'),
                'sectorTitle' => cf('sector_title', 'Sector'),
            ],
            'currentPage' => 'home',
            'serviceItems' => array_map(function ($item) use ($serviceIntro) {
                return [
                    'icon' => isset($item['icon']) ? $item['icon'] : '',
                    'title' => $item['title'],
                    'description' => strip_tags($serviceIntro), // NOTE : this content is always fixed
                    'readMore' => [
                        'text' => FixedValue::get('readMore') . html_entity_decode(' &gt; &nbsp;&nbsp;&nbsp;&nbsp;'),
                        'link' => url('') . 'services/#' . $item['the_post']->post_name
                    ],
                    'slug' => $item['the_post']->post_name
                ];
            }, $services),
            'slideArrows' => [
                'prev' => asset('img/expertises/arrow_prev.png'),
                'next' => asset('img/expertises/arrow_next.png'),
            ],
            'extendBottom' => true,
            'pushBottom' => true,
        ]); ?>"></div>
    <?php endif; ?>
<?php endif; ?>

<?php if ($currentPage === 'sectors'): ?>
    <?php foreach($expertises as $key => $service) :?>
        <div id="popup<?php echo $key ?>" class="Modal__box" data-pop-up="<?php echo $service['the_post']->post_name?>">
            <div class="Modal__wrapper">
                <div class="Modal__close"><img src="<?php echo get_bloginfo( 'template_directory' ); ?>/public/img/nav/x.png" class="img-responsive"> </div>
                <div class="Modal__header">
                    <div class="Modal__icon_wrapper">
                        <img src="<?php echo $service['icon']; ?>" class="Modal__icon">
                    </div>
                    <div class="Modal__title"><?php echo $service['title']?></div>
                </div>
                <div class="Modal__body">
                    <?php echo $service['description']?>
                </div>
                <?php if(!empty($service['person'])) :?>
                    <div class="Modal__footer">
                        <div class="Modal__profile">
                            <img src="<?php echo $service['person']['image'] ?>">
                        </div>
                        <div class="Modal__profile_description">
                            <?php echo $service['person']['name'] ?> <br/>
                            <?php echo $service['person']['job'] ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>
    <div class="Services__elements">

        <div class="container">
            <div class="row row-equal-columns">
                <?php foreach($expertises as $key => $service): ?>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="Service" data-modal-id="popup<?php echo $key ?>" data-slug="<?php echo $service['the_post']->post_name?>">
                            <div class="Service__wrapper">
                                <div class="Service__preview">
                                    <div class="Service__container">
                                        <div class="Service__title"><?php echo $service['title']?></div>
                                        <div class="Service__icon Service__icon--type2">
                                            <img src="<?php echo $service['icon']; ?>" class="">
                                        </div>
                                        <div class="Service__more" style="z-index: 20">Find out more</div>
                                    </div>
                                </div>
                                <div class="Service__background">
                                    <img src="<?php echo $service['background']?>" class="Service__element_img">
                                </div>
                            </div>
                        </div>
                    </div>
                <?php  endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
