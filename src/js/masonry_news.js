import breakpoints from "./Utils/breakpoints";

class MasonryNews {

    constructor(props) {
        this.initialised = false;
        this.$container = null;
        this.$template = null;
        this.previousActive = null;
        this.items = [];

        this.$container = $(props.holder);
        this.$template = $(props.template);
        this.items = props.items;
        this.itemSelector = props.itemSelector;
        this.itemPreviewSelector = props.itemPreviewSelector;
        this.itemContentSelector = props.itemContentSelector;
        this.clickHandler = props.clickHandler;
        this.closeHandler = props.closeHandler;
        this.firstElement = null;
        this.firstElementPosTop = null;
        this.documentContainer = null;
        this.scrollable = (!(props.scrollable === false));
        // this.scrollable = true;

        this.appendItems();

        let $grid = this.$container.masonry({
            columnWidth: 1,
            initLayout: false,
        });

        $grid.masonry('on', 'layoutComplete', () => {
            if (!this.initialised) {
                this.initialised = true;
                this.rearangeItems(null);
            }
        });

        $grid.masonry();

        window.addEventListener('news-filter-category', event => {
            this.filterByCategory(event.value);
        });

        window.addEventListener('resize', event => {
            const top = this.firstElement.getBoundingClientRect().y + this.documentContainer.scrollTop;
            if (this.firstElement !== null && this.firstElementPosTop !== top) {
                this.firstElementPosTop = top;
                this.rearangeItems(null);
            }
        });
    }

    static getColumns() {
        const {md} = breakpoints;
        if (window.innerWidth < 600) { // custom breakpoint
            return 1;
        } else if (window.innerWidth >= md) {
            return 4;
        }
        return 2;
    }

    filterByCategory(category) {
        this.$container.find('.NewsMasonry__item').map(function () {
            if (category === '') {
                $(this).show();
                return;
            }

            // If the element's category has a category
            // that matches, then hide the element.
            if ($(this).data('category').split(' ').indexOf(category) > -1) {
                $(this).hide();
            }
        });

        this.rearangeItems(null);
    }

    appendItems() {
        for (let i = 0; i < this.items.length; i++) {
            let $cpy = this.$template.children().first().clone();

            $cpy.attr('data-category', this.items[i].category);

            $cpy.attr('data-orig', i);
            $cpy.find(this.clickHandler).click(el => this.openElement(el));
            $cpy.find(this.closeHandler).click(el => this.closeElement(el,));

            for (let key in this.items[i]) {
                if (this.items[i].hasOwnProperty(key)) {

                    let val = this.items[i][key];
                    let $el = $cpy.find('.__' + key);

                    if (typeof val === 'object') {
                        for (let j = 0; j < val.length; j++) {
                            for (let subKey in val[j]) {

                                let subVal = val[j][subKey];
                                let $subEl = $el.find(`.__${key}__${j}__${subKey}`).first();

                                if (val[j].hasOwnProperty(subKey)) {
                                    if (subKey === 'image') {
                                        $subEl.css('background-image', 'url(' + subVal + ')');
                                    } else {
                                        $subEl.html(subVal);
                                    }
                                }
                            }
                        }
                    } else {
                        if (key === 'image') {
                            $el.css('background-image', 'url(' + val + ')');
                        } else {
                            $el.html(val);
                        }
                    }
                }
            }

            this.$container.append($cpy);

            if (this.firstElement === null) {
                this.documentContainer = document.querySelector('#container');
                this.firstElement = document.querySelector('.NewsMasonry');
                this.firstElementPosTop = 0;
            }

        }
    }

    openElement(el) {
        const $target = $(el.target).closest(this.itemSelector);
        const isOpen = $target.hasClass('open');

        this.$container.find('.NewsMasonry__item').map((index, item) => {
            if (isOpen) this.closeElement(item, true);
            else this.closeElement(item, false);
        });

        if (!isOpen) {
            const $targetPreview = $target.find(this.itemPreviewSelector);
            const $targetContent = $target.find(this.itemContentSelector);
            $target.addClass('open');
            $targetContent.addClass('open');
            $targetPreview.addClass('open');
        }

        this.newOrder(parseInt($target.attr('data-orig')));
        this.rearangeItems($target);
    }

    closeElement(el, clicked = true) {
        const $target = $(el.target).closest(this.itemSelector);
        let has = false;

        if ($(this.itemSelector).hasClass('open')) {
            $(this.itemSelector).removeClass('open');
            $(this.itemPreviewSelector).removeClass('open');
            $(this.itemContentSelector).removeClass('open');
            this.lastOpened = null;
            has = true;
        }

        //this.newOrder(parseInt($target.attr('data-orig')),);
        if (clicked) this.rearangeItems($target, has);
    }

    newOrder(active) {
        let children = this.$container.children();

        if (this.previousActive % MasonryNews.getColumns() !== 0) {
            let current = this.previousActive + (MasonryNews.getColumns() - this.previousActive % MasonryNews.getColumns());
            let target = $(children[current]).attr('data-orig') - 1;
            $(children[current]).insertAfter($(children[target]));
        }

        children = this.$container.children();

        if (active % MasonryNews.getColumns() !== 0) {
            let target = active + (MasonryNews.getColumns() - active % MasonryNews.getColumns());
            $(children[active]).insertAfter($(children[target]));
        }

        this.previousActive = parseInt(active);
        this.rearangeItems(null);
    }

    rearangeItems(target, isClose = false) {
        setTimeout(() => {
            this.$container.masonry('reloadItems');
            this.$container.masonry('layout');
        }, 1);

        if (isClose && this.scrollable) {
            setTimeout(() => {
                let targetTop = this.documentContainer.scrollTop + $('.FeaturedNews').first().position().top + $('.FeaturedNews').first().offset().top;
                let extra = 0;

                if (window.innerWidth > 992) {
                    extra = 15
                }

                $('#container').first().animate({
                    scrollTop: targetTop + extra
                }, 300, () => {
                    this.rearangeItems(null)
                });
            }, 300);
        } else {
            if (target !== null && target.hasClass('open') && this.scrollable) {
                setTimeout(() => {
                    let headerTop = $('.Header')[0].getBoundingClientRect().height;
                    let targetTop = target.position().top;
                    let introTop = $('.News__intro').height();
                    let featuredNewsTop = $('.FeaturedNews').height();
                    let extra = 0;
                    if (window.innerWidth > 992) {
                        extra = 15
                    }

                    $('#container').first().animate({
                        scrollTop: headerTop + targetTop + introTop + featuredNewsTop - extra
                    }, () => {
                        this.rearangeItems(null)
                    });
                }, 300);
            }
        }
    }
}

window.MasonryNews = MasonryNews;