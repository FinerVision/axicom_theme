class Carousel {

    static init() {
        const heroCarousel = document.querySelector('.Carousel--hero');
        if (heroCarousel) {
            new Carousel({slides: heroCarousel.querySelector('.Carousel__slides').children});
        }

        const stackedCarousel = document.querySelector('.Carousel--stacked');
        if (stackedCarousel) {
            new Carousel({slides: stackedCarousel.querySelector('.Carousel__slides').children, type: 'stacked', delay: 3000});
        }

    }

    constructor(settings = {}) {
        this.settings = Object.assign({
            delay: 5000,
            slides: [],
            type: 'default'
        }, settings);

        this.lastUpdateTimestamp = +new Date;
        this.activeIndex = 0;
        this.slides = this.settings.slides;
        setTimeout(()=>{
            this.update();
            this.handleStackedType();
        },1500);
    }

    shouldUpdate() {
        let nowTimestamp = +new Date;
        const canUpdate = nowTimestamp - this.lastUpdateTimestamp >= this.settings.delay;
        if (canUpdate) {
            this.lastUpdateTimestamp = nowTimestamp;
        }
        return canUpdate && this.slides.length > 0;
    }

    handleStackedType() {
        if (this.settings.type !== 'stacked') {
            return;
        }

        const slidesToPreview = 2;
        for (let i = 0; i < this.slides.length; i++) {
            for (let j = 1; j <= slidesToPreview; j++) {
                this.slides[i].classList.remove(`next-${j}`);
                this.slides[i].classList.remove(`prev-${j}`);
            }
        }

        const nextSlideIndex = this.activeIndex + 1 > this.slides.length - 1 ? 0 : this.activeIndex + 1;
        const prevSlideIndex = this.activeIndex - 1 < 0 ? this.slides.length - 1 : this.activeIndex - 1;

        this.slides[nextSlideIndex].classList.add('next-1');
        this.slides[nextSlideIndex + 1 > this.slides.length - 1 ? 0 : nextSlideIndex + 1].classList.add('next-2');

        this.slides[prevSlideIndex].classList.add('prev-1');
        this.slides[prevSlideIndex - 1 < 0 ? this.slides.length - 1 : prevSlideIndex - 1].classList.add('prev-2');
    }

    update() {
        requestAnimationFrame(() => this.update());

        if (!this.shouldUpdate()) {
            return;
        }

        const newActiveIndex = this.activeIndex >= this.slides.length - 1 ? 0 : this.activeIndex + 1;
        const classNamesToRemove = ['out', 'in', 'active', 'next', 'prev'];

        for (let i = 0; i < this.slides.length; i++) {
            classNamesToRemove.map(className => {
                this.slides[i].classList.remove(className);
            });
        }

        this.slides[this.activeIndex].classList.add('out');
        this.slides[newActiveIndex].classList.add('in');
        this.slides[newActiveIndex].classList.add('active');

        this.activeIndex = newActiveIndex;

        this.handleStackedType();
    }
}

// if the page is preloading, should call the init after the page has loaded
if (typeof window.pageHasPreloading === 'undefined' || !window.pageHasPreloading) {
    Carousel.init();
}

window.addEventListener('preloadfinish', () => Carousel.init());
