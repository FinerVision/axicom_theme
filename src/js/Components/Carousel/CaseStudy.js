import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import Slick from "react-slick";

import breakpointImages from "../../Utils/breakpointImage";

const PrevArrow = props => <div
    className={`${props.className || ''} CaseStudyCarousel__arrow CaseStudyCarousel__arrow--prev`}
    onClick={props.onClick}/>;
const NextArrow = props => <div
    className={`${props.className || ''} CaseStudyCarousel__arrow CaseStudyCarousel__arrow--next`}
    onClick={props.onClick}/>;

const PADDING = 15;
const TRANSITION_SPEED = 500;
const SLICK_OPTIONS = {
    dots: false,
    arrows: false,
    infinite: false,
    centerMode: false,
    variableWidth: true,
    useTransform: true,
    speed: TRANSITION_SPEED,
    slidesToShow: 1,
    slidesToScroll: 1
};

const triggerWindowResize = () => {
    if ('createEvent' in document) {
        const event = document.createEvent('HTMLEvents');
        event.initEvent('resize', false, true);
        window.dispatchEvent(event);
    } else {
        window.fireEvent('resize');
    }
};

class CaseStudyCarousel extends Component {
    constructor(props) {
        super(props);

        const initialSlide = this.props.items.length < 2 ? 0 : Math.floor(this.props.items.length / 2);

        this.state = {
            activeIndex: initialSlide,
            showMore: false,
            transitioning: false,
            initialSlide,
            windowWidth: window.innerWidth
        };
    }

    componentDidMount() {
        (function update() {
            requestAnimationFrame(update);
            triggerWindowResize();
        })();

        this.setState({windowWidth: window.innerWidth});
        window.addEventListener('resize', () => this.setState({windowWidth: window.innerWidth}));
    }

    handleChange(currentSlide, nextSlide) {
        this.setState({activeIndex: nextSlide, showMore: false, transitioning: true}, () => {
            setTimeout(() => {
                this.setState({transitioning: false});
            }, TRANSITION_SPEED);
        });
    }

    openItem(event, index) {
        if (this.state.transitioning || event.target.className === 'CaseStudyCarousel__slide__close') {
            return;
        }
        this.refs.carousel.slickGoTo(index);
        this.setState({showMore: true});
    }

    closeItem() {
        if (!this.state.showMore) {
            return;
        }
        this.setState({showMore: false});
    }

    toggleItem(event, index) {
        if (this.state.showMore) {
            this.closeItem();

            if (this.state.activeIndex !== index) {
                this.openItem(event, index);
                return;
            }

            this.setState({showMore: false});
            return;
        }

        this.openItem(event, index);
    }

    getSlideStyle() {
        const style = {};
        if (this.state.windowWidth >= 1000) {
            return {};
        }

        style.maxWidth = `${this.state.windowWidth}px`;

        if (this.state.showMore) {
            style.maxWidth = `${this.state.windowWidth}px`;

            const slideHeight = Math.ceil(this.refs[`slide-${this.state.activeIndex}`].getBoundingClientRect().height);
            const contentHeight = Math.ceil(this.refs[`expanded-content-${this.state.activeIndex}`].getBoundingClientRect().height);

            if (slideHeight <= contentHeight + PADDING && !(this.state.windowWidth <= 991)) {
                style.height = `${contentHeight + PADDING}px`;
            }
        }

        return style;
    }

    getSlideImage(item) {
        if (!item.image) {
            return null;
        }

        return <div className="CaseStudyCarousel__slide__img" style={{backgroundImage: `url(${breakpointImages(item.image)})`}}/>;
    }

    renderSlickItems() {
        const {remarkText} = this.props;
        return this.props.items.map((item, index) => (
            <div
                key={index}
                ref={`slide-${index}`}
                className={`
                    CaseStudyCarousel__slide
                    ${this.state.activeIndex === index ? 'CaseStudyCarousel__slide--active' : ''}
                `}
                onClick={event => this.toggleItem(event, index)}
                style={this.getSlideStyle()}
            >
                {this.getSlideImage(item)}
                <div className="CaseStudyCarousel__slide__overlay" style={{backgroundImage: `url(${item.overlay})`}}/>
                <div className="CaseStudyCarousel__slide__content CaseStudyCarousel__slide__content--preview">
                    <h4>{item.title}</h4>
                    <div className="CaseStudyCarousel__slide__content__banner-title">{item.bannerHeaderTitle}</div>
                    <a className="CaseStudyCarousel__slide__content--read-more" href={item.link}
                       onClick={event => event.preventDefault()}>{this.props.readMoreText.replace(/\.+?$/,'')}</a>
                </div>

                <div
                    ref={`expanded-content-${index}`}
                    className="CaseStudyCarousel__slide__content CaseStudyCarousel__slide__content--expanded"
                >
                    <h4>{item.title}</h4>
                    <div
                        className="CaseStudyCarousel__slide__content__content"
                        dangerouslySetInnerHTML={{__html: item.content}}
                    />
                    {
                        item.remarkText && (
                            <div className="CaseStudyCarousel__slide__content__remarks">{item.remarkText}</div>
                        )
                    }
                    {
                        item.linkText && (
                            <a href={item.link}>
                                <div className="CaseStudyCarousel__slide__content__remarks">{item.linkText}</div>
                            </a>
                        )
                    }
                </div>
            </div>
        ));
    }

    render() {
        return (
            <div className={`CaseStudyCarousel ${this.state.showMore ? 'CaseStudyCarousel--show-more' : ''}`}>
                <div className="CaseStudyCarousel__title">
                    <h3 className="section__title">
                        {this.props.title}
                    </h3>

                    <div className="CaseStudyCarousel__dots">
                        {this.props.items.map((item, index) => (
                            <div
                                key={index}
                                className={`
                                    CaseStudyCarousel__dots__dot
                                    ${index === this.state.activeIndex ? 'CaseStudyCarousel__dots__dot--active' : ''}
                                `}
                                onClick={() => this.refs.carousel.slickGoTo(index)}
                            />
                        ))}
                    </div>
                </div>


                {this.state.activeIndex === 0 ? null : <PrevArrow
                    onClick={() => this.refs.carousel.slickPrev()}
                    className="visible-md visible-lg"
                />}
                <div className="CaseStudyCarousel__inner">
                    {this.state.activeIndex === 0 ? null : <PrevArrow
                        onClick={() => this.refs.carousel.slickPrev()}
                        className="hidden-md hidden-lg"
                    />}
                    <Slick
                        ref="carousel"
                        {...SLICK_OPTIONS}
                        initialSlide={this.state.initialSlide}
                        beforeChange={(currentSlide, nextSlide) => this.handleChange(currentSlide, nextSlide)}
                    >
                        {this.renderSlickItems()}
                    </Slick>
                    {this.state.activeIndex === this.props.items.length - 1 ? null : <NextArrow
                        onClick={() => this.refs.carousel.slickNext()}
                        className="hidden-md hidden-lg"
                    />}
                </div>

                {this.state.activeIndex === this.props.items.length - 1 ? null : <NextArrow
                    onClick={() => this.refs.carousel.slickNext()}
                    className="visible-md visible-lg"
                />}

            </div>
        );
    }
}

CaseStudyCarousel.propTypes = {
    title: PropTypes.string,
    readMoreText: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.shape({
        image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
        overlay: PropTypes.string,
        title: PropTypes.string,
        content: PropTypes.string,
        subtitle: PropTypes.string,
        bannerHeaderTitle: PropTypes.string,
        link: PropTypes.string,
        remarkText: PropTypes.string,
    })),
};

CaseStudyCarousel.defaultProps = {
    readMoreText: 'Read more',
};

const nodes = document.querySelectorAll('[data-component="CaseStudyCarousel"]');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const props = JSON.parse(node.getAttribute('data-props'));
    render(<CaseStudyCarousel {...props}/>, node);
}
