import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";

class PaginatedList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            page: 1
        };
    }

    getPages() {
        const pages = [];

        for (let page = 1; page <= Math.ceil(this.props.items.length / this.props.perPage); page++) {
            pages.push(page);
        }

        return pages;
    }

    changePage(page) {
        this.setState({page});
    }

    renderItems() {
        const {page} = this.state;
        const {items, perPage} = this.props;

        const startIndex = perPage * (page - 1);
        const endIndex = perPage * page;
        const itemsToRender = items.slice(startIndex, endIndex);

        return itemsToRender.map((item, index) => {
            return <span key={index} dangerouslySetInnerHTML={{__html: item}}/>
        });
    }

    renderPagination() {
        if (this.props.items.length <= this.props.perPage) {
            return null;
        }

        const pages = this.getPages();

        return (
            <ul className="PaginatedList__pagination">
                {pages.map((page, index) => (
                    <li
                        key={index}
                        className={`
                            PaginatedList__pagination-link
                            ${this.state.page === page ? 'PaginatedList__pagination-link--active' : ''}
                        `}
                        onClick={() => this.changePage(page)}
                    />
                ))}
            </ul>
        );
    }

    render() {
        return (
            <div className="PaginatedList">
                {this.renderItems()}
                {this.renderPagination()}
            </div>
        );
    }
}

PaginatedList.defaultProps = {
    perPage: 10
};

PaginatedList.propTypes = {
    items: PropTypes.array.isRequired,
    perPage: PropTypes.number,
};

const nodes = document.querySelectorAll('[data-component="paginated-list"]');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const props = JSON.parse(node.getAttribute('data-props'));
    render(<PaginatedList {...props}/>, node);
}
