import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import classNames from "classnames";

import breakpointImage from "../Utils/breakpointImage";

class NewsCard extends Component {

    constructor(props) {
        super(props);

        this.touchStartTime = null;

        // binding
        this.onItemTouchStart = this.onItemTouchStart.bind(this);
        this.onItemTouchEnd = this.onItemTouchEnd.bind(this);
    }

    componentDidMount() {
        this.updateHeight();
    }

    componentDidUpdate() {
        this.updateHeight();
    }

    updateHeight() {
        const {expend, root} = this.refs;
        switch(this.props.expendPosition) {
            case NewsCard.EXPEND_BOTTOM:
                return this.height = root.getBoundingClientRect().height + expend.getBoundingClientRect().height;

            case NewsCard.EXPEND_RIGHT:
            default:
                if (this.props.isExpending) {
                    return this.height = Math.max(root.getBoundingClientRect().height, expend.getBoundingClientRect().height + 10);
                } else {
                    return this.height = root.getBoundingClientRect().height;
                }
        }
    }

    onItemTouchStart() {
        this.touchStartTime = (+(new Date()));
    }

    onItemTouchEnd() {
        const now = +(new Date());
        if (now - this.touchStartTime < 50) {
            this.props.onActivate();
        }
        this.touchStartTime = null;
    }

    getRelatedNews() {
        const {relatedNews} = this.props;

        if (relatedNews.length === 0) {
            return null;
        }

        return (
            <div className="NewsCard__related">
                <div className="NewsCard__related-inner">
                    {
                        relatedNews.map((newsItem, index) => (
                            <div
                                key={index}
                                className="NewsCard__related-item"
                                onClick={() => this.props.onRelatedNewsActivate(newsItem)}
                            >
                                <div className="row">
                                    <div className="col-md-5">
                                        <div
                                            className="NewsCard__related-image"
                                            style={{
                                                backgroundImage: `url(${breakpointImage(newsItem.image)})`,
                                            }}
                                        />
                                        <div className="ThickLine ThickLine--stick-top ThickLine--thin ThickLine--short" />
                                    </div>
                                    <div className="col-md-7">
                                        <div className="NewsCard__related-title">{newsItem.title}</div>
                                        <br />
                                    </div>
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>
        );
    }

    getOnClickCallbackFunction() {
        if (['ios', 'crios', 'fxios', 'ios-webview'].indexOf(window.browser && window.browser.name) !== -1) {
            return {
                onTouchStart: this.onItemTouchStart,
                onTouchEnd: this.onItemTouchEnd,
            };
        }
        return { onClick: !this.props.isExpending ? this.props.onActivate : this.props.onDeactivate };
    }

    render() {
        const {expendPosition} = this.props;

        return (
            <div className={classNames(
                'NewsCard',
                {
                    'NewsCard--right': expendPosition === NewsCard.EXPEND_RIGHT,
                    'NewsCard--bottom': expendPosition === NewsCard.EXPEND_BOTTOM,
                }
            )} ref="root">
                <div className="NewsCard__image-wrap">
                    <div
                        className={(this.props.disableHoverAnimation === false || this.props.disableHoverAnimation === undefined)?'NewsCard__image':'NewsCard__image NewsCard__image--no-animation'}
                        {...this.getOnClickCallbackFunction()}
                    >
                        <div
                            className="NewsCard__image-inner"
                            style={{
                                backgroundImage: `url(${breakpointImage(this.props.image)})`,
                            }}
                        />
                        <div className="HoveringCutOff HoveringCutOff--small">
                            <div className="HoveringCutOff__cut-off" />
                            <div className="HoveringCutOff__inner">{this.props.category}</div>
                        </div>
                    </div>
                </div>
                <div className="ThickLine ThickLine--stick" />
                <br/>
                <div className="NewsCard__title">{this.props.title}</div>
                <br/>
                <div
                    ref="expend"
                    className={classNames(
                        'NewsCard__expend',
                        {
                            'NewsCard__expend--active': this.props.isExpending,
                        },
                    )}
                    style={{
                        width: `${this.props.expendWidth * 100}%`,
                    }}
                >
                    <div className="NewsCard__expend-inner">
                        <div
                            className="NewsCard__expend-close"
                            onClick={this.props.onDeactivate}
                        >&times;</div>
                        <div
                            className="NewsCard__expend-content"
                        >
                        </div>
                        <div className="Gap" />
                        {this.getRelatedNews()}
                    </div>
                </div>
            </div>
        );
    }
}

NewsCard.EXPEND_RIGHT = 'right';
NewsCard.EXPEND_BOTTOM = 'bottom';

NewsCard.propTypes = {
    // text
    image: PropTypes.object,
    category: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    relatedNews: PropTypes.arrayOf(PropTypes.shape({
        image: PropTypes.object,
        title: PropTypes.string.isRequired,
        category: PropTypes.string.isRequired,
        date: PropTypes.string,
    })),
    // numbers
    isExpending: PropTypes.bool,
    expendPosition: PropTypes.oneOf([
        NewsCard.EXPEND_RIGHT,
        NewsCard.EXPEND_BOTTOM,
    ]),
    expendWidth: PropTypes.number,
    onActivate: PropTypes.func,
    onDeactivate: PropTypes.func,
    onRelatedNewsActivate: PropTypes.func,
};

NewsCard.defaultProps = {
    isExpending: false,
    expendPosition: NewsCard.EXPEND_RIGHT,
    expendWidth: 1,
    onActivate: () => {},
    onDeactivate: () => {},
    onRelatedNewsActivate: () => {},
};

export default NewsCard;

const nodes = document.querySelectorAll('[data-component=NewsCard]');
for(let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const params = JSON.parse(node.getAttribute('data-params') || {});
    render(<NewsCard {...params} />, node);
}
