import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import Slick from "react-slick";

const SLICK_OPTIONS = {
    dots: true,
    arrows: false,
    autoplaySpeed: 10000,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
        {
            breakpoint: 991, // sm max
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: true,
                dots: false,
            },
        }
    ],
};

class NewsCardSlider extends Component {
    renderSlickSlides() {
        return this.props.items.map((item, index) => (
            <div key={index}>
                <div className="NewsCard">
                    <div className="NewsCard__image-wrap">
                        <div className="NewsCard__image">
                            <div className="NewsCard__image-inner" style={{backgroundImage: `url(${item.image})`}}/>
                            <div className="HoveringCutOff  HoveringCutOff--small">
                                <div className="HoveringCutOff__cut-off"/>
                                <div className="HoveringCutOff__inner"
                                     dangerouslySetInnerHTML={{__html: item.category.replace(' ', '<br />')}}/>
                            </div>
                        </div>
                    </div>
                    <div className="ThickLine ThickLine--stick-top"/>
                    <br/>
                    <div className="NewsCard__title">
                        {item.title}
                    </div>
                </div>
            </div>
        ));

    }

    render() {
        return (
            <div className="NewsCardSlider">
                <div className="NewsCardSlider__inner">
                    <Slick
                        {...SLICK_OPTIONS}
                    >
                        {this.renderSlickSlides()}
                    </Slick>
                </div>
            </div>
        )
    }
}

NewsCardSlider.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
        image: PropTypes.string,
        title: PropTypes.string,
        category: PropTypes.string,
    })),
};

const nodes = document.querySelectorAll('[data-component=NewsCardSlider]');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const props = JSON.parse(node.getAttribute('data-props'));
    render(<NewsCardSlider {...props}/>, node);
}
