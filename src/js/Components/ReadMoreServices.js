import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";

class ReadMoreServices extends Component {
    constructor(props) {
        super(props);

        this.state = {
            backgroundPositionYPercentage: .5,
        };

        // bind function
        this.updateImagePosition = this.updateImagePosition.bind(this);

    }

    componentDidMount() {
        this.getScrollParent().addEventListener('scroll', this.updateImagePosition);
    }

    componentWillUnmount() {
        this.getScrollParent().removeEventListener('scroll', this.updateImagePosition);
    }

    getScrollParent() {
        return document.querySelector(this.props.scrollParent);
    }

    updateImagePosition() {
        const scrollParent = this.getScrollParent();
        const {root} = this.refs;
        const {height: rootHeight, top: rootTop} = root.getBoundingClientRect();
        const {height: scrollParentHeight} = scrollParent.getBoundingClientRect();

        // part of the calculations
        const calc1 = scrollParentHeight + rootHeight;
        const calc2 = rootTop + rootHeight;

        this.setState({ backgroundPositionYPercentage: 1 - calc2 / calc1 });

    }

    render() {
        const {backgroundImage, title, content, linkText, linkUrl} = this.props;
        const {backgroundPositionYPercentage} = this.state;
        return (
            <div
                className="ReadMoreServices"
                ref="root"
            >
                <div className="ReadMoreServices__inner"
                     style={{
                         backgroundImage: `url(${backgroundImage})`,
                         backgroundPositionY: `${backgroundPositionYPercentage * 100}%`,
                     }}
                >
                    <div className="ReadMoreServices__overlay">
                    <div className="ReadMoreServices__shapes">
                        <div className="ReadMoreServices__shapes-item ReadMoreServices__shapes-item--1" />
                        <div className="ReadMoreServices__shapes-item ReadMoreServices__shapes-item--2" />
                        <div className="ReadMoreServices__shapes-item ReadMoreServices__shapes-item--3" />
                    </div>
                    <div className="content full-height">
                        <div className="container-fluid">
                            <div className="row full-height">
                                <div className="col-md-12">
                                    <br />
                                    <h3 className="section__title">
                                        {title}
                                    </h3>
                                </div>
                                <div className="col-md-5">
                                    <div className="visible-md visible-lg">
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                    </div>
                                    <div className="ReadMoreServices__text">
                                        <div className="ReadMoreServices__text-inner">
                                            <p>{content}</p>
                                            <div className="ReadMoreServices__read-more">
                                                <a href={linkUrl} className="LinkButton LinkButton--white LinkButton--long">{linkText}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}

ReadMoreServices.propTypes = {
    scrollParent: PropTypes.string,
    backgroundImage: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    linkText: PropTypes.string.isRequired,
    linkUrl: PropTypes.string.isRequired,
};

ReadMoreServices.defaultProps = {
    scrollParent: '#container',
};

const nodes = document.querySelectorAll('[data-component="ReadMoreServices"]');

for(let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const props = JSON.parse(node.getAttribute('data-props'));
    render(<ReadMoreServices {...props} />, node);
}
