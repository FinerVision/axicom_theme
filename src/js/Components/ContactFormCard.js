import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";

import CornerCutImage from "./CornerCutImage";
import LinkedinButton from "./LinkedinButton";

class ContactFormCard extends Component {
    render() {
        let {image, contact_title_secondary, name, email, contact_title, linkedin} = this.props;

        if (contact_title && contact_title !== '') {
            contact_title_secondary = '';
        }

        return (
            <div className="ContactFormCard">
                <div className="ContactFormCard__table">
                    <div className="ContactFormCard__cell ContactFormCard__cell--100">
                        <CornerCutImage src={image}/>
                    </div>
                    <div className="ContactFormCard__cell">
                        <div className="ContactFormCard__text">
                            {
                                [contact_title_secondary, name, contact_title].map((item, index) => {

                                    if(index === 0){
                                        return (
                                            <div key={index} className="ContactFormCard__text-item"><b>{item}</b></div>
                                        )
                                    }else{
                                        return (
                                            <div key={index} className="ContactFormCard__text-item">{item}</div>
                                        )
                                    }

                                })
                            }
                            <a className="ContactFormCard__text-item" href={`mailto:${email}`}>{email}</a>
                            <LinkedinButton style={{marginTop:5}} href={linkedin}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ContactFormCard.propTypes = {
    image: PropTypes.object.isRequired,
    name: PropTypes.string.isRequired,
    email: PropTypes.string,
    contact_title: PropTypes.string,
    contact_title_secondary: PropTypes.string,
    linkedin: PropTypes.string
};

ContactFormCard.defaultProps = {
    contact_title: '',
};

export default ContactFormCard;

const nodes = document.querySelectorAll('[data-component=ContactFormCard]');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const params = JSON.parse(node.getAttribute('data-params'));
    render(<ContactFormCard {...params} />, node);
}
