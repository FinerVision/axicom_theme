import $ from "jquery";

$(document).ready(function () {
    $('.CaseStudyTargets__menu .CaseStudyTargets__link').click(function(){
        let tab_id = $(this).attr('data-tab');
        let title_id = $(this).attr('data-title');
        $('.CaseStudyTargets__menu .CaseStudyTargets__link').removeClass('CaseStudyTargets__link--current');
        $('.CaseStudyTargets__content').removeClass('CaseStudyTargets--show');
        $('.CaseStudyTargets__title-content').removeClass('CaseStudyTargets__title-content--current');

        $(this).addClass('CaseStudyTargets__link--current');
        $("#"+tab_id).addClass('CaseStudyTargets--show');
        $("#"+title_id).addClass('CaseStudyTargets__title-content--current');
    })
});
