import React, {Component} from "react";
import {render} from "react-dom";
import Slick from "react-slick";
import PropTypes from "prop-types";
import $ from "jquery";
import classNames from "classnames";

import breakpoints from "../Utils/breakpoints";
import breakpointImage from "../Utils/breakpointImage";

class CaseStudyCarousel extends Component {
    constructor(props) {
        super(props);

        this.lastCheckActiveSlideHeight = 0;
        this.isAnimating = false;

        this.state = {
            activeIndex: 0,
            expendingIndex: null,
            rootHeight: 0,
        };

        this.updateActiveSlideInner = this.updateActiveSlideInner.bind(this);

    }

    shouldComponentUpdate() {
        // do re-rendering during the slick is animating will cause issues
        return !this.isAnimating;
    }

    componentDidMount() {
        // to make sure the title bar is exist by referencing the height of active slides
        // @see getActiveSlideHeight()
        window.setTimeout(this.forceUpdate.bind(this), 1000);
    }

    updateActiveSlideInner() {

        window.requestAnimationFrame(this.updateActiveSlideInner);

        const activeSlideInner = this.refs[`slideInner_${activeIndex}`];

        if (activeSlideInner && this.lastCheckActiveSlideHeight !== $(activeSlideInner).height() && !this.isAnimating) {
            this.lastCheckActiveSlideHeight = $(activeSlideInner).height;
            this.forceUpdate();
        }

    }

    getActiveSlideHeight() {

        if (window.innerWidth <= breakpoints.md - 1) {
            return 'auto';
        }

        const {activeIndex} = this.state;
        const activeSlideInner = this.refs[`slideInner_${activeIndex}`];
        if (!activeSlideInner) { return 0; }
        return $(activeSlideInner).height();
    }

    getSlideContent(item, index) {

        const {expendingIndex} = this.state;
        const length = this.props.items.length;

        // check if this is the active slide and it's active
        const isExpending = index === expendingIndex;
        const hasAnySlideExpending = expendingIndex !== null;
        const isExpendPrevSlide = hasAnySlideExpending && index === (expendingIndex - 1 + length) % length;
        const isExpendNextSlide = hasAnySlideExpending && index === (expendingIndex + 1) % length;

        return (
            <div className={classNames(
                'CaseStudyCarousel__slide-content',
                {
                    'CaseStudyCarousel__slide-content--expending': isExpending,
                    'CaseStudyCarousel__slide-content--expend-prev-slide': isExpendPrevSlide,
                    'CaseStudyCarousel__slide-content--expend-next-slide': isExpendNextSlide,
                }
            )}>
                <div className="CaseStudyCarousel__slide-background" style={{backgroundImage: `url(${breakpointImage(item.image)})`}} />
                <div className="CaseStudyCarousel__slide-overlay" style={{backgroundImage: `url(${breakpointImage(item.overlay)})`}} />
                <div className="CaseStudyCarousel__slide-content-inner">

                    <h3 className="CaseStudyCarousel__slide-title">{item.title}</h3>
                    {
                        isExpending && <div
                            className="CaseStudyCarousel__slide-text"
                            dangerouslySetInnerHTML={{__html: item.content}}
                        />
                    }
                    {
                        isExpending && <a
                            className="CaseStudyCarousel__slide-remark"
                            href={item.link}
                        >{item.remarkText}</a>
                    }
                    {
                        item.linkText && !isExpending && <a
                            className="CaseStudyCarousel__slide-remark"
                            href={item.link}
                        >{item.linkText}</a>
                    }
                    {
                        this.props.readMoreText && !isExpending && <a
                            className="CaseStudyCarousel__slide-readmore"
                            href={item.link}
                        >{this.props.readMoreText}</a>
                    }
                </div>
            </div>
        );

    }

    getSlides() {
        const {activeIndex, expendingIndex} = this.state;
        const length = this.props.items.length;

        const style = (() => {
            if (window.innerWidth < breakpoints.md - 1) {
                return {
                    width: window.innerWidth,
                };
            }
            // if (window.innerWidth < breakpoints.lg - 1) {
            //     return {
            //         width: window.innerWidth / 2,
            //     };
            // }
            return {
                width: window.innerWidth / 2.8,
            };

        })();

        return this.props.items.map((item, index) => (
            <div
                key={index}
                style={style}
            >
                <div className="CaseStudyCarousel__slide">
                    <div
                        className="CaseStudyCarousel__slide-inner"
                        ref={`slideInner_${index}`}
                        onClick={() => {
                            if (!this.isAnimating) {
                                this.setState({expendingIndex: expendingIndex !== index ? index : null}, () => {
                                    // should do the sliding after expending
                                    if (index === (activeIndex - 1 + length) % length) {
                                        this.refs.slick.slickPrev();
                                    } else if (index === (activeIndex + 1) % length) {
                                        this.refs.slick.slickNext();
                                    }
                                });

                            }
                        }}
                    >
                        {this.getSlideContent(item, index)}
                    </div>
                </div>
            </div>
        ));
    }

    render() {
        return (
            <div
                ref="root"
                className={classNames(
                    'CaseStudyCarousel',
                    {
                        'CaseStudyCarousel--expend': this.state.expendingIndex !== null,
                    })}
            >
                <div className={classNames(
                    'CaseStudyCarousel__title',
                    {
                        'CaseStudyCarousel__title--hide': this.state.expendingIndex !== null,
                    }
                )}>
                    <div className="content">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-2 col-md-offset-10">
                                    <div
                                        className="CaseStudyCarousel__title-bar"
                                        style={{
                                            height: this.getActiveSlideHeight(),
                                        }}
                                    >
                                        <h3 className="section__title CaseStudyCarousel__title-text">
                                            {this.props.title}
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="CaseStudyCarousel__inner">
                    <Slick
                        ref="slick"
                        {...CaseStudyCarousel.SLICK_OPTIONS}
                        beforeChange={() => {
                            this.isAnimating = true;
                        }}
                        afterChange={index => {
                            this.isAnimating = false;
                            this.setState({activeIndex: index});
                        }}
                    >
                        {this.getSlides()}
                    </Slick>
                </div>
            </div>
        );
    }
}

CaseStudyCarousel.SLICK_OPTIONS = {
    variableWidth: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    speed: 1000,
    responsive: [
        {
            breakpoint: breakpoints.md - 1,
            settings: {
                variableWidth: false,
            },
        },
    ],
};

CaseStudyCarousel.propTypes = {
    title: PropTypes.string,
    readMoreText: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.shape({
        image: PropTypes.oneOfType([PropTypes.object, PropTypes.string]),
        overlay: PropTypes.string,
        title: PropTypes.string,
        content: PropTypes.string,
        subtitle: PropTypes.string,
        bannerHeaderTitle: PropTypes.string,
        link: PropTypes.string,
        linkText: PropTypes.string,
        remarkText: PropTypes.string,
    })),
};

export default CaseStudyCarousel;

$('[data-component=CaseStudyCarousel]').each(function() {
    const props = $(this).data('props');
    render(<CaseStudyCarousel {...props} />, this);
});
