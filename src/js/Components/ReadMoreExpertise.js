import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";

class ReadMoreExpertise extends Component {
    constructor(props) {
        super(props);

        this.state = {
            backgroundPositionYPercentage: .5,
        };

        // bind function
        this.updateImagePosition = this.updateImagePosition.bind(this);

    }

    componentDidMount() {
        this.getScrollParent().addEventListener('scroll', this.updateImagePosition);
    }

    componentWillUnmount() {
        this.getScrollParent().removeEventListener('scroll', this.updateImagePosition);
    }

    getScrollParent() {
        return document.querySelector(this.props.scrollParent);
    }

    updateImagePosition() {
        const scrollParent = this.getScrollParent();
        const {root} = this.refs;
        const {height: rootHeight, top: rootTop} = root.getBoundingClientRect();
        const {height: scrollParentHeight} = scrollParent.getBoundingClientRect();

        // part of the calculations
        const calc1 = scrollParentHeight + rootHeight;
        const calc2 = rootTop + rootHeight;

        this.setState({ backgroundPositionYPercentage: 1 - calc2 / calc1 });

    }

    render() {
        const {backgroundImage, title, content, linkText, linkUrl} = this.props;
        const {backgroundPositionYPercentage} = this.state;
        return (
            <div
                className="ReadMoreExpertise"
                ref="root"
            >
                <div className="ReadMoreExpertise__inner"
                     style={{
                         backgroundImage: `url(${backgroundImage})`,
                         backgroundPositionY: `${backgroundPositionYPercentage * 100}%`,
                     }}
                >
                    <div className="ReadMoreExpertise__overlay">
                        <div className="ReadMoreExpertise__shapes">
                            <div className="ReadMoreExpertise__shapes-item ReadMoreExpertise__shapes-item--1" />
                            <div className="ReadMoreExpertise__shapes-item ReadMoreExpertise__shapes-item--2" />
                            <div className="ReadMoreExpertise__shapes-item ReadMoreExpertise__shapes-item--3" />
                        </div>
                        <div className="content full-height">
                            <div className="container-fluid">
                                <div className="row full-height">
                                    <div className="col-md-2 col-md-offset-10">
                                        <br />
                                        <h3 className="section__title">
                                            {title}
                                        </h3>
                                    </div>
                                    <div className="col-md-5 col-md-offset-7">
                                        <div className="visible-md visible-lg">
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                        </div>
                                        <div className="ReadMoreExpertise__text">
                                            <div className="ReadMoreExpertise__text-inner">
                                                <p>{content}</p>
                                                <div className="ReadMoreExpertise__read-more">
                                                    <a href={linkUrl} className="LinkButton LinkButton--white LinkButton--long">{linkText}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ReadMoreExpertise.propTypes = {
    scrollParent: PropTypes.string,
    backgroundImage: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    linkText: PropTypes.string.isRequired,
    linkUrl: PropTypes.string.isRequired,
};

ReadMoreExpertise.defaultProps = {
    scrollParent: '#container',
};

const nodes = document.querySelectorAll('[data-component="ReadMoreExpertise"]');

for(let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const props = JSON.parse(node.getAttribute('data-props'));
    render(<ReadMoreExpertise {...props} />, node);
}
