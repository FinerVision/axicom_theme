import getScrollbarWidth from "scrollbar-width";
import $ from "jquery";
$(document).ready(function () {
    setTimeout(function () {
        $('.Nav__links-link').css({"transition":"all .3s ease","-webkit-transition":"all 0.3s ease"});
    },300);
});
(() => {
    const container = document.querySelector('#container');
    const nav = document.querySelector('.Nav');

    if (!nav) {
        return;
    }

    const mobileToggle = document.querySelector('.Nav__mobile-toggle');
    const navLogo = document.querySelector('.Nav__logo');

    const navLanguage = document.querySelector('.Nav__language');
    const navGlobe = document.querySelector('.Nav__language__globe');

    const state = {
        mobileMenuOpen: false
    };

    const handleScroll = () => {
        const scrolled = container.scrollTop > 0;

        if (scrolled) {
            nav.classList.add('Nav--scrolled');
        } else {
            nav.classList.remove('Nav--scrolled');
        }

        // update the pages' home button logo when the user is scrolling
        const navLogoScroll = navLogo.querySelector('.Nav__logo-scroll');
        const axicomCharCount = 6; // A X I C O M
        const {scrollTop, scrollHeight} = container;
        const scrollableHeight = scrollHeight - container.getBoundingClientRect().height;
        const perSection = scrollableHeight / axicomCharCount;

        for(let i = axicomCharCount - 1; i >= 0; i--) {
            if (scrollTop > i * perSection) {
                navLogoScroll.style.top = `${-100 * i}%`;
                break;
            }
        }

    };

    const toggleMobileMenu = () => {
        state.mobileMenuOpen = !state.mobileMenuOpen;

        if (state.mobileMenuOpen) {
            nav.classList.add('Nav--mobile-open');
            mobileToggle.classList.add('Nav__mobile-toggle--open');
        } else {
            mobileToggle.classList.remove('Nav__mobile-toggle--open');
            nav.classList.remove('Nav--mobile-open');
        }
    };

// easing function swing
// TODO : refactor this to helper function
    const swing = p => 0.5 - Math.cos( p * Math.PI ) / 2;

    const onLogoClick = () => {

        // back to the page's top
        const SCROLLING_DURATION = 1000;
        const SCROLLING_UPDATING_INTERVAL = 10;
        const SCROLL_PER_INTERVAL = SCROLLING_UPDATING_INTERVAL / SCROLLING_DURATION;
        const TOTAL_SCROLL_TURN = 1 / SCROLL_PER_INTERVAL;

        const scrollable = container;
        const easingFunction = swing;
        const originTop = scrollable.scrollTop;
        const destinationTop = 0;

        let currentTurn = 1;

        const scrollInterval = window.setInterval(() => {
            scrollable.scrollTop = originTop - Math.abs((originTop - destinationTop) * easingFunction(currentTurn * SCROLL_PER_INTERVAL));
            currentTurn++;

            if (currentTurn > TOTAL_SCROLL_TURN) {
                window.clearInterval(scrollInterval);
            }
        }, SCROLLING_UPDATING_INTERVAL);

    };

    const onGlobeClick = ()=> {
        navLanguage.classList.toggle('open');
    };

    container.addEventListener('scroll', handleScroll);
    mobileToggle && mobileToggle.addEventListener('click', toggleMobileMenu);
    navLogo && navLogo.addEventListener('click', onLogoClick);
    if (navGlobe) {
        navGlobe.addEventListener('click', onGlobeClick);
    }

    let lastUpdatedNavWidth = 0;
    const trackUpdateNavWidth = () => {
        window.requestAnimationFrame(trackUpdateNavWidth);
        if (lastUpdatedNavWidth !== window.innerWidth) {
            lastUpdatedNavWidth = window.innerWidth;
            const scrollbarWidth = getScrollbarWidth();

            $(nav).css({width: `calc(100% - ${scrollbarWidth}px)`});
            const navContent = $('.Nav__content');
            navContent.css({"opacity":"1"});
            navContent.css({"transition":"opacity .2s ease-in-out"});
        }
    };
    window.setTimeout(trackUpdateNavWidth, 1000);

})();
