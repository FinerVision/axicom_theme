import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import classNames from "classnames";

class JobList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            filter: {
                category: '',
                location: ''
            },
            active: []
        };

        this.filterCategory = this.filterCategory.bind(this);
        this.filterLocation = this.filterLocation.bind(this);
    }

    componentDidMount() {
        window.addEventListener('jobs-filter-category', this.filterCategory);
        window.addEventListener('jobs-filter-location', this.filterLocation);
    }

    componentWillUnmount() {
        window.removeEventListener('jobs-filter-category', this.filterCategory);
        window.removeEventListener('jobs-filter-location', this.filterLocation);
    }

    filterCategory(event) {
        let {filter} = this.state;
        filter.category = event.value;
        this.setState({filter});
    }

    filterLocation(event) {
        let {filter} = this.state;
        filter.location = event.value;
        this.setState({filter});
    }

    toggleItem(index) {

        let active = this.state.active;
        let p = active.indexOf(index);

        if (p === -1) {
            active.push(index);
        } else {
            active.splice(p, 1);
        }

        this.setState({active});
    }

    getJobList() {
        const {items} = this.props;

        return items.map((job, index) => {

            if (this.state.filter.category !== '' && this.state.filter.category !== job.category_slug) {
                return null
            }

            if (this.state.filter.location !== '' && this.state.filter.location !== job.location_slug) {
                return null
            }

            const collapsibleInner = this.refs[`collapsibleInner_${index}`];
            const collapsibleInnerHeight = collapsibleInner ? collapsibleInner.getBoundingClientRect().height || 0 : 0;
            const isActive = this.state.active.indexOf(index) !== -1;

            return (
                <div key={index} className="JobList__item">
                    <div className="row no-gutter">
                        <div className="col-md-7">
                            <div className="JobList__item-collapsible-header"
                                 onClick={() => {
                                     this.toggleItem(index)
                                 }}
                            >
                                <div className="JobList__item-title" dangerouslySetInnerHTML={{__html: job.title}}/>
                                <div className="JobList__item-subtitle">{job.subtitle}</div>
                                <div
                                    className={classNames(
                                        'icon',
                                        {
                                            close: !isActive,
                                            open: isActive,
                                        }
                                    )}>
                                    >
                                </div>
                            </div>
                            <div
                                className="JobList__item-collapsible"
                                style={{
                                    height: isActive ? collapsibleInnerHeight : 0,
                                }}
                            >
                                <div
                                    className="JobList__item-collapsible-inner"
                                    ref={`collapsibleInner_${index}`}
                                >
                                    <br/>
                                    <div className="JobList__item-brief"
                                         dangerouslySetInnerHTML={{__html: job.description}}/>
                                    <br/>
                                    <a href={job.short_link} target="_blank"
                                       className="LinkButton LinkButton--center LinkButton--no-underline LinkButton--border LinkButton--border-radius LinkButton--olive">{this.props.applyText}</a>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12">
                            <br/>
                            <div className="JobList__item-bottom-line"/>
                        </div>
                    </div>
                </div>
            );
        });
    }

    render() {
        return (
            <div className="JobList">
                <div className="JobList__inner">
                    {this.getJobList()}
                </div>
            </div>
        );
    }
}

JobList.propTypes = {
    applyText: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string.isRequired,
        subtitle: PropTypes.string.isRequired,
        brief: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
    })).isRequired,
};

export default JobList;

const nodes = document.querySelectorAll('[data-component=JobList]');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const params = JSON.parse(node.getAttribute('data-params'));
    render(<JobList {...params} />, node);
}
