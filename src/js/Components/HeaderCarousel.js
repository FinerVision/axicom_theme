import SwiperCore, {Navigation} from 'swiper';
import $ from 'jquery';

import 'core-js/es6/number';

SwiperCore.use([Navigation]);

window.addEventListener('load', () => {
    let swiper = new SwiperCore('.HeaderCarousel__wrapper', {
        loop: true,
        direction: "horizontal",
        loopFillGroupWithBlank: true,
        navigation: {
            nextEl: '.HeaderCarousel__button-next',
            prevEl: '.HeaderCarousel__button-prev',
        },
        draggable: true,
        autoplay: {
            delay: 3000,
        },
        simulateTouch:false,
        spaceBetween: 0,
        updateOnWindowResize: true,
        // setWrapperSize: '100%',
        slideToClickedSlide: false,
        preventClicksPropagation: true,
        preventClicks: true,
        on: {
            transitionEnd: function (swiper) {
                /* do something */

                this.$el.find('HeaderCarousel__content');
                let content = $(`.swiper-slide-active`)
                    .find('.HeaderCarousel__content').clone();

                content.prependTo('.HeaderCarousel__wrapper').fadeIn('slow',function () {
                    $(this).animate({opacity:1});
                });

            },
            transitionStart: function (swiper) {
                /* do something */

                this.$el.find('HeaderCarousel__content');
                $(`.HeaderCarousel__wrapper > .HeaderCarousel__content`)
                    .fadeOut(400, function() { $(this).remove(); })
            },
            init: function () {
                $(window).trigger('resize');
            },
        },
        slidesPerView: 1,
        centeredSlides: true,
        speed: 1000,
        preventInteractionOnTransition: true,
        touchEventsTarget: false,
        allowTouchMove: true,
        breakpoints: {
            // when window width is >= 320px
            320: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            // when window width is >= 480px
            576: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            // when window width is >= 640px
            992: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            // when window width is >= 1200px
            1200: {
                slidesPerView: 1,
                spaceBetween: 0
            },

        }
    });
    let autoPlay;
    autoPlayStart();
    $('.HeaderCarousel__button').on('click',()=>{
        clearInterval(autoPlay);
        autoPlayStart();
    });
    function autoPlayStart() {
        autoPlay = setInterval(function(){
            swiper.slideNext();
        }, 6000);
    }

    let yourIntervalId = setInterval(function(){
        $(window).trigger('resize');
    }, 500);

    setTimeout(function(){
        clearInterval(yourIntervalId);
    }, 4000);
}, false);
let width;
if (window.matchMedia("(min-width: 1129px)").matches) {
    width = 1000;
}else{
    width = screen.width / 100 * 80;
}
$('.HeaderCarousel__button-prev').css('transform',`translateX(-${width/2 + 50}px) rotate(-45deg)`);
$('.HeaderCarousel__button-next').css('transform',`translateX(${width/2 + 50}px) rotate(135deg)`);
$(document).ready(function(){
    $('.HeaderCarousel__button').css('z-index','99999','important')
    $('.HeaderCarousel__button-prev').css('transform',`translateX(-${width/2 + 50}px) rotate(-45deg)`);
    $('.HeaderCarousel__button-next').css('transform',`translateX(${width/2 + 50}px) rotate(135deg)`);
});
$(window).on('resize', function(){
    $('.HeaderCarousel__button-prev').css('transform',`translateX(-${width/2 + 50}px) rotate(-45deg)`);
    $('.HeaderCarousel__button-next').css('transform',`translateX(${width/2 + 50}px) rotate(135deg)`);
});