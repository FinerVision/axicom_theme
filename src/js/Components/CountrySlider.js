import React, {PureComponent} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import Slick from "react-slick";
import classNames from "classnames";

const SLICK_OPTIONS = {
    arrows: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: 0,
    infinite: true,
};

class CountrySlider extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            activeCountryName: null, // the index which clicked once
        };

    }

    renderSlickItems() {
        const {items} = this.props;
        const countryNames = Object.keys(items);

        return countryNames.map((countryName, index) => {
            const cityList = items[countryName];

            return (
                <div key={index}>
                    <div
                         className={classNames('CountrySlider__slide', {
                             'CountrySlider__slide--multiple-offices': cityList.length > 1,
                         })}
                    >
                        <a className="CountrySlider__url">
                            <div
                                onClick={() => {
                                    this.setState({activeCountryName: countryName}, () => {
                                        if (countryNames.length > 1) {
                                            this.props.onClick(countryName);
                                        } else {
                                            this.props.onClick(countryName, 0);
                                        }
                                    });

                                }}
                            >{countryName}</div>

                        </a>
                        {this.renderCityList(cityList, countryName)}
                    </div>
                </div>
            );
        });
    }

    renderCityList(cityList, countryName) {
        if (cityList.length === 1) {
            return null;
        }

        return (
            <div
                className={classNames('CountrySlider__city-list', {
                    'CountrySlider__city-list--active': this.state.activeCountryName === countryName,
                })}
            >
                <ul>
                    {cityList.map((city, index) => (
                        <li
                            key={index}
                            onClick={() => {
                                this.setState({activeCountryName: null}, () => {
                                    this.props.onClick(countryName, index);
                                });
                            }}
                            style={{cursor:'pointer'}}
                        >{city.city}</li>
                    ))}
                </ul>
            </div>
        );
    }

    render() {

        // clone
        const slickSettings = JSON.parse(JSON.stringify(SLICK_OPTIONS));
        slickSettings.afterChange = (...props) => {
            if (typeof this.props.onChange === 'function') {
                this.props.onChange(...props);
            }
        };

        return (
            <div className="CountrySlider">
                <Slick {...slickSettings} ref="slick">
                    {this.renderSlickItems()}
                </Slick>
            </div>
        );
    }
}


CountrySlider.propTypes = {
    items: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.object)).isRequired,
    clickToSwitchPage: PropTypes.bool,
    onChange: PropTypes.func,
    onClick: PropTypes.func,
};

CountrySlider.defaultProps = {
    clickToSwitchPage: true,
    onChange: null,
    onClick: () => {},
};

export default CountrySlider;

const nodes = document.querySelectorAll('[data-component=CountrySlider]');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const params = JSON.parse(node.getAttribute('data-params'));
    render(<CountrySlider {...params} />, node);
}
