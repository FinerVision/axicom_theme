import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import Slick from "react-slick";

import breakpointImage from "../Utils/breakpointImage"

class ImageSlider extends Component {
    constructor(props) {
        super(props);

        this.checkSlidesWidth = this.checkSlidesWidth.bind(this);
    }

    // fix an issue which the calculation is wrong when initializing the slick
    // ref: https://github.com/akiran/react-slick/issues/809#issuecomment-317277508
    componentWillReceiveProps(){
        this.refs.slick.innerSlider.onWindowResized()
    }

    componentDidMount() {
        this.checkSlidesWidth();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.imageIndex !== this.props.imageIndex) {
            this.refs.slick.slickGoTo(this.props.imageIndex);
        }
    }

    checkSlidesWidth() {

        window.requestAnimationFrame(this.checkSlidesWidth);
        const rootWidth = this.refs.root.getBoundingClientRect().width;
        const slideWidthMax = (() => {
            const length = this.props.items.length;
            const widths = [];
            for(let i = 0; i < length; i++) {
                const element = this.refs[`slickSlide_${i}`];
                widths.push(element.getBoundingClientRect().width);
            }
            return Math.max(...widths);
        })();
        if (rootWidth !== slideWidthMax) {
            this.forceUpdate();
        }
    }

    slickOptions() {
        return {
            infinite: true,
            touchMove: false,
            arrows: false,
            swipe: false,
        };
    }

    getActiveItem() {
        const {imageIndex} = this.props;
        return this.props.items[imageIndex];
    }

    renderSlickSlides() {
        return this.props.items.map(({image, image_title, url}, index) => (
            <div key={index}>
                <a href={url}>
                    <div className="ImageSlider__slide" style={{backgroundImage: `url(${breakpointImage(image)})`}} ref={`slickSlide_${index}`}>
                        <div className="ImageSlider__description">
                            {image_title}
                        </div>
                    </div>
                </a>
            </div>
        ));
    }

    updateActiveIndex(index) {
        this.refs.slick.slickGoTo(index);
    }

    render() {
        const {person} = this.getActiveItem();
        return (
            <div className="ImageSlider" ref="root">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-5 col-md-offset-1">
                            <div className="ImageSlider__slider">
                                <div className="ImageSlider__slider-inner">
                                    <Slick
                                        ref="slick"
                                        {...this.slickOptions()}
                                    >
                                        {this.renderSlickSlides()}
                                    </Slick>
                                </div>
                            </div>
                        </div>
                        {person && (
                            <div className="col-md-5">
                                <div className="Services__card">
                                    <div
                                        className="Services__card-image"
                                        style={{backgroundImage: `url(${breakpointImage(person.image)})`}}
                                    />
                                    <div className="Services__card-text">
                                        {person.name}<br/>
                                        {person.job}
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

ImageSlider.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
        image: PropTypes.object,
        image_title: PropTypes.string,
        person: PropTypes.oneOfType([
            PropTypes.shape({
                image: PropTypes.object,
                name: PropTypes.string,
                job: PropTypes.string,
                url: PropTypes.string,
            }),
            PropTypes.any
        ]),
        url: PropTypes.string,
    })).isRequired,
    imageIndex: PropTypes.number,
};

ImageSlider.defaultProps = {
    imageIndex: 0,
};

export default ImageSlider;

const nodes = document.querySelectorAll('[data-component=ImageSlider]');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const props = JSON.parse(node.getAttribute('data-props'));
    render(<ImageSlider {...props}/>, node);
}
