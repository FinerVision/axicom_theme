import $ from "jquery";

$(function () {
    let appendthis = ("<div class='modal-overlay js-modal-close'></div>");
    let url = window.location.pathname;

    if(window.location.hash) {


        console.log('window.location.hash.substr(1)',window.location.hash.substr(1));
        modalsPosition();
        $(".Services__elements").append(appendthis);
        $(".modal-overlay").fadeTo(500, 0.7,function(){
            $("[data-pop-up="+window.location.hash.substr(1)+"]").show()
        });

    }

    $('[data-modal-id]').click(function (e) {
        e.preventDefault();
        modalsPosition();
        $(".Services__elements").append(appendthis);
        $(".modal-overlay").fadeTo(500, 0.7);
        let modalBox = $(this).attr('data-modal-id');
        $('#' + modalBox).fadeIn($(this).data());

        history.pushState({}, null, "#" + $(this).data('slug'));
    });

    $(".Services__elements").on('click', '.modal-overlay', function () {
        closeModal()
    });
    $(".Modal__close").click(function () {
        closeModal()
    });

    $(window).resize(function () {
        modalsPosition();
    });

    function closeModal() {
        $(".Modal__box, .modal-overlay").fadeOut(500, function () {
            $(".modal-overlay").remove();
            window.history.replaceState("", "Page Title 2", url);
        });
    }

//setup modals position based on scroll
    function modalsPosition() {
        let scrollTop = $('#container').scrollTop();
        $(".Modal__box").css({
            top: (scrollTop - $('.HeroParallax').height() + $('Nav.Nav--sticky.Nav--scrolled').height() + window.screen.height / 100 * 10)-80,
        });
    }
});