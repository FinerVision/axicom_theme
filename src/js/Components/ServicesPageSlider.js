import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import $ from "jquery";

import ExpertiseRotator from "./ExpertiseRotator";
import ImageSlider from "./ImageSlider";

class ServicesPageSlider extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeIndex: 0,
        };
    }

    componentDidMount() {

        const hashId = location.hash;

        if (hashId) {
            // scroll the page to the such element
            // TODO : make cleaner code and move the hardcoded selector to ba props
            const $container = $('#container');
            const $nav = $('.Nav');
            const $root = $(this.refs.root);

            $container.scrollTop($container.scrollTop() - $nav.height() + $root.offset().top);
        }
    }

    getImageSliderProps() {

        const {serviceItems} = this.props;

        return {
            items: serviceItems.map(({imageSlider}) => imageSlider),
        }
    }

    render() {
        return (
            <div className="ServicesPageSlider" ref="root">
                <ExpertiseRotator
                    {...this.props}
                    onChange={activeIndex => this.setState({activeIndex})}
                />

                <div className="section section--no-top-padding Services__image-slider">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-xs-10 col-sm-offset-0 col-xs-offset-1 col-sm-offset-1 col-md-offset-0">
                                <ImageSlider
                                    {...this.getImageSliderProps()}
                                    imageIndex={this.state.activeIndex}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="Gap visible-md visible-lg" />
            </div>
        );
    }
}

ServicesPageSlider.propTypes = {
    title: PropTypes.string.isRequired,
    showSwitch: PropTypes.bool.isRequired,
    defaultSection: PropTypes.oneOf([
        ExpertiseRotator.sector,
        ExpertiseRotator.service,
    ]),
    subtitles: PropTypes.shape({
        serviceTitle: PropTypes.string,
        sectorTitle: PropTypes.string,
    }),
    serviceItems: PropTypes.arrayOf(PropTypes.shape({
        icon: PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
        person: PropTypes.shape({
            image: PropTypes.object,
            name: PropTypes.string,
            job: PropTypes.string,
        }),
        imageSlider: PropTypes.shape({
            image: PropTypes.object,
            image_title: PropTypes.string,
            url: PropTypes.string,
        }),
        slug: PropTypes.string,
    })),
    slideArrows: PropTypes.shape({
        prev: PropTypes.string.isRequired,
        next: PropTypes.string.isRequired,
    }),
};

export default ServicesPageSlider;

const nodes = document.querySelectorAll('[data-component=ServicesPageSlider]');

for(let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const params = JSON.parse(node.getAttribute('data-params'));
    render(<ServicesPageSlider {...params} />, node);
}
