import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";

class MobileServices extends Component {
    getItems() {
        const {services} = this.props;

        return services.map(({title, description}, index) => (
            <div className="MobileServices__item" key={index}>
                <div className="MobileServices__title">
                    {title}
                    <div className="ThickLine ThickLine--stick ThickLine--thin"/>
                </div>
                <div className="MobileServices__description">
                    {description}
                </div>
            </div>
        ));
    }

    render() {
        return (
            <div className="section MobileServices">
                <div className="section__title">
                    {this.props.title}
                </div>
                <br/>
                <div className="MobileServices__inner">
                    {this.getItems()}
                </div>
            </div>
        );
    }
}

MobileServices.propTypes = {
    title: PropTypes.string.isRequired,
    services: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        person: PropTypes.shape({
            image: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            job: PropTypes.string.isRequired,
        }),
        order: PropTypes.number.isRequired,
    })).isRequired,
};

const nodes = document.querySelectorAll('[data-component=MobileServices]');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const params = JSON.parse(node.getAttribute('data-params'));
    render(<MobileServices {...params} />, node);
}
