import $ from "jquery";
import {detect} from "detect-browser";

window.addEventListener('load', () => {

    const browser = detect();

    if (browser.name === 'safari' || browser.name === 'chrome') {
        // laggy issue in safari, use perspective and 3d transform parallax to build the parallax feature,
        // this parallax effect is good but will not be working in older browsers especially IE
        // also issue in chrome
        return;
    }

    const $heroParallax = $('.HeroParallax');
    const $scrollable = $('#container');

    $scrollable.on('scroll', function() {
        const $backgroundImages = $heroParallax.find('.HeroParallax__background-image');

        if (!$backgroundImages[0] || !$scrollable[0]) {
            return;
        }

        const {top: heroParallaxTop} = $backgroundImages[0].getBoundingClientRect();
        const {top: scrollableTop} = $scrollable[0].getBoundingClientRect();
        const top = heroParallaxTop - scrollableTop;

        // !!! so what will happens is whenever the user scroll, the background will scroll half of the other parts do
        const topOverride = Math.abs(top) >> 1;
        $backgroundImages.css({top: topOverride});
    });

});
