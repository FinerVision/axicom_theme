window.$ = require("jquery");
require("jquery-match-height");
$(function() {
    $('.same-height-columns').each(function () {

        $(this).find('>*').matchHeight();
    });
});