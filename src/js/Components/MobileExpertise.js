import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import Slick from "react-slick";
import classNames from "classnames";

import breakpoints from "../Utils/breakpoints";
import breakpointImage from "../Utils/breakpointImage";

class MobileExpertise extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeIndex: 0,
        };
    }

    getSlickSettings() {
        const {onChange} = this.props;
        const slickSettings = JSON.parse(JSON.stringify(MobileExpertise.SLICK_SETTINGS));
        slickSettings.beforeChange = (oldIndex, newIndex) => {
            this.setState({activeIndex: newIndex});
            onChange(newIndex);
        };
        return slickSettings;
    }

    getActiveExpertise() {
        const {activeIndex} = this.state;
        return this.props.expertises[activeIndex];
    }

    getItems() {
        const {expertises} = this.props;
        return expertises.map((item, index) => (
            <div key={index}>
                <div className={classNames('MobileExpertise__item', {
                    'MobileExpertise__item--active': index === this.state.activeIndex,
                })}
                     onClick={() => {
                         this.refs.slick.slickGoTo(index);
                     }}
                >
                    <div className="MobileExpertise__item-inner">
                        {item.title}
                    </div>
                </div>
            </div>
        ));
    }

    getClientExperienceSection() {
        const {clientExperience = null} = this.getActiveExpertise();

        if (!clientExperience || !Array.isArray(clientExperience.images) || clientExperience.images.length === 0) {
            return null;
        }

        return (
            <div className="ExpertiseRotator__client-experience">
                <p className="text-bold">
                    <br/>
                    {clientExperience.title}
                </p>
                {
                    clientExperience.images.map(({image}, index) => (
                        <img key={index} src={image} />
                    ))
                }
            </div>
        );

    }

    render() {
        const {person, description, readMore} = this.getActiveExpertise();

        return (
            <div className="MobileExpertise section">
                <div className="section__title" dangerouslySetInnerHTML={{__html: this.props.title}}/>
                {/* switch buttons which might binded to the parent component */}
                {this.props.switchButtons}
                <br/>
                <div className="MobileExpertise__inner">
                    <div className="MobileExpertise__slick" ref="slickWrapper">
                        <Slick ref="slick" {...this.getSlickSettings()}>
                            {this.getItems()}
                        </Slick>
                    </div>
                    <br/>
                    <div className="MobileExpertise__description">
                        {description}
                    </div>
                    {readMore && (
                        <div>
                            <br/>
                            <a href={readMore.link} className="Expertise__read-more-link">{readMore.text}</a>
                        </div>
                    )}
                    {/** TODO : edit the className, so they are actually belong to MobileExpertise **/}
                    {
                        person && (
                            <div className="media">
                                <div className="media-left">
                                    <img className="ExpertiseRotator__person-image media-object" src={breakpointImage(person.image)}/>
                                </div>
                                <div className="media-body">
                                    {person.name}
                                    <br/>
                                    {person.job}
                                </div>
                            </div>
                        )
                    }
                    {this.getClientExperienceSection()}
                </div>
            </div>
        );
    }
}

MobileExpertise.SLICK_SETTINGS = {
    slidesToShow: 3,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: 0,
    autoplay: true,
    autoplaySpeed: 10000,
    responsive: [
        {
            breakpoint: breakpoints.md - 1, // sm-max
            settings: {
                autoplay: false,
            },
        },
    ]
};

MobileExpertise.propTypes = {
    title: PropTypes.string.isRequired,
    expertises: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        person: PropTypes.shape({
            image: PropTypes.object,
            name: PropTypes.string,
            job: PropTypes.string,
        }),
        clientExperience: PropTypes.shape({
            title: PropTypes.string.isRequired,
            images: PropTypes.oneOfType([
                PropTypes.bool,
                PropTypes.arrayOf(PropTypes.string.isRequired),
            ]).isRequired,
        }),
        readMore: PropTypes.shape({
            text: PropTypes.string.isRequired,
            link: PropTypes.string.isRequired,
        }),
    })),
    onChange: PropTypes.func,
    switchButtons: PropTypes.node,
    autoplay: PropTypes.bool,
};

MobileExpertise.defaultProps = {
    onChange: () => {
    },
    switchButtons: null,
    autoplay: true,
};

const nodes = document.querySelectorAll('[data-component="MobileExpertise"]');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const params = JSON.parse(node.getAttribute('data-params'));
    render(<MobileExpertise {...params} />, node);
}

export default MobileExpertise;
