import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import momentZone from "moment-timezone";
import classNames from "classnames";

import CountrySlider from "./CountrySlider"

// TODO : refactor this into helper functions

const nl2br = str => str ? str.replace(/\n/g, '<br>') : '';
const sp2br = str => str.replace(" ", '<br />');

class Offices extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activeCountry: props.items[0] ? props.items[0].name : null,
            activeLocationIndex: 0,
            time: momentZone(),
            countriesMap: [],
        };

        this.intervall = null;

        this.onCountrySliderItemClick = this.onCountrySliderItemClick.bind(this);
    }

    componentWillMount() {

        this.setState(() => {
            const countriesMap = {};
            for(let i = 0; i < this.props.items.length; i++) {
                const item = this.props.items[i];
                const country = item.name;

                if (countriesMap.hasOwnProperty(country)) {
                    countriesMap[country].push(item);
                } else {
                    countriesMap[country] = [item];
                }
            }

            return {countriesMap};
        });
    }

    componentDidMount() {
        this.intervall = setInterval(() => this.setState({time: momentZone()}), 500);
    }

    componentWillUnmount() {
        clearInterval(this.intervall);
    }

    getActiveItem() {
        if (this.state.activeCountry === null) {
            return {};
        }

        if (
            this.state.activeLocationIndex !== null &&
            typeof this.state.countriesMap[this.state.activeCountry] !== 'undefined' &&
            typeof this.state.countriesMap[this.state.activeCountry][this.state.activeLocationIndex] !== 'undefined'
        ) {
            return this.state.countriesMap[this.state.activeCountry][this.state.activeLocationIndex];
        }

        return this.state.countriesMap[this.state.activeCountry][0];
    }

    getMapImage() {
        const activeItem = this.getActiveItem();
        const mapArray = [];

        Object.keys(this.state.countriesMap).map((country, countryIndex) => {
            const locations = this.state.countriesMap[country];

            locations.map((location, locationIndex) => {
                mapArray.push(
                    <div
                        key={`${countryIndex}|${locationIndex}`}
                        className={classNames(
                            'Offices__map-inner',
                            {
                                'Offices__map-inner--active': location.map === activeItem.map,
                            }
                        )}
                        style={{backgroundImage: `url(${location.map})`}}
                    />
                );
            });

        });

        return mapArray;
    }

    onCountrySliderItemClick(activeCountry, activeLocationIndex = null) {
        if (activeLocationIndex !== null) {
            this.setState({activeCountry, activeLocationIndex});
        } else {
            this.setState({activeCountry});
        }
    }

    render() {
        const {arrowImage} = this.props;
        const {activeCountry, activeLocationIndex, countriesMap} = this.state;
        const activeItem = this.getActiveItem();

        return (
            <div className="container-fluid" style={{position: 'relative', padding: 0, marginTop: -1}}>
                <div className="Offices__map">
                    {this.getMapImage()}
                </div>
                <div className="Offices__container">
                    <div className="content">

                        <div className="Offices__titles--mobile col-sm-12 col-md-2">
                            <h3 className="section__title">{this.props.sectionTitle}</h3>
                            <ul className="Offices__list">
                                <CountrySlider
                                    onClick={this.onCountrySliderItemClick}
                                    items={countriesMap}
                                />
                            </ul>
                        </div>

                        <div className="Offices__mapText col-sm-12 col-md-9 col-lg-10">
                            <div className="Offices__text">
                                <div className="Offices__text__col">
                                    <h3 className="Offices__text-title text-yellow text-bold">
                                        {activeItem.city}
                                    </h3>
                                    <p>{activeItem.timeZone != null ? momentZone(this.state.time).tz(`${activeItem.timeZone}`).format('LT') : ''}</p>
                                    <p style={{display: 'block'}}>{activeItem.temp}</p>
                                </div>
                                <div className="Offices__text__col Offices__text__col--img">
                                    <img src={arrowImage} className="img-responsive full-height rel"/>
                                </div>
                                <div className="Offices__text__col w40">
                                    <div className="Offices__text--small">
                                        <p className="Offices__text-address" dangerouslySetInnerHTML={{__html: nl2br(activeItem.address)}}/>
                                        <p className="Offices__text-address Offices__text-address--expand">
                                            <a href={`mailto:${activeItem.email}`}>
                                                {activeItem.email}
                                            </a>
                                        </p>
                                        <p>
                                            <a href={`tel:${activeItem.phone}`}>
                                                {activeItem.phone}
                                            </a>
                                        </p>
                                        <p className="Offices__text-address Offices__text-address--expand">
                                            <a className="text-yellow"
                                               href={activeItem.link}>
                                                Visit the {activeItem.name} <br/>Website &gt;
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="Offices__titles--desktop col-sm-12 col-md-2 section">
                            <h3 className="section__title">{this.props.sectionTitle}</h3>
                            <ul className="Offices__list" style={{maxWidth: '6.2rem', marginLeft: 'auto'}}>
                                {Object.keys(this.state.countriesMap).map((country, index) => {
                                    if (this.state.countriesMap[country].length > 1 && activeCountry === country) {
                                        const locations = this.state.countriesMap[country];
                                        return (
                                            <li key={index}
                                                className={activeCountry === country ? 'active' : ''}
                                                onClick={() => this.setState({activeCountry: country})}
                                            >
                                                <div>
                                                    {country}
                                                </div>
                                                <ul className="Offices__list Offices__list--sub">
                                                    {locations.map((location, index) => (
                                                        <li
                                                            key={index}
                                                            className={activeCountry === country && activeLocationIndex === index ? 'active' : ''}
                                                            onClick={() => this.setState({activeLocationIndex: index})}
                                                        >
                                                            <div>
                                                                {location.city}
                                                            </div>
                                                        </li>
                                                    ))}
                                                </ul>
                                            </li>
                                        );
                                    }

                                    return (
                                        <li key={index} className={activeCountry === country ? 'active' : ''}
                                            onClick={() => this.setState({activeCountry: country, activeLocationIndex: null})}
                                        >
                                            <div>
                                            {country}
                                            </div>
                                        </li>
                                    );

                                })}
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

Offices.propTypes = {
    items: PropTypes.arrayOf(PropTypes.object).isRequired,
    arrowImage: PropTypes.string.isRequired,
    sectionTitle: PropTypes.string.isRequired,
};

export default Offices;

const nodes = document.querySelectorAll('[data-component=Offices]');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const params = JSON.parse(node.getAttribute('data-params'));
    render(<Offices {...params} />, node);
}
