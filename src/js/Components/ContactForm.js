import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import ContactFormCard from "./ContactFormCard";
import momentZone from "moment-timezone";

import breakpointImage from "../Utils/breakpointImage";

class ContactForm extends Component {
    constructor(props) {
        super(props);
        this.id = `ContactForm-${Date.now()}`;

        this.state = {
            time: momentZone()
        };

        this.intervall = null;
    }

    componentDidMount() {
        hbspt.forms.create({
            css: '',
            portalId: this.props.hubspot.portal_id,
            formId: this.props.hubspot.form_id,
            target: `#${this.id}`
        });

        this.intervall = setInterval(() => {
            this.setState({time: momentZone()})
        }, 500);
    }

    componentWillUnmount() {
        clearInterval(this.intervall);
    }

    getContactFormCard() {
        const {contacts} = this.props;
        return contacts.map((contact, index) => (
            <ContactFormCard key={index} {...contact} />
        ));
    }

    renderOffice(offices) {
        return (
            <div>
                <div className="row">
                    <div className="ContactForm__office-item ContactForm__office-item--big ContactForm__office-item--bold ContactForm__office-item--yellow">
                        <div>{this.props.country}</div>
                    </div>
                    {offices.map((office, index) => (
                        office !== null ?
                            (
                                <React.Fragment key={index}>
                                    <div className="col-xs-12 col-sm-5 col-lg-5">
                                        <div className="ContactForm__office-item ContactForm__office-item--big">
                                            {office.timeZone != null ? momentZone(this.state.time).tz(`${office.timeZone}`).format('LT') : ''}
                                        </div>
                                        <div className="ContactForm__office-item ContactForm__office-item--big">
                                            {office.temp}
                                        </div>
                                    </div>

                                    <div className="col-xs-12 col-sm-7 col-lg-7 pt-15 ContactForm__address-grid">
                                        <div className="ContactForm__office-item"
                                             dangerouslySetInnerHTML={
                                                 {__html:office.address.replace(/\n/g,'<br>')}
                                             }
                                        />
                                        <br/>
                                        <div className="ContactForm__office-item ContactForm__office-item--small">
                                            {office.email}
                                        </div>
                                        <div className="ContactForm__office-item ContactForm__office-item--small">
                                            {office.phone}
                                        </div>
                                        <br/>
                                    </div>
                                </React.Fragment>
                            ) : null
                    ))}
                </div>

            </div>
        );
    }

    render() {
        const office1 = this.props.office;
        const office2 = this.props.office2;
        const office3 = this.props.office3;
        const office4 = this.props.office4;

        return (
            <div className="ContactForm">
                <div
                    className="ContactForm__inner"
                    style={{backgroundImage: `url(${breakpointImage(office1.mapImage)})`}}
                >
                    <div className="section">
                        <div className="container">
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="ContactForm__business-cards">
                                        {this.getContactFormCard()}
                                    </div>

                                    <div className="ContactForm__office">
                                        <div className="container-fluid">
                                            {this.renderOffice([office1, office2, office3, office4])}
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="ContactForm__business-cards-mobile-title">{this.props.formText.title}</div>
                                    <div className="HubspotForm">
                                        <div id={this.id}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const officePropType = PropTypes.shape({
    title: PropTypes.string.isRequired,
    city: PropTypes.string.isRequired,
    timeZone: PropTypes.string.isRequired,
    temp: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
    mapImage: PropTypes.object.isRequired,
    shortCode: PropTypes.string.isRequired,
});

ContactForm.propTypes = {
    country: PropTypes.string.isRequired,
    office: officePropType,
    office2: officePropType,
    office3: officePropType,
    office4: officePropType,

    contacts: PropTypes.arrayOf(PropTypes.shape({
        image: PropTypes.object.isRequired,
        name: PropTypes.string.isRequired,
        email: PropTypes.string,
        contact_title: PropTypes.string,
        contact_title_secondary: PropTypes.string,
        linkedin: PropTypes.string,
    })),

    arrowImage: PropTypes.string.isRequired,
    formText: PropTypes.shape({
        title: PropTypes.string.isRequired,
        // firstName: PropTypes.string.isRequired,
        // lastName: PropTypes.string.isRequired,
        // email: PropTypes.string.isRequired,
        // message: PropTypes.string.isRequired,
        // submit: PropTypes.string.isRequired,
    }),
    hubspot: PropTypes.shape({
        portal_id: PropTypes.string.isRequired,
        form_id: PropTypes.string.isRequired
    })
    // formSubmitUrl: PropTypes.string.isRequired,
};

const nodes = document.querySelectorAll('[data-component=ContactForm]');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const params = JSON.parse(node.getAttribute('data-params'));
    render(<ContactForm {...params} />, node);
}
