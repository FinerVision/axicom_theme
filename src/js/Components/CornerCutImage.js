import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";

import breakpointImage from "../Utils/breakpointImage";

class CornerCutImage extends Component {
    render() {
        const {src} = this.props;
        return (
            <div className="CornerCutImage">
                <div className="CornerCutImage__inner">
                    <div className="CornerCutImage__real" style={{backgroundImage: `url(${breakpointImage(src)})`}} />
                </div>
            </div>
        );
    }
}

CornerCutImage.propTypes = {
    src: PropTypes.object.isRequired,
};

export default CornerCutImage;

const nodes = document.querySelectorAll('[data-component=CornerCutImage]');

for(let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const params = JSON.parse(node.getAttribute('data-params'));
    render(<CornerCutImage {...params} />, node);
}
