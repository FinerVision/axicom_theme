import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import $ from "jquery";
import classNames from "classnames";

class CookieBanner extends Component {

    constructor(props) {
        super(props);

        this.shouldInitialize = !this.isCookiePolicyAccepted();

        // binding
        this.onContinueButtonClick = this.onContinueButtonClick.bind(this);

        this.state = {
            accepted: false,
        };
    }

    isCookiePolicyAccepted() {
        const key = CookieBanner.COOKIE_POLICY_ACCEPTING_KEY;
        const value = window.localStorage.getItem(key) || false;
        return value === CookieBanner.COOKIE_POLICY_ACCEPTING_VALUE;
    }

    onContinueButtonClick() {
        const key = CookieBanner.COOKIE_POLICY_ACCEPTING_KEY;
        window.localStorage.setItem(key, CookieBanner.COOKIE_POLICY_ACCEPTING_VALUE);
        this.setState({accepted: true});
    }

    render() {

        if (!this.shouldInitialize) {
            return (
                <div className="CookieBanner CookieBanner--accepted">
                    { /** nothing **/ }
                </div>
            );
        }

        return (
            <div className={classNames(
                'CookieBanner',
                {
                    'CookieBanner--accepted': this.state.accepted,
                }
            )}>
                <div className="content">
                    <div className="container-fluid">
                        <div className="row">

                            <div className="col-md-2">
                                <div className="CookieBanner__title">
                                    <h3>{this.props.title}</h3>
                                </div>
                            </div>

                            <div className="col-md-7">
                                <div className="CookieBanner__content" dangerouslySetInnerHTML={{__html: this.props.content}} />
                            </div>

                            <div className="col-md-2">
                                <div className="CookieBanner__buttons">
                                    <a
                                        className="CookieBanner__button"
                                        onClick={this.onContinueButtonClick}
                                    >
                                        {this.props.buttons.continue.title}
                                    </a>
                                    <a
                                        className="CookieBanner__button"
                                        href={this.props.buttons.findOutMore.url}
                                    >
                                        {this.props.buttons.findOutMore.title}
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

CookieBanner.COOKIE_POLICY_ACCEPTING_KEY = 'axicomUserAcceptedCookiePolicy';
CookieBanner.COOKIE_POLICY_ACCEPTING_VALUE = 'yes';

CookieBanner.propTypes = {
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    buttons: PropTypes.shape({
        continue: PropTypes.shape({
            title: PropTypes.string.isRequired,
        }),
        findOutMore: PropTypes.shape({
            title: PropTypes.string.isRequired,
            url: PropTypes.string.isRequired,
        }),
    }),
};

CookieBanner.defaultProps = {
    //
};

export default CookieBanner;

$('[data-component=CookieBanner]').each(function() {
    render(<CookieBanner {...$(this).data('params')} />, this);
});

