import $ from "jquery";

$(document).ready(function () {

    const state = {
        open: false,
    };

    // elements
    const $featuredNews = $('.FeaturedNews');
    const $content = $featuredNews.find('.FeaturedNews__content');

    $featuredNews.on('click', function () {

        if (state.open) {
            return;
        }

        $content.slideDown(function () {
            state.open = true;

            // auto scroll to the such article
            const $container = $('#container');
            const navScrollHeight = $('.Nav').height();
            const featuredNewsTop = $featuredNews.find('.News__row-content').offset().top;
            const containerScrollTop = $container.scrollTop();

            $container.animate({
                scrollTop: containerScrollTop + featuredNewsTop - navScrollHeight,
            }, 400);

        });
    });

    $(document).on('click', '.News__content-close, .FeaturedNews__inner', function () {
        if (!state.open) {
            return;
        }

        $content.slideUp(function () {
            state.open = false;
        });
    });
});
