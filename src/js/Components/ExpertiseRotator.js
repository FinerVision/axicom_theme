import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import classNames from "classnames";

import MobileExpertise from "./MobileExpertise";
import breakpointImage from "../Utils/breakpointImage";

const ROTATION_INCREMENT = 60;
const SLIDE_UPDATING_INTERVAL = 10000;

class ExpertiseRotator extends Component {

    static transform(transform) {
        return {
            MsTransform: transform,
            MozTransform: transform,
            WebkitTransform: transform,
            transform: transform,
        };
    }

    constructor(props) {
        super(props);

        const {serviceItems} = this.props;
        this.intervalId = null;

        // Assume there are only two kinds of expertises, much work will caused if want to add more
        this.state = {
            // Either ExpertiseRotator.sector or ExpertiseRotator.service
            activeSection: Array.isArray(serviceItems) && serviceItems.length !== 0 ? ExpertiseRotator.service : ExpertiseRotator.sector,
            activeIndex: 0,
            rotation: 60,
            autoplay: true,
        };
    }

    componentDidMount() {

        this.startSlidingInterval();
        this.preloadImages();

        let hashId = location.hash.replace('#','');
        if (this.props.serviceItems.length>0) {
            let active = -1;
            this.props.serviceItems.map((element,index) => {
                if (element.slug === hashId) active = index;
            });

            if (active!== -1) {
               this.setState({activeIndex: active});
            }
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.activeIndex !== this.state.activeIndex) {
            this.props.onChange(this.state.activeIndex);
        }
    }

    startSlidingInterval() {
        this.intervalId = window.setInterval(() => this.goNext(), SLIDE_UPDATING_INTERVAL);
    }

    endSlidingInterval() {
        window.clearInterval(this.intervalId);
        this.intervalId = null;
    }

    preloadImages() {

        const preload = url => (new Image()).src = url;

        const {serviceItems, sectorItems} = this.props;

        // services
        for(let i = 0; i < serviceItems.length; i++) {
            const item = serviceItems[i];

            if (typeof item !== 'object' || JSON.stringify(item) === '{}') {
                continue;
            }

            // services icon
            item.icon && preload(item.icon);

            // services person image
            item.person && item.person.image && preload(breakpointImage(item.person.image));
        }

        // sectors
        for(let i = 0; i < sectorItems.length; i++) {
            const item = sectorItems[i];

            if (typeof item !== 'object' || JSON.stringify(item) === '{}') {
                continue;
            }

            // sector icon
            item.icon && preload(item.icon);

            // sector client logo images
            const {images} = item.clientExperience;
            if (images) {
                for(let j = 0; j < images.length; j++) {
                    const {image} = images[j];
                    image && preload(image);
                }
            }

        }

    }

    getActiveSections() {
        const {activeSection} = this.state;
        switch(activeSection) {
            case ExpertiseRotator.service:
                return this.props.serviceItems;
            case ExpertiseRotator.sector:
                return this.props.sectorItems;
            default:
                return null;
        }
    }

    getActiveItem() {
        const {activeSection, activeIndex} = this.state;
        switch(activeSection) {
            case ExpertiseRotator.service:
                return this.props.serviceItems[activeIndex];
            case ExpertiseRotator.sector:
                return this.props.sectorItems[activeIndex];
            default:
                return null;
        }
    }

    getSwitch() {
        if (!this.props.showSwitch) {
            return null;
        }

        const {serviceItems, sectorItems} = this.props;
        const {activeSection} = this.state;
        const {serviceTitle, sectorTitle} = this.props.subtitles;

        return (
            <div className="ExpertiseRotator__switch">
                <span
                    className={classNames('ExpertiseRotator__switch-text', {
                        'ExpertiseRotator__switch-text--active': activeSection === ExpertiseRotator.service,
                        'hidden': !Array.isArray(serviceItems) || serviceItems.length === 0
                    })}
                    onClick={() => this.setState({activeSection: ExpertiseRotator.service})}
                >{serviceTitle}</span>
                <span
                    className={classNames('ExpertiseRotator__switch-divider', {
                        'hidden': (!Array.isArray(serviceItems) || serviceItems.length === 0) || (!Array.isArray(sectorItems) || sectorItems.length === 0)
                    })}
                >&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</span>
                <span
                    className={classNames('ExpertiseRotator__switch-text', {
                        'ExpertiseRotator__switch-text--active': activeSection === ExpertiseRotator.sector,
                        'hidden': !Array.isArray(sectorItems) || sectorItems.length === 0
                    })}
                    onClick={() => this.setState({activeSection: ExpertiseRotator.sector})}
                >{sectorTitle}</span>
            </div>
        )
    }

    getSlideButtonTitle() {
        const {slideArrows} = this.props;
        return (
            <div className="ExpertiseRotator__slide-buttons">
                <img
                    className="ExpertiseRotator__slide-button ExpertiseRotator__slide-button-prev"
                    src={slideArrows.prev}
                    onClick={() => {
                        this.endSlidingInterval();
                        this.startSlidingInterval();
                        this.goPrev();
                    }}
                />
                <img
                    className="ExpertiseRotator__slide-button ExpertiseRotator__slide-button-next"
                    src={slideArrows.next}
                    onClick={() => {
                        this.endSlidingInterval();
                        this.startSlidingInterval();
                        this.goNext();
                    }}
                />
                <span className="ExpertiseRotator__slide-text">{(this.getActiveItem() || {}).title}</span>
            </div>
        );
    }

    handleIndexChange(index) {
        const length = this.getActiveSections().length;
        const {activeIndex} = this.state;
        const diff = (index - activeIndex + length) % length;

        switch(diff) {
            // just assume the length is always 6
            case 1:
                this.goNext(); break;
            case 2:
                this.goNext(() => this.goNext()); break;
            case 3:
                this.goNext(() => this.goNext(() => this.goNext())); break;
            case 4:
                this.goPrev(() => this.goPrev()); break;
            case 5:
                this.goPrev(); break;
        }
    }

    getRotatorSlides() {
        const {activeSection, activeIndex} = this.state;
        let slideItems = {};

        switch(activeSection) {
            case ExpertiseRotator.service:
                slideItems = this.props.serviceItems;
                break;
            case ExpertiseRotator.sector:
                slideItems = this.props.sectorItems;
                break;
            default:
                return null;
        }

        return slideItems.map((items, index) => {
            const degrees = (index - 1) * ROTATION_INCREMENT + this.state.rotation;

            return (
                <div
                    key={index}
                    className={classNames('ExpertiseRotator__desktop-rotator-slide', {
                        'ExpertiseRotator__desktop-rotator-slide--active': index === activeIndex,
                    })}
                    style={this.constructor.transform(`rotate(${degrees}deg)`)}
                >
                    <div
                        className="ExpertiseRotator__desktop-rotator-slide-inner"
                        style={this.constructor.transform(`rotate(${-degrees}deg)`)}
                        onClick={() => this.handleIndexChange(index)}
                    >
                        <img className="ExpertiseRotator__icon" src={items.icon} />
                        <h3 className="ExpertiseRotator__title">{items.title}</h3>
                    </div>
                </div>
            );
        });
    }

    getClientExperienceSection() {
        const {clientExperience} = this.getActiveItem();

        if (!Array.isArray(clientExperience.images) || clientExperience.images.length === 0) {
            return null;
        }

        return (
            <div className="ExpertiseRotator__client-experience">
                <p>
                    <br />
                    {clientExperience.title}
                </p>
                {
                    clientExperience.images.map((image, index) => (
                        image && <img key={index} src={image} />
                    ))
                }
            </div>
        );
    }

    goPrev(callback) {
        callback = typeof callback === 'function' ? callback : () => {};
        const {activeIndex} = this.state;
        const length = this.getActiveSections().length;
        const newIndex = activeIndex - 1;
        const rotation = this.state.rotation + ROTATION_INCREMENT;
        this.setState({rotation, activeIndex: newIndex >= 0 ? newIndex : length - 1}, () => callback());
    }

    goNext(callback) {
        callback = typeof callback === 'function' ? callback : () => {};
        const {activeIndex} = this.state;
        const length = this.getActiveSections().length;
        const newIndex = activeIndex + 1;
        const rotation = this.state.rotation - ROTATION_INCREMENT;
        this.setState({rotation, activeIndex: newIndex < length ? newIndex : 0}, () => callback());
    }

    render() {
        const {title, descriptionAbove, extendBottom, pushBottom} = this.props;
        const activeItem = this.getActiveItem();
        const {person = null, clientExperience = null, readMore = null} = activeItem;

        return (
            <div className={classNames(
                'ExpertiseRotator',
                {
                    'ExpertiseRotator--extend-bottom': extendBottom,
                    'ExpertiseRotator--push-bottom': pushBottom,
                }
            )}>
                {/* desktop site */}
                <div className="visible-md visible-lg">

                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-sm-12 col-md-6">
                                {/* "6 triangle" background */}
                                <div className="ExpertiseRotator__col-placehold" />
                                <div className="ExpertiseRotator__desktop-background">
                                    <div className="ExpertiseRotator__desktop-background-slice" />
                                    <div className="ExpertiseRotator__desktop-background-slice" />
                                    <div className="ExpertiseRotator__desktop-background-slice" />
                                    <div className="ExpertiseRotator__desktop-background-slice" />
                                    <div className="ExpertiseRotator__desktop-background-slice" />
                                    <div className="ExpertiseRotator__desktop-background-slice" />
                                </div>
                                <div className="ExpertiseRotator__desktop-rotator">
                                    {this.getRotatorSlides()}
                                    <div className="ExpertiseRotator__desktop-rotator-slice">

                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-12 col-md-6">
                                <div className="ExpertiseRotator__col-placehold">
                                    <div className="ExpertiseRotator__right-top">
                                        {
                                            descriptionAbove && (
                                                <div className="ExpertiseRotator__description ExpertiseRotator__description--above">{(activeItem || {}).description}</div>
                                            )
                                        }
                                        {this.getSwitch()}
                                        <br />
                                        {this.getSlideButtonTitle()}
                                    </div>
                                    <div className="ExpertiseRotator__right-bottom">
                                        <div
                                            className="ExpertiseRotator__description"
                                            onMouseEnter={this.endSlidingInterval.bind(this)}
                                            onMouseLeave={this.startSlidingInterval.bind(this)}
                                        >
                                            { !descriptionAbove && (activeItem || {}).description }
                                            { readMore && <a href={readMore.link} className="Expertise__read-more-link">{readMore.text}</a> }
                                        </div>
                                        {
                                            person && JSON.stringify(person) !== '{}' && (
                                                <div className="media">
                                                    <div className="media-left">
                                                        <img className="ExpertiseRotator__person-image media-object" src={breakpointImage(person.image)} />
                                                    </div>
                                                    <div className="media-body">
                                                        {person.name}
                                                        <br />
                                                        {person.job}
                                                    </div>
                                                </div>
                                            )
                                        }
                                        { clientExperience && this.getClientExperienceSection()}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="ExpertiseRotator__title-section">
                        <div className="content">
                            <div className="container-fluid">
                                <div className="row">
                                    <div className="col-sm-12 col-md-offset-10 col-md-2">
                                        <h3 className="section__title" dangerouslySetInnerHTML={{__html: title}} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                {/* mobile site */}
                <div className="hidden-md hidden-lg">
                    <div className="container-fluid">
                        <div className="content">
                            {/* NOTE : this ExpertiseRotator components rotating interval is depends on MobileExpertise's slick */}
                            <MobileExpertise
                                title={title}
                                expertises={this.getActiveSections()}
                                switchButtons={this.getSwitch()}
                                showSwitch={this.state.mobileShowSwitch}
                                autoplay={this.state.autoplay}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

ExpertiseRotator.sector = 'sector';
ExpertiseRotator.service = 'service';

ExpertiseRotator.propTypes = {
    title: PropTypes.string.isRequired,
    showSwitch: PropTypes.bool,
    mobileShowSwitch: PropTypes.bool,
    extendBottom: PropTypes.bool,
    pushBottom: PropTypes.bool,
    descriptionAbove: PropTypes.bool,
    defaultSection: PropTypes.oneOf([
        ExpertiseRotator.sector,
        ExpertiseRotator.service,
    ]),
    subtitles: PropTypes.shape({
        serviceTitle: PropTypes.string,
        sectorTitle: PropTypes.string,
    }),
    serviceItems: PropTypes.arrayOf(PropTypes.shape({
        icon: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        person: PropTypes.shape({
            image: PropTypes.object,
            name: PropTypes.string,
            job: PropTypes.string,
        }),
        readMore: PropTypes.shape({
            text: PropTypes.string.isRequired,
            link: PropTypes.string.isRequired,
        }),
        slug: PropTypes.string
    })),
    sectorItems: PropTypes.arrayOf(PropTypes.shape({
        icon: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,

        clientExperience: PropTypes.shape({
            title: PropTypes.string.isRequired,
            images: PropTypes.oneOfType([
                PropTypes.bool,
                PropTypes.arrayOf(PropTypes.string.isRequired),
            ]).isRequired,
        }),
    })),
    slideArrows: PropTypes.shape({
        prev: PropTypes.string.isRequired,
        next: PropTypes.string.isRequired,
    }),

    // callbacks
    onChange: PropTypes.func,

};

ExpertiseRotator.defaultProps = {
    showSwitch: false,
    mobileShowSwitch: false,
    extendBottom: false,
    pushBottom: false,
    descriptionAbove: false,
    subtitles: {

        serviceTitle: 'Service',
        sectorTitle: 'Sector',
    },
    sectorItems: [],
    serviceItems: [],
    onChange: () => {},
};

export default ExpertiseRotator;

const nodes = document.querySelectorAll('[data-component=ExpertiseRotator]');

for(let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const params = JSON.parse(node.getAttribute('data-params'));
    render(<ExpertiseRotator {...params} />, node);
}

setTimeout(function() {
    //window.dispatchEvent(new Event('resize'));
}, 100);
