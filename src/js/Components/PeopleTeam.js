import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import Slick from "react-slick";

import LinkedinButton from "./LinkedinButton";
import breakpointImage from "../Utils/breakpointImage";

const SLICK_OPTIONS = {
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 991, // sm max
            settings: {
                slidesToShow: 4,
            },
        },
        {
            breakpoint: 767, // xs max
            settings: {
                slidesToShow: 2,
            },
        },
        {
            breakpoint: 468,
            settings: {
                slidesToShow: 1,
            },
        }
    ]
};

export default class PeopleTeam extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activePersonId: -1
        };
    }

    mobileToggleShowProfile(id) {
        if (id !== -1 && id === this.state.activePersonId) {
            id = -1;
        }

        this.setState({activePersonId: id});
    }

    getPersonOpacity(index) {
        if (this.state.activePersonId === -1 || (this.state.activePersonId !== -1 && this.state.activePersonId === index)) {
            return 1;
        }
        return 0.6;
    }

    getPeopleTeamList() {
        const {items} = this.props;
        return items.map((item, index) => (
            <div key={index}>
                <div className="PeopleTeam__person" ref={`person${index}`}
                     style={{opacity: this.getPersonOpacity(item.id)}}>
                    <div
                        className="PeopleTeam__person-image"
                        style={{backgroundImage: `url(${breakpointImage(item.image)})`}}
                        onClick={() => {
                            this.mobileToggleShowProfile(item.id);
                        }}
                    />
                    <h3>
                        <strong className="PeopleTeam__person-name">{item.name}</strong><br/>
                        <div className="PeopleTeam__person-title">{item.title}</div>
                        <LinkedinButton href={`${item.linkedin}`} />
                    </h3>
                    <div
                        className="PeopleTeam__person-intro"
                    >
                        <p dangerouslySetInnerHTML={{__html: item.intro}}/>
                    </div>
                </div>
            </div>
        ));
    }

    render() {
        return (
            <div className="PeopleTeam" ref="root">
                <Slick {...SLICK_OPTIONS} beforeChange={() => this.setState({activePersonId: -1})}>
                    {this.getPeopleTeamList()}
                </Slick>
            </div>
        );
    }
}

PeopleTeam.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
        image: PropTypes.object.isRequired,
        name: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        intro: PropTypes.string.isRequired,
        linkedin: PropTypes.string.isRequired,
    }))
};

const nodes = document.querySelectorAll('[data-component=PeopleTeam]');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const params = JSON.parse(node.getAttribute('data-params'));
    render(<PeopleTeam {...params}/>, node);
}
