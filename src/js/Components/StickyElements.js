import $ from "jquery";
import jQuery from "jquery";

(function ($) {
    $.fn.hasScrollBar = function () {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);
$(document).ready(function () {

    if ($('.News').length > 0) {
        if (checkBrowser() === 'Chrome' && navigator.appVersion.indexOf("Win") != -1) {
            $('#container').css('overflow-y', 'overlay');
        }
        setTimeout(function () {
            let titleHeight;
            titleHeight = $('.NewsSingle .HeroParallax__subtitle').height();
            let socialOffset = titleHeight + 70;

            let socialIcons = $('.SocialIcons__list');
            socialIcons.css('top', socialOffset);
            $('#container').scroll(function () {

                if ($('.content-news').offset() !== undefined && $('.content-news').offset().top <= 70) {
                    if ($('.social-reference').find('.SocialIcons__list').length > 0) {
                        socialIcons.insertAfter('.Nav');
                        socialIcons.css('top', socialOffset + 70 + 'px');
                        socialIcons.css('bottom', 'inherit');
                        if (osIs('Windows')) socialIcons.css('transform', `translateX(-${405}px)`);
                    }

                } else {
                    if ($('.social-reference').find('.SocialIcons__list').length == 0) {
                        socialIcons.prependTo('.social-reference');
                        socialIcons.css('top', socialOffset + 'px');
                        socialIcons.css('bottom', 'inherit');
                        if (osIs('Windows')) socialIcons.css('transform', `translateX(-${405}px)`);
                    }

                }
                let elementOffset = document.getElementById("main").offsetTop;

                let remaining = $(this).prop('scrollHeight') - elementOffset - ($(this).scrollTop() - $('.Header').height());
                if (remaining <= 890) {
                    socialIcons.prependTo('.social-reference');
                    socialIcons.css('bottom', '0');
                    socialIcons.css('top', 'inherit');
                }
                if (window.matchMedia("(min-width: 600px)").matches) {
                    let reference = $('main').offset().top;
                    if (reference < -120) {
                        $('.sidebar').css('position', 'fixed');
                        $('.sidebar').css('top', Math.abs(reference) + elementOffset + 110);
                    } else {
                        $('.sidebar').css('position', 'relative');
                        $('.sidebar').css('top', 'inherit');
                    }
                }

            });
        }, 400);
    }
});

function checkBrowser() {
    let c, f, m8, m9, browser = undefined;
    c = navigator.userAgent.search("Chrome");
    f = navigator.userAgent.search("Firefox");
    m8 = navigator.userAgent.search("MSIE 8.0");
    m9 = navigator.userAgent.search("MSIE 9.0");
    if (c > -1) {
        browser = "Chrome";
    } else if (f > -1) {
        browser = "Firefox";
    } else if (m9 > -1) {
        browser = "MSIE 9.0";
    } else if (m8 > -1) {
        browser = "MSIE 8.0";
    }
    return browser;
}

function osIs(osName) {
    let userAgent = window.navigator.userAgent,
        platform = window.navigator.platform,
        macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'],
        windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
        iosPlatforms = ['iPhone', 'iPad', 'iPod'],
        os = null;

    if (macosPlatforms.indexOf(platform) !== -1) {
        os = 'Mac OS';
    } else if (iosPlatforms.indexOf(platform) !== -1) {
        os = 'iOS';
    } else if (windowsPlatforms.indexOf(platform) !== -1) {
        os = 'Windows';
    } else if (/Android/.test(userAgent)) {
        os = 'Android';
    } else if (!os && /Linux/.test(platform)) {
        os = 'Linux';
    }

    return os === osName;
}