import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import $ from "jquery";

class LinkedinButton extends Component {
    render() {
        return (
            <a
                {...this.props}
                target="_blank"
                className="LinkedinButton"
            >&nbsp;</a>
        )
    }
}

LinkedinButton.propTypes = {
    href: PropTypes.string.isRequired,
};

export default LinkedinButton;

$('[data-component=LinkedinButton]').each(function() {
    render(<LinkedinButton {...$(this).data('params')} />, this);
});
