const container = document.querySelector('#container');
const people = [];
const peopleImages = [];

const inViewport = (element, container) => {
    const elementBox = element.getBoundingClientRect();
    const scrolled = container.scrollTop;
    return (elementBox.top + elementBox.height) - scrolled <= 0;
};

const parallax = () => {
    peopleImages.map(person => {
        if (!inViewport(person, container)) {
            return;
        }

        const containerBox = container.getBoundingClientRect();
        const personBox = person.getBoundingClientRect();

        const percentage = Math.min(100, (100 / containerBox.height) * (personBox.top + personBox.height));
        const velocity = parseFloat(person.getAttribute('data-velocity'));

        person.style.transform = `translate(0, -${(100 - percentage) * velocity}%)`;
    });

    people.map(person => {
        if (!inViewport(person, container)) {
            return;
        }

        const containerBox = container.getBoundingClientRect();
        const personBox = person.getBoundingClientRect();
        const percentage = Math.min(100, (100 / containerBox.height) * (personBox.top + personBox.height));
        const velocity = parseFloat(person.getAttribute('data-velocity'));
        const start = parseFloat(person.getAttribute('data-start'));

        person.style.backgroundPosition = `0 ${start - ((100 - percentage) * velocity)}%`;
    });
};

const person = document.querySelectorAll('.Person');

if (person.length > 0) {
    document.querySelectorAll('.Person').forEach(person => people.push(person));
    document.querySelectorAll('.Person__img').forEach(person => peopleImages.push(person));
    container.addEventListener('scroll', parallax);
}
