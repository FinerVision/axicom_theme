const Category = expertise => {
    if (!expertise) {
        return;
    }

    const activeIndex = 1;
    const picker = expertise.querySelector('.Expertise__picker');
    const items = expertise.querySelector('.Expertise__items');
    const categoryPickers = expertise.querySelectorAll('[data-picker]');
    const pickerPrev = picker.querySelector('.Expertise__picker__arrow--prev');
    const pickerNext = picker.querySelector('.Expertise__picker__arrow--next');
    const expertiseNodes = expertise.querySelectorAll('.Expertise__item');
    const titleNodes = picker.querySelectorAll('.Expertise__picker__title');
    const descriptionNodes = expertise.querySelectorAll('.Expertise__description');
    const expertises = [];
    const titles = [];
    const descriptions = [];
    const state = {
        activeIndex,
        rotation: 0
    };
    let currentInterval = null;

    for (let i = 0; i < expertiseNodes.length; i++) {
        const expertiseNode = expertiseNodes[i];
        const titleNode = titleNodes[i];
        const descriptionNode = descriptionNodes[i];
        const expertiseOrder = expertiseNode.getAttribute('data-order');
        expertises[expertiseOrder] = expertiseNode;
        titles[expertiseOrder] = titleNode;
        descriptions[expertiseOrder] = descriptionNode;
    }

    const updateExpertises = () => {
        items.style.transform = `rotate(${state.rotation}deg)`;

        for (let i = 0; i < expertises.length; i++) {
            expertises[i].style.transform = `rotate(${-state.rotation}deg)`;

            expertises[i].classList.remove('Expertise__item--active');
            titles[i].classList.remove('Expertise__picker__title--active');
            descriptions[i].classList.remove('Expertise__description--active');
        }
        expertises[state.activeIndex].classList.add('Expertise__item--active');
        titles[state.activeIndex].classList.add('Expertise__picker__title--active');
        descriptions[state.activeIndex].classList.add('Expertise__description--active');
        resetAutoSlideInterval();
        typeof window.expertiseUpdated === 'function' && window.expertiseUpdated(state.activeIndex);
    };

    const next = () => {
        state.activeIndex = state.activeIndex === expertises.length - 1 ? 0 : state.activeIndex + 1;
        state.rotation -= ROTATION_INCREMENT;
        updateExpertises();
    };

    const prev = () => {
        state.activeIndex = state.activeIndex === 0 ? expertises.length - 1 : state.activeIndex - 1;
        state.rotation += ROTATION_INCREMENT;
        updateExpertises();
    };

    const go = index => {
        if (window.innerWidth < 992) {
            return;
        }

        state.activeIndex = index;
        state.rotation = -ROTATION_INCREMENT * (state.activeIndex - activeIndex);
        updateExpertises();
    };

    const setAutoSlideInterval = () => {
        // to ensure there are one interval is running only
        currentInterval && clearInterval(currentInterval);
        currentInterval = setInterval(next, AUTO_SLIDE_INTERVAL);
    };

    const clearAutoSlideInterval = () => {
        clearInterval(currentInterval);
        currentInterval = null;
    };

    const resetAutoSlideInterval = () => {
        clearAutoSlideInterval();
        setAutoSlideInterval();
    };

    pickerPrev.addEventListener('click', prev);
    pickerNext.addEventListener('click', next);
    expertise.addEventListener('mousemove', clearAutoSlideInterval);
    expertise.addEventListener('mouseout', setAutoSlideInterval);

    for (let i = 0; i < categoryPickers.length; i++) {
        const categoryPicker = categoryPickers[i];
        const category = categoryPicker.getAttribute('data-picker');
        categoryPicker.addEventListener('click', () => {
            document.querySelector('.Expertise--active').classList.remove('Expertise--active');
            document.querySelector(`.Expertise--${category}`).classList.add('Expertise--active');
        });
    }

    for (let i = 0; i < items.children.length; i++) {
        const item = items.children[i];
        const index = item.getAttribute('data-order');
        item.addEventListener('click', () => go(parseInt(index)));
    }

    // run
    setAutoSlideInterval();
};

const AUTO_SLIDE_INTERVAL = 4000;
const ROTATION_INCREMENT = 60;
const expertises = document.querySelectorAll('.Expertise');

for (let i = 0; i < expertises.length; i++) {
    Category(expertises[i]);
}
