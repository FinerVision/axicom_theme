import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import Select from "react-select";

class CustomSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: props.defaultValue,
        };

        this.filterEvent = props.filterEvent ? props.filterEvent : null;
    }

    componentDidMount() {
        if (this.state.value === null) {
            this.setState({value: Object.keys(this.props.items)[0]});
        }
    }

    /**
     * get the formatted options
     * @see https://github.com/JedWatson/react-select
     * @returns {Array} array of object includes a value and label
     */
    getOptions() {
        const options = [];
        const {items} = this.props;
        for (let key in items) {

            const value = items[key];
            options.push({
                value: key,
                label: value,
            });
        }
        return options;
    }

    triggerEvent(value) {
        console.log('filter event trigger test');
        if (this.filterEvent) {
            console.log('filter event trigger');
            let event;
            if (window.CustomEvent) {
                event = new CustomEvent(this.filterEvent);
                event.value = value;
            } else {
                event = document.createEvent(this.filterEvent);
                event.initCustomEvent(this.filterEvent, true, true, {value:value});
                event.value = value;
            }
            window.dispatchEvent(event);
        }
    }

    render() {
        const {name} = this.props;
        const {value} = this.state;
        console.log(this.props);
        return (
            <div className="CustomSelect">
                <Select
                    name={name}
                    value={value}
                    removeSelected={true}
                    options={this.getOptions()}
                    clearable={false}
                    onChange={({value}) => {
                        this.triggerEvent(value);
                        this.setState({value});
                    }}
                />
            </div>
        );
    }
}

CustomSelect.propTypes = {
    name: PropTypes.string.isRequired,
    items: PropTypes.objectOf(PropTypes.string).isRequired,
    defaultValue: PropTypes.string,
    filterEvent: PropTypes.string,
};

CustomSelect.defaultProps = {
    defaultValue: null,
};

const nodes = document.querySelectorAll('[data-component=CustomSelect]');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const params = JSON.parse(node.getAttribute('data-params'));
    render(<CustomSelect {...params} />, node);
}
