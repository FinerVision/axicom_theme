import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import Slick from "react-slick";

const SLICK_OPTIONS = {
    arrows: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: 0,
    infinite: true,
};

class NavSlider extends Component {
    getSlickItems() {
        const {items, clickToSwitchPage} = this.props;

        return items.map((item, index) => (
            <div key={index}>
                <div className="NavSlider__slide">
                    <a
                        className="NavSlider__url"
                        href={clickToSwitchPage ? item.url : 'javascript:void(0)'}
                        onClick={() => {
                            if (this.props.onClick) {
                                this.props.onClick(item.id);
                            }
                            const slick = this.refs;
                            slick.slick.slickGoTo(index);
                        }}
                    >
                        {item.title}
                    </a>
                </div>
            </div>
        ));
    }

    render() {

        // clone
        const slickSettings = JSON.parse(JSON.stringify(SLICK_OPTIONS));
        slickSettings.afterChange = (...props) => {
            this.props.onChange(...props);
        };

        return (
            <div className="NavSlider">
                <Slick {...slickSettings} ref="slick">
                    {this.getSlickItems()}
                </Slick>
            </div>
        );
    }
}


NavSlider.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
        id: PropTypes.number
    })).isRequired,
    clickToSwitchPage: PropTypes.bool,
    onChange: PropTypes.func,
};

NavSlider.defaultProps = {
    clickToSwitchPage: true,
    onChange: () => {
    },
};

export default NavSlider;

const nodes = document.querySelectorAll('[data-component=NavSlider]');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const params = JSON.parse(node.getAttribute('data-params'));
    render(<NavSlider {...params} />, node);
}
