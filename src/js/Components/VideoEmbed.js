import $ from "jquery";

$(document).ready(function () {
    if($('.News').length > 0){
        let $str = $('.NewsSingle__content').html();
        // The array of regex patterns to look for
        let $format_search = [
            /\[embed\](.*?)\[\/embed\]/ig
            // note: NO comma after the last entry
        ];
        let $format_replace = [
            embedVideo
        ];

        // Perform the actual conversion
        for (let i = 0; i < $format_search.length; i++) {
            if($str !== undefined){
                $str = $str.replace($format_search[i], $format_replace[i]);
            }
        }

        $('.NewsSingle__content').replaceWith('<div class="NewsSingle__content">'+$str+'</div>');

    }

    function isYouTubeLink(url) {
        let regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        let match = url.match(regExp);

        if (match && match[2].length == 11) {
            return true;
        } else {
            return false;
        }
    }
    function isVimeoLink(url) {
        let VIMEO_BASE_URL = "https://vimeo.com/api/oembed.json?url=";
        $.ajax({
            url: VIMEO_BASE_URL + url,
            type: 'GET',
            success: function(data) {
                return true;
                // not a valid Vimeo url
            },
            error: function(data) {
                return false;
            }
        });
    }

    function getYTVideoId(url) {
        let regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        let match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        }
    }

    function embedVideo(str, p1, offset, s){
        let isYouTube = isYouTubeLink(p1);
        let isVimeo = isVimeoLink(p1);
        if(isYouTube){
            return '<iframe width="560" height="315" src="//www.youtube.com/embed/' + getYTVideoId(p1) + '" frameborder="0" allowfullscreen></iframe>';
        }else if(isVimeo){
            console.log('is vimeeeooo');
            return '<iframe src="https://player.vimeo.com/video/377534362" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>';

        } else{
            return '<iframe width="560" height="315" src="'+p1+'" frameborder="0" allowfullscreen></iframe>';
        }
    }
});