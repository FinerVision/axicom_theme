import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import classNames from "classnames";
import $ from "jquery";

import NewsCard from "./NewsCard";
import breakpoints from "../Utils/breakpoints";

class NewsGrid extends Component {
    constructor(props) {
        super(props);

        this.hardScroll = false;

        this.state = {
            lastUpdatedScreenSize: 0,
            gridColumnCount: 1,
            itemsHeightPx: 0, // NOTE : doesn't include the active item's height
            activeIndex: null,
            activeItemHeightPx: 0,
            filteringCategory: null,
            rootWidth: 0,
        };

        // binding
        this.onScreenResize = this.onScreenResize.bind(this);
        this.trackSitesRenderingChanges = this.trackSitesRenderingChanges.bind(this);
        this.onFilteringEventTriggered = this.onFilteringEventTriggered.bind(this);

    }

    componentDidMount() {
        this.onScreenResize(); // trigger to get the initial states
        window.setTimeout(this.onScreenResize, 5000);
        window.addEventListener('load', this.onScreenResize);

        // request animation frame
        this.trackSitesRenderingChanges();

        // listen for the filtering event to update the posts
        if (this.props.filteringEvent) {
            this.filteringEvent = this.props.filteringEvent;
            window.addEventListener(this.filteringEvent, this.onFilteringEventTriggered);
        }

        // if the page needs to scroll to the news (when this is in an news article page)
        this.checkDefaultNewsName();
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.getItemsHeight().maxHeight !== this.state.itemsHeightPx) {
            this.onScreenResize();
        }

        const {activeIndex} = this.state;

        if (this.props.autoScroll && activeIndex !== prevState.activeIndex) {
            this.scrollToIndex(activeIndex !== null ? activeIndex : prevState.activeIndex);
        }
    }

    componentWillUnmount() {
        if (this.filteringEvent) {
            window.removeEventListener(this.filteringEvent, this.onFilteringEventTriggered);
        }
    }

    onFilteringEventTriggered(event) {
        this.setState({
            filteringCategory: event.value ? event.value : null,
        });
    }

    getFilteredItems() {
        const {filteringCategory} = this.state;

        if (!filteringCategory) {
            return this.props.items;
        }

        return this.props.items.filter(item => {
            return item.category === filteringCategory
        });
    }

    onScreenResize() {

        const {itemsHeightPx, activeItemHeightPx} = this.getItemsHeight();

        if (window.innerWidth !== this.state.lastUpdatedScreenSize || this.state.activeItemHeightPx !== activeItemHeightPx) {
            this.setState({
                lastUpdatedScreenSize: window.innerWidth,
                gridColumnCount: this.getGridBreakpointCount(),
                itemsHeightPx,
                activeItemHeightPx,
            });
        }
    }

    checkDefaultNewsName() {

        // if not checking the currentNewsSlug
        if (!this.props.isUriUpdating) { return; }

        // check the active id related to the such slug
        this.activateNewsBySlug(this.props.currentNewsSlug);
    }

    updateUrlSlug(name) {
        window.location.href = this.props.newsPageUri +'/'+ name;
    }

    removeUrlSlug() {
        if (this.props.isUriUpdating && typeof window.history != 'undefined' && typeof window.history.replaceState == 'function') {
            window.history.replaceState( {} , 'news', `${this.props.newsPageUri}` );
        }
    }

    activateNewsBySlug(slug) {
        const {items} = this.props;
        for(let i = 0; i < items.length; i++) {
            const {name} = items[i];
            if (name === slug) {
                this.hardScroll = true;
                this.setState({activeIndex: i});
                break;
            }
        }
    }

    scrollToIndex(index) {

        if (index === null) {
            return;
        }

        const {animationDuration} = this.props;

        window.setTimeout(() => {
            const {scrollableElement, navElement} = this.props;
            const $scrollableElement = $(scrollableElement);
            const navTop = $(navElement).offset().top;
            const navScrollHeight = $(navElement).height();
            const currentScrollTop = $scrollableElement.scrollTop();
            const activeElement = this.refs[`item_${index}`];
            const activeElementTop = activeElement.getBoundingClientRect().top;

            const newScrollTop = currentScrollTop + activeElementTop - (navScrollHeight + navTop);

            if (this.hardScroll) {
                $(scrollableElement).scrollTop(newScrollTop);
                this.hardScroll = false;
            } else {
                $(scrollableElement).animate({scrollTop: newScrollTop}, animationDuration);
            }
        }, (this.hardScroll)?2000:animationDuration);
    }

    /**
     * this function basically is keep checking the items size, and will trigger the state if anything is changed
     *
     */
    trackSitesRenderingChanges() {

        // keep updating
        window.requestAnimationFrame(this.trackSitesRenderingChanges);

        const {activeIndex} = this.state;
        const {root} = this.refs;
        const newState = {};

        const itemsHeightPx = (() => {

            let maxHeight = 0;

            const items = this.getFilteredItems();

            for(let i = 0; i < items.length; i++) {
                const newsCard = this.refs[`newsCard_${i}`];
                if (i !== activeIndex && newsCard.height > maxHeight) {
                    maxHeight = newsCard.height;
                }
            }
            return maxHeight;
        })();

        // check if items height px is updated
        if (itemsHeightPx !== this.state.itemsHeightPx) {
            newState.itemsHeightPx = itemsHeightPx;
        }

        // check if root width is updated
        if (root.getBoundingClientRect().width !== this.state.rootWidth) {
            newState.rootWidth = root.getBoundingClientRect().width;
        }

        if (Object.keys(newState).length !== 0) {
            this.setState(newState);
        }
    }

    getGridBreakpointCount() {
        const breakpoints = Object.keys(NewsGrid.GRID_BREAKPOINTS_COUNT);

        // need to do in backward order
        for(let i = breakpoints.length - 1; i >= 0; i--) {
            const breakpoint = breakpoints[i];
            const gridCount = NewsGrid.GRID_BREAKPOINTS_COUNT[breakpoint];
            if (window.innerWidth >= breakpoint) {
                return gridCount;
            }
        }
        return 1;
    }

    getGridWidthPercentage() {
        return 100 / this.state.gridColumnCount;
    }

    getRootHeight() {
        const gridBreakpointCount = this.getGridBreakpointCount();
        const {itemsHeightPx, gridColumnCount} = this.state;
        const items = this.getFilteredItems();
        const itemsCount = items.length;

        if (gridBreakpointCount !== 1) {

            if (this.state.activeIndex !== null) {
                return itemsHeightPx * Math.ceil((itemsCount - 1) / gridColumnCount) + this.state.activeItemHeightPx;
            }
            return itemsHeightPx * Math.ceil((itemsCount) / gridColumnCount);
        }

        return itemsHeightPx * itemsCount + (this.state.activeIndex !== null ? this.state.activeItemHeightPx : 0);

    }

    getItemsHeight() {

        let itemsHeightPx = 0, activeItemHeightPx = 0;
        const items = this.getFilteredItems();

        for(let i = 0; i < items.length; i++) {

            const $newsCard = $(`.NewsGrid__item--${i}`);
            const newsCard = $newsCard[0];
            if (!newsCard) { continue; }

            const isNewsCardExpended = $newsCard.find('.NewsCard__expend--active').length !== 0;
            const newsCardHeight = isNewsCardExpended ? $newsCard.find('.NewsCard__expend--active').height() : $newsCard.height();

            try {
                if (isNewsCardExpended) {
                    activeItemHeightPx = newsCardHeight;
                } else if (newsCardHeight > itemsHeightPx) {
                    itemsHeightPx = newsCardHeight;
                }
            } catch(e) {
                // alert(e);
            }

        }

        return {
            itemsHeightPx,
            activeItemHeightPx,
        };

    }

    getGridOffsets(index) {


        const gridWidthPercentage = this.getGridWidthPercentage();
        const {gridColumnCount, itemsHeightPx, activeIndex, activeItemHeightPx} = this.state;
        const innerRoot = this.refs.inner;

        if (!innerRoot) {
            // return nothing before rendered
            return '';
        }

        const innerWidth = innerRoot.getBoundingClientRect().width;

        // TODO : simplify this 'package'
        const {top, left} = (() => {

            // when a news is active, every news after that will need to fill the gap before it caused by the active news, so minus 1
            const fakeIndex = index - 1;

            switch(true) {

                // when no active index, or;
                // the active index is after this element
                case activeIndex === null:
                case activeIndex > index:
                    return {
                        top: Math.floor(index / gridColumnCount) * itemsHeightPx,
                        left: gridWidthPercentage * (index % gridColumnCount) * innerWidth / 100,
                    };

                // this is the active element
                case activeIndex === index:
                    return {
                        top: Math.ceil(index / gridColumnCount) * itemsHeightPx,
                        left: 0,
                    };

                // this element is just right after the active element originally
                case index > Math.ceil(activeIndex / gridColumnCount) * gridColumnCount:
                    return {
                        top: (Math.floor(fakeIndex / gridColumnCount) * itemsHeightPx) + activeItemHeightPx,
                        left: gridWidthPercentage * (fakeIndex % gridColumnCount) * innerWidth / 100,
                    };

                default:
                    return {
                        top: (Math.floor(fakeIndex / gridColumnCount) * itemsHeightPx),
                        left: gridWidthPercentage * (fakeIndex % gridColumnCount) * innerWidth / 100,
                    };
            }

        })();

        return `translate(${left}px, ${top}px)`;

    }

    getNewsItems() {
        const {animationDuration} = this.props;
        const {activeIndex} = this.state;
        const gridWidthPercentage = this.getGridWidthPercentage();
        const gridBreakpointCount = this.getGridBreakpointCount();
        const items = this.getFilteredItems();

        return items.map((item, index) => {
            const transform = this.getGridOffsets(index);
            const transition = `transform ${animationDuration / 1000}s`;
            const style = {
                WebkitTransform: transform,
                transform,
                WebkitTransition: transition,
                transition,
                width: `${gridWidthPercentage}%`,
            };

            return (
                <div
                    key={index}
                    className={classNames(
                        'NewsGrid__item',
                        `NewsGrid__item--${index}`,
                        {
                            'NewsGrid__item--active': index === activeIndex,
                        }
                    )}
                    ref={`item_${index}`}
                    style={style}
                >
                    <NewsCard
                        ref={`newsCard_${index}`}
                        {...item}
                        tempIndex={index}
                        disableHoverAnimation={this.props.disableHoverAnimation}
                        isExpending={activeIndex === index}
                        expendPosition={gridBreakpointCount !== 1 ? NewsCard.EXPEND_RIGHT : NewsCard.EXPEND_BOTTOM}
                        expendWidth={Math.max(gridBreakpointCount - 1, 1)} // make sure the value is not zero
                        onActivate={() => {
                            //this.setState({activeIndex: index !== activeIndex ? index : null});
                            this.updateUrlSlug(item.name);
                        }}
                        onDeactivate={() => {
                            this.setState({activeIndex: null});
                            this.removeUrlSlug();
                        }}
                        onRelatedNewsActivate={item => {
                            this.activateNewsBySlug(item.name);
                        }}
                    />
                </div>
            );
        });
    }

    render() {
        const {animationDuration} = this.props;
        const transition = `height ${animationDuration / 1000}s`;
        return (
            <div className="NewsGrid" ref="root">
                <div
                    className="NewsGrid__inner"
                    ref="inner"
                    style={{
                        height: this.getRootHeight(),
                        WebkitTransition: transition,
                        transition,
                    }}
                >
                    {this.getNewsItems()}
                </div>
            </div>
        );
    }
}

NewsGrid.GRID_BREAKPOINTS_COUNT = {
    [breakpoints.md]: 3,
    [breakpoints.sm]: 3,
    [breakpoints.xs]: 2,
};

NewsGrid.propTypes = {
    items: PropTypes.arrayOf(PropTypes.shape({
        image: PropTypes.object,
        category: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired,
        relatedNews: PropTypes.arrayOf(PropTypes.shape({
            image: PropTypes.object,
            title: PropTypes.string.isRequired,
            category: PropTypes.string.isRequired,
            date: PropTypes.string,
        })),
    })),
    autoScroll: PropTypes.bool,
    scrollableElement: PropTypes.string,
    navElement: PropTypes.string,
    animationDuration: PropTypes.number,
    filteringEvent: PropTypes.string,
    isUriUpdating: PropTypes.bool,
    currentNewsSlug: PropTypes.string,
    newsPageUri: PropTypes.string,
};

NewsGrid.defaultProps = {
    items: [],
    autoScroll: false,
    scrollableElement: '#container',
    navElement: '.Nav',
    animationDuration: 400,
    filteringEvent: null,
    isUriUpdating: false,
    currentNewsSlug: '',
    newsPageUri: '',
};

export default NewsGrid;

$(document).ready(() => {
    const nodes = document.querySelectorAll('[data-component=NewsGrid]');
    for(let i = 0; i < nodes.length; i++) {
        const node = nodes[i];
        const params = JSON.parse(node.getAttribute('data-params') || {});
        render(<NewsGrid {...params} />, node);
    }
});
