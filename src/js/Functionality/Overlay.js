class Overlay {
    constructor(overlay) {
        this.overlay = overlay;
        this.name = this.overlay.getAttribute('data-name');
        this.events();
    }

    events() {
        const close = this.overlay.querySelector('.Overlay__close');
        close.addEventListener('click', () => this.close());

        // Listen for click events on data-overlay elements,
        // then open this overlay.
        const opens = document.querySelectorAll(`[data-overlay="${this.name}"]`);
        for (let i = 0; i < opens.length; i++) {
            opens[i].addEventListener('click', () => this.open());
        }
    }

    open() {
        this.overlay.style.display = 'block';
        document.querySelector('.Nav').style.display = 'none';
    }

    close() {
        this.overlay.style.display = 'none';
        document.querySelector('.Nav').style.display = 'block';
    }
}

const overlays = document.querySelectorAll('.Overlay');

for (let i = 0; i < overlays.length; i++) {
    new Overlay(overlays[i]);
}
