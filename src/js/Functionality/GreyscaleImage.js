const images = document.querySelectorAll('[data-img-greyscale]');

const DEFAULT_BRIGHTNESS = 67;

for (let i = 0; i < images.length; i++) {
    const image = images[i];
    const img = new Image();
    const brightness = parseInt(image.getAttribute('data-brightness')) || DEFAULT_BRIGHTNESS;

    img.src = image.getAttribute('data-img-greyscale');

    img.onload = () => {
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');

        canvas.width = img.width;
        canvas.height = img.height;
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

        const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        const data = imageData.data;

        for (let j = 0; j < data.length; j += 4) {

            if (!(
                    data[j] > 200 && data[j] <= 255 &&
                    data[j + 1] > 200 && data[j + 1] <= 255 &&
                    data[j + 2] > 200 && data[j + 2] <= 255
            )) {
                data[j] = brightness;
                data[j + 1] = brightness;
                data[j + 2] = brightness;
            } else {
                data[j + 3] = 0;
            }

        }

        ctx.putImageData(imageData, 0, 0);

        image.setAttribute('src', canvas.toDataURL());
    };
}
