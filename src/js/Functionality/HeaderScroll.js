
const swing = p => 0.5 - Math.cos( p * Math.PI ) / 2;

const DEFAULT_SCROLL_DURATION = 400;
const DEFAULT_SCROLL_UPDATE_INTERVAL = 20;
const DEFAULT_SCROLL_UPDATE_TIMES = DEFAULT_SCROLL_DURATION / DEFAULT_SCROLL_UPDATE_INTERVAL;
const DEFAULT_SCROLL_PER_UPDATE = 1 / DEFAULT_SCROLL_UPDATE_TIMES;

const easingFunction = swing;

const scrollTo = (element, scrollTop) => {
    const {scrollTop: originalScrollTop} = element;
    const difference = scrollTop - originalScrollTop;
    let currentStep = 1;
    const scrollInterval = window.setInterval(() => {
        const scrollRatio = easingFunction(DEFAULT_SCROLL_PER_UPDATE * currentStep);

        element.scrollTop = originalScrollTop + scrollRatio * difference;

        // increase step
        currentStep++;

        // check if reached target
        if (currentStep > DEFAULT_SCROLL_UPDATE_TIMES) {
            window.clearInterval(scrollInterval);
        }

    }, DEFAULT_SCROLL_UPDATE_INTERVAL);
};



(() => {
    const container = document.querySelector('#container');
    const header = document.querySelector('.Header');
    const nav = document.querySelector('.Nav');
    const arrow = document.querySelector('.Hero__scroll-arrow') || document.querySelector('.HeroParallax__scroll-arrow');

    if (!arrow) {
        return;
    }

    arrow.addEventListener('click', () => {
        const scrollDestination = header.clientHeight - nav.clientHeight;
        scrollTo(container, scrollDestination);
    });
})();
