// TODO : separate this into an individual script file, include this script file only when using IEs
import "./react-16-polyfill.js";
window.$ = require("jquery");
// Plugin: jQuery.scrollSpeed
// Source: github.com/nathco/jQuery.scrollSpeed
// Author: Nathan Rutzky
// Update: 1.0.2


// add the browser class to the html tag
import "./Utils/Browser";
import "./Utils/helpers";

import "./Components/Carousel";
import "./Components/Expertise";
import "./Components/CustomSelect";
import "./Components/Offices";
import "./Components/Nav";
import "./Components/ImageSlider";
import "./Components/NewsCardSlider";
import "./Components/ContactForm";
import "./Components/JobList";
import "./Components/HeroParallax";
import "./Components/PeopleTeam";
import "./Components/ReadMoreExpertise";
import "./Components/ReadMoreServices";
import "./Components/NavSlider";
import "./Components/MobileExpertise";
import "./Components/MobileServices";
import "./Components/PaginatedList";
import "./Components/ExpertiseRotator";
import "./Components/ServicesPageSlider";
import "./Components/NewsGrid";
import "./Components/NewsCard";
import "./Components/FeaturedNews";
import "./Components/CookieBanner";
import "./Components/LinkedinButton";
import "./Components/CaseStudyCarousel";
import "./Components/CaseStudyTargets";
import "./Components/ServicePopUp";
import "./Components/StickyElements";
import "./Components/VideoEmbed";
import "./Components/SameHeights";
import "./Components/HeaderCarousel";

import "./Functionality/HeaderScroll";
import "./Functionality/Overlay";
import "./Functionality/GreyscaleImage";

// NOTE: Removed due to re-design
// import "./Components/Person";
