import breakpoints from "../Utils/breakpoints";

const breakpointImage = imageSet => {

    if (typeof imageSet === 'string') {
        return imageSet;
    }

    // check if this is an object
    if (imageSet instanceof Object) {
        const keys = Object.keys(breakpoints);
        for(let i = keys.length - 1; i >= 0; i--) {
            const key = keys[i];
            const value = breakpoints[key];
            if (window.innerWidth >= value && typeof imageSet[key] !== 'undefined') {
                return imageSet[key];
            }
        }

        return imageSet.default || null;
    }

    return null;

};

export default breakpointImage;
