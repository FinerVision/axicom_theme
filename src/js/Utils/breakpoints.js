/**
 * Note : please order the breakpoints in ascending order
 */
export default {
    xs: 480,
    sm: 768,
    md: 992,
    lg: 1200,
};
