import {detect} from "detect-browser";

try {

    // detect
    let browser = detect();

    // if browser equals null, still try to check
    if (browser === null) {
        if (/AppleWebKit\/([0-9\.]+).*Mobile/.test(window.navigator.userAgent)) {
            browser = {
                os: 'iOS',
                name: 'ios-webview',
            };
        }
    }

    // allow access
    window.browser = browser;

    const htmlTag = document.documentElement;
    const {name, version = '0.0.0'} = browser;

    // get the major version code
    const versionMajor = version.split('.')[0];

    let className = htmlTag.getAttribute('class') || '';
    className += ` browser-${name}`;
    className += ` version-${versionMajor}`;

    htmlTag.setAttribute('class', className.trim());
    htmlTag.setAttribute('data-browser-version', version);

} catch (e) {
    // just simply ignore it
    console.error(e);
}
