<?php

define('AXICOM_VERSION', '2.1.4');

include_once __DIR__ . '/bootstrap.php';


include_once __DIR__ . '/views/partials/header.php';
include_once __DIR__ . '/views/partials/content.php';
include_once __DIR__ . '/views/partials/footer.php';
