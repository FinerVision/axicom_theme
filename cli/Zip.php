<?php

use phpseclib\Net\SSH2;
use phpseclib\Crypt\RSA;

class Zip
{
    public static function get($url, $path = 'file.zip')
    {
        $serverIp = getenv('SERVER_IP');
        $serverUsername = getenv('SERVER_USERNAME');
        $serverPrivate = getenv('SERVER_PRIVATE');

        $ssh = new SSH2($serverIp);
        $key = new RSA();
        $key->loadKey(file_get_contents($serverPrivate));

        if (!$ssh->login($serverUsername, $key)) {
            echo "SSH login failed\n";
            exit;
        }

        $ssh->exec('/usr/bin/zip -r /var/www/apps/public/axicom_ms.zip /var/www/apps/public/axicom_ms/');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        curl_close($ch);

        $file = fopen($path, 'w+');
        fputs($file, $data);
        fclose($file);
    }

    public static function open($name, $directory = '.')
    {
        $directory = rtrim($directory, '/');
        $zip = new ZipArchive;
        $res = $zip->open("{$directory}/{$name}");

        if (!$res) {
            return false;
        }

        $zip->extractTo($directory);
        $zip->close();

        unlink("{$directory}/{$name}");

        return true;
    }
}
