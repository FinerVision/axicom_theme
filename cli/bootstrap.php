<?php

ini_set('memory_limit', '-1');

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/Zip.php';

$file = isset($argv[2]) ? ".env.{$argv[2]}" : '.env';
$dotenv = new Dotenv\Dotenv(__DIR__ . '/../', $file);
$dotenv->load();
