# Install single-site, single-user for development

### Installation
1. Create a UTF-8 Unicode utf8mb4 database called axicom_ms
3. Login to WordPress from your browser
    - username finervision
    - password axicom2017!
    - Tick the 'allow not secure password' checkbox, if shown
    - Tick the allow search indexes checkbox

### Install Axicom Theme
1. Login to WordPress from your browser
2. From the menu go to appearance -> themes and activate the axicom theme

### Install Plugins
1. Login to WordPress from your browser
2. Go to the plugins menu and activate the advanced custom fields plugin
3. From the menu go to "Custom Fields" -> Updates and add the following to the "License Key" field:
b3JkZXJfaWQ9ODgxNzl8dHlwZT1wZXJzb25hbHxkYXRlPTIwMTYtMDgtMjIgMjA6MDM6NDM=

### Import Data
1. From the menu go to "Custom Fields" -> Tools, then select the JSON file from wp-content/themes/axicom/data/exports, finally click import
2. From the menu go to Tools -> import, then click "Install Now" on the WordPress item, then select the XML file from wp-content/themes/axicom/data/exports, then select the finervision user, finally click import

### Fix links
1. From the menu go to Settings -> Permalinks, then select the "Post name" radio button

### Cloning Scripts

To download the WordPress and 

Install dependencies.

```bash
composer install
npm install
```

Copy the env variables and update them to the Samantha database details.

```bash
cp .env.example .env
```

Compile the assets
```bash
npm run build
```

Run the clone command. The first argument is the directory you want
axicom_ms.zip and axicom_ms.sql to be downloaded and extracted to.

```bash
./bin/clone.php ~/code
```

### Transferring to WPP Team

- Copy axicom_ms directory to a new directory called axicom
- Update wp-config.php defines: 'DB_*', 'DOMAIN_CURRENT_SITE', 'PATH_CURRENT_SITE'
- Zip axicom directory
- Update database with exported axicom_ms.sql file and then update with axicom_ms_update.sql script
