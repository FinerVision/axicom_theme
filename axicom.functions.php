<?php

/**
 * Return an absolute HTTP url to given the uri.
 *
 * @param $uri
 * @return string
 */
function asset($uri)
{
    $uri = ltrim($uri, '/');
    $base = get_stylesheet_directory_uri() . '/public';
    return "{$base}/{$uri}";
}

function assetVersion($url)
{
    return asset($url) . '?v=' . AXICOM_VERSION;
}

/**
 * Return an absolute HTTP url to given the uri.
 *
 * @param $uri
 * @return string
 */
function url($uri)
{
    $uri = ltrim($uri, '/');
    $base = get_bloginfo('url');
    return "{$base}/{$uri}";
}

/**
 * Slug Generator (From Laravel)
 *
 * @param $str
 * @return string
 */
function create_slug($str)
{
    return mb_strtolower(preg_replace("/\W+/", '_', $str));
}

/**
 * limit string (from Laravel)
 *
 * @param $value
 * @param int $limit
 * @param string $end
 * @return string
 */
function str_limit($value, $limit = 100, $end = '...')
{
    if (mb_strwidth($value, 'UTF-8') <= $limit) {
        return $value;
    }

    return rtrim(mb_strimwidth($value, 0, $limit, '', 'UTF-8')) . $end;
}

/**
 * Jobs list from Workable.com
 *
 * @return array
 */
function getJobsAndDepartment($categorySearchTitle, $locationSearcTitle)
{
    $ch = curl_init('https://axicom.workable.com/spi/v3/jobs/?include_fields[]=description&include_fields[]=full_description&state[]=published');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //curl_setopt($ch, CURLOPT_USERAGENT, 'YourScript/0.1 (contact@email)');
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Authorization: Bearer ' . '58516ea32e5599631215b8fc7317db6513a51ac77af244bde6a4d7d080fda4de'
    ));

    $data = curl_exec($ch);
    $info = curl_getinfo($ch);
    curl_close($ch);

    $jobs = [];
    $departments = [];
    $locations = [];

    if ($info['http_code'] == 200) {
        $result = json_decode($data);

        foreach ($result->jobs as $job) {
            $location = isset($job->location) ? $job->location->country . ', ' . $job->location->city : '';

            $jobs[] = [
                'title' => $job->title . ($job->state == 'archived' ? ' <small>(archived)</small>' : ''),
                'shortcode' => $job->shortcode,
                'subtitle' => $job->department . ' - ' . $location,
                'department' => $job->department,
                'category_slug' => create_slug($job->department),
                'location_slug' => create_slug($location),
                'brief' => $job->description ? str_limit($job->description, 255) : '',
                'description' =>
                    ($job->full_description && strlen($job->full_description) > 0)
                        ? $job->full_description : $job->description ? $job->full_description : '',
//                'short_link' => $job->shortlink ? $job->shortlink : '',
                'short_link' => $job->application_url ? $job->application_url : '',
            ];

            $departments[] = $job->department;
            $locations[] = $location;
        }
    }

    sort($departments);
    sort($locations);

    $departments = array_unique($departments);
    $src_locations = array_unique($locations);
    $locations = [];

    $categories = [];
    foreach ($departments as $department) {
        $categories[create_slug($department)] = $department;
    }
    $categories[''] = $categorySearchTitle;

    foreach ($src_locations as $src_location) {
        $locations[create_slug($src_location)] = $src_location;
    }
    $locations[''] = $locationSearcTitle;

    return [
        'jobs' => $jobs,
        'categories' => $categories,
        'locations' => $locations
    ];
}

/**
 * Sort an array of objects alphabetically.
 *
 * @param array $objects
 * @param string $prop
 * @param string $order
 * @return array
 */
function sortAlphabetically($objects, $prop, $order = 'asc')
{
    uasort($objects, function ($a, $b) use ($prop, $order) {
        if ($order === 'asc') {
            return $a->{$prop} > $b->{$prop};
        }
        return $a->{$prop} < $b->{$prop};
    });
    return array_values($objects);
}
function add_social_sharing()
{
    return'
	<ul class="SocialIcons__list">
  <li class="SocialIcons__element"><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Flocalhost%2Ftestsite&t=Test%20site" title="Share on Facebook" target="_blank" onclick="window.open(\'https://www.facebook.com/sharer/sharer.php?u=\' + encodeURIComponent(document.URL) + \'&t=\' + encodeURIComponent(document.URL)); return false;"><img src="'. get_bloginfo( 'template_directory' ).'/public/img/facebook.png"></a></li>
  <li class="SocialIcons__element"><a href="https://twitter.com/intent/tweet?source=http%3A%2F%2Flocalhost%2Ftestsite&text=Test%20site:%20http%3A%2F%2Flocalhost%2Ftestsite" target="_blank" title="Tweet" onclick="window.open(\'https://twitter.com/intent/tweet?text=\' + encodeURIComponent(document.title) + \':%20\'  + encodeURIComponent(document.URL)); return false;"><img src="'. get_bloginfo( 'template_directory' ).'/public/img/twitter.png"></a></li>
  <li class="SocialIcons__element"><a href="http://www.linkedin.com/shareArticle?mini=true&url=http%3A%2F%2Flocalhost%2Ftestsite&title=Test%20site&summary=Test%20site&source=http%3A%2F%2Flocalhost%2Ftestsite" target="_blank" title="Share on LinkedIn" onclick="window.open(\'http://www.linkedin.com/shareArticle?mini=true&url=\' + encodeURIComponent(document.URL) + \'&title=\' +  encodeURIComponent(document.title)); return false;"><img src="'. get_bloginfo( 'template_directory' ).'/public/img/linkedin.png"></a></li>
</ul>
';
}
add_action( 'custom_social_share', 'add_social_sharing' );
/**
 * Get posts from all site !!! take care to use it. Maybe the get_field will not work correctly!
 * Alternativelly
 *
 * @param $args
 * @param $blogIds - to limit the the blog sources
 * @param null $orderBy
 * @param string $order
 *
 * @return array - of posts or empty array
 */
function getPostFromAllSite($args, $blogIds = NULL, $orderBy = NULL, $order = 'ASC')
{
    $originalBlogId = get_current_blog_id();
    $sites = get_sites();

    $allPosts = [];
    foreach ($sites as $site) {
        if ($blogIds == NULL || in_array($site->blog_id, $blogIds)) {
            switch_to_blog($site->blog_id);

            $query = new WP_Query($args);
            $posts = $query->posts;
            foreach ($posts as $p) {
                $p->site_id = $site->blog_id;
                $p->customFields = get_fields($p->ID);
            }
            $allPosts = array_merge($allPosts, $posts);
        }
    }

    switch_to_blog($originalBlogId);
    wp_reset_query();

    if ($orderBy != NULL) {
        usort($allPosts, function ($a, $b) use ($orderBy, $order) {
            $retVal = 0;
            if ($a[$orderBy] < $b[$orderBy]) $retVal = 1;
            if ($a[$orderBy] > $b[$orderBy]) $retVal = -1;
            if ($order !== 'ASC') {
                $retVal *= -1;
            }
            return $retVal;
        });
    }

    return $allPosts;
}

/**
 * Safely JSON encode and echo out the result.
 *
 * @param $payload
 */
function json($payload)
{
    echo htmlentities(json_encode($payload));
}

/**
 * weather requests
 *
 * @param $weatherCity
 *
 * @param bool $metric
 * @return array|null
 */
// todo : if some country not use Metric system, then change it (source is Kelvin)
function getWeatherInfo($weatherCity, $temperatureScale = 'kelvin')
{
    $appId = getConfig('weather');
    $request = getCachedRequest("https://api.openweathermap.org/data/2.5/weather?q={$weatherCity}&appid={$appId}&mode=jso‌​n", 60);
    if (count($request) > 2) {

        $temp = $request['main']['temp'];
        if ($temperatureScale == 'kelvin') {
            $temp = $request['main']['temp'] != '' ? round($request['main']['temp'] - 273.15) . ' °C' : '-';
        }
        if ($temperatureScale == 'fahrenheit') {
            $temp = $request['main']['temp'] != '' ? round(9/5 * ($request['main']['temp'] - 273.15) + 32) . ' °F' : '-';
        }
        return [
            'temp' => $temp,
            'description' => count($request['weather']) > 0 ? $request['weather'][0]['description'] : '',
            'icon' => count($request['weather']) > 0 ? $request['weather'][0]['icon'] : ''
        ];
    }
    return null;
}

/**
 * Returns the replacements for the ascii method.
 *
 * Note: Adapted from Stringy\Stringy.
 *
 * @see https://github.com/danielstjules/Stringy/blob/2.3.1/LICENSE.txt
 *
 * @return array
 */
function charsArray()
{
    static $charsArray;
    if (isset($charsArray)) {
        return $charsArray;
    }
    return $charsArray = [
        '0' => ['°', '₀', '۰'],
        '1' => ['¹', '₁', '۱'],
        '2' => ['²', '₂', '۲'],
        '3' => ['³', '₃', '۳'],
        '4' => ['⁴', '₄', '۴', '٤'],
        '5' => ['⁵', '₅', '۵', '٥'],
        '6' => ['⁶', '₆', '۶', '٦'],
        '7' => ['⁷', '₇', '۷'],
        '8' => ['⁸', '₈', '۸'],
        '9' => ['⁹', '₉', '۹'],
        'a' => ['à', 'á', 'ả', 'ã', 'ạ', 'ă', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ', 'â', 'ấ', 'ầ', 'ẩ', 'ẫ', 'ậ', 'ā', 'ą', 'å', 'α', 'ά', 'ἀ', 'ἁ', 'ἂ', 'ἃ', 'ἄ', 'ἅ', 'ἆ', 'ἇ', 'ᾀ', 'ᾁ', 'ᾂ', 'ᾃ', 'ᾄ', 'ᾅ', 'ᾆ', 'ᾇ', 'ὰ', 'ά', 'ᾰ', 'ᾱ', 'ᾲ', 'ᾳ', 'ᾴ', 'ᾶ', 'ᾷ', 'а', 'أ', 'အ', 'ာ', 'ါ', 'ǻ', 'ǎ', 'ª', 'ა', 'अ', 'ا'],
        'b' => ['б', 'β', 'Ъ', 'Ь', 'ب', 'ဗ', 'ბ'],
        'c' => ['ç', 'ć', 'č', 'ĉ', 'ċ'],
        'd' => ['ď', 'ð', 'đ', 'ƌ', 'ȡ', 'ɖ', 'ɗ', 'ᵭ', 'ᶁ', 'ᶑ', 'д', 'δ', 'د', 'ض', 'ဍ', 'ဒ', 'დ'],
        'e' => ['é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê', 'ế', 'ề', 'ể', 'ễ', 'ệ', 'ë', 'ē', 'ę', 'ě', 'ĕ', 'ė', 'ε', 'έ', 'ἐ', 'ἑ', 'ἒ', 'ἓ', 'ἔ', 'ἕ', 'ὲ', 'έ', 'е', 'ё', 'э', 'є', 'ə', 'ဧ', 'ေ', 'ဲ', 'ე', 'ए', 'إ', 'ئ'],
        'f' => ['ф', 'φ', 'ف', 'ƒ', 'ფ'],
        'g' => ['ĝ', 'ğ', 'ġ', 'ģ', 'г', 'ґ', 'γ', 'ဂ', 'გ', 'گ'],
        'h' => ['ĥ', 'ħ', 'η', 'ή', 'ح', 'ه', 'ဟ', 'ှ', 'ჰ'],
        'i' => ['í', 'ì', 'ỉ', 'ĩ', 'ị', 'î', 'ï', 'ī', 'ĭ', 'į', 'ı', 'ι', 'ί', 'ϊ', 'ΐ', 'ἰ', 'ἱ', 'ἲ', 'ἳ', 'ἴ', 'ἵ', 'ἶ', 'ἷ', 'ὶ', 'ί', 'ῐ', 'ῑ', 'ῒ', 'ΐ', 'ῖ', 'ῗ', 'і', 'ї', 'и', 'ဣ', 'ိ', 'ီ', 'ည်', 'ǐ', 'ი', 'इ'],
        'j' => ['ĵ', 'ј', 'Ј', 'ჯ', 'ج'],
        'k' => ['ķ', 'ĸ', 'к', 'κ', 'Ķ', 'ق', 'ك', 'က', 'კ', 'ქ', 'ک'],
        'l' => ['ł', 'ľ', 'ĺ', 'ļ', 'ŀ', 'л', 'λ', 'ل', 'လ', 'ლ'],
        'm' => ['м', 'μ', 'م', 'မ', 'მ'],
        'n' => ['ñ', 'ń', 'ň', 'ņ', 'ŉ', 'ŋ', 'ν', 'н', 'ن', 'န', 'ნ'],
        'o' => ['ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'ơ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ', 'ø', 'ō', 'ő', 'ŏ', 'ο', 'ὀ', 'ὁ', 'ὂ', 'ὃ', 'ὄ', 'ὅ', 'ὸ', 'ό', 'о', 'و', 'θ', 'ို', 'ǒ', 'ǿ', 'º', 'ო', 'ओ'],
        'p' => ['п', 'π', 'ပ', 'პ', 'پ'],
        'q' => ['ყ'],
        'r' => ['ŕ', 'ř', 'ŗ', 'р', 'ρ', 'ر', 'რ'],
        's' => ['ś', 'š', 'ş', 'с', 'σ', 'ș', 'ς', 'س', 'ص', 'စ', 'ſ', 'ს'],
        't' => ['ť', 'ţ', 'т', 'τ', 'ț', 'ت', 'ط', 'ဋ', 'တ', 'ŧ', 'თ', 'ტ'],
        'u' => ['ú', 'ù', 'ủ', 'ũ', 'ụ', 'ư', 'ứ', 'ừ', 'ử', 'ữ', 'ự', 'û', 'ū', 'ů', 'ű', 'ŭ', 'ų', 'µ', 'у', 'ဉ', 'ု', 'ူ', 'ǔ', 'ǖ', 'ǘ', 'ǚ', 'ǜ', 'უ', 'उ'],
        'v' => ['в', 'ვ', 'ϐ'],
        'w' => ['ŵ', 'ω', 'ώ', 'ဝ', 'ွ'],
        'x' => ['χ', 'ξ'],
        'y' => ['ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ', 'ÿ', 'ŷ', 'й', 'ы', 'υ', 'ϋ', 'ύ', 'ΰ', 'ي', 'ယ'],
        'z' => ['ź', 'ž', 'ż', 'з', 'ζ', 'ز', 'ဇ', 'ზ'],
        'aa' => ['ع', 'आ', 'آ'],
        'ae' => ['ä', 'æ', 'ǽ'],
        'ai' => ['ऐ'],
        'at' => ['@'],
        'ch' => ['ч', 'ჩ', 'ჭ', 'چ'],
        'dj' => ['ђ', 'đ'],
        'dz' => ['џ', 'ძ'],
        'ei' => ['ऍ'],
        'gh' => ['غ', 'ღ'],
        'ii' => ['ई'],
        'ij' => ['ĳ'],
        'kh' => ['х', 'خ', 'ხ'],
        'lj' => ['љ'],
        'nj' => ['њ'],
        'oe' => ['ö', 'œ', 'ؤ'],
        'oi' => ['ऑ'],
        'oii' => ['ऒ'],
        'ps' => ['ψ'],
        'sh' => ['ш', 'შ', 'ش'],
        'shch' => ['щ'],
        'ss' => ['ß'],
        'sx' => ['ŝ'],
        'th' => ['þ', 'ϑ', 'ث', 'ذ', 'ظ'],
        'ts' => ['ц', 'ც', 'წ'],
        'ue' => ['ü'],
        'uu' => ['ऊ'],
        'ya' => ['я'],
        'yu' => ['ю'],
        'zh' => ['ж', 'ჟ', 'ژ'],
        '(c)' => ['©'],
        'A' => ['Á', 'À', 'Ả', 'Ã', 'Ạ', 'Ă', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ', 'Â', 'Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ', 'Å', 'Ā', 'Ą', 'Α', 'Ά', 'Ἀ', 'Ἁ', 'Ἂ', 'Ἃ', 'Ἄ', 'Ἅ', 'Ἆ', 'Ἇ', 'ᾈ', 'ᾉ', 'ᾊ', 'ᾋ', 'ᾌ', 'ᾍ', 'ᾎ', 'ᾏ', 'Ᾰ', 'Ᾱ', 'Ὰ', 'Ά', 'ᾼ', 'А', 'Ǻ', 'Ǎ'],
        'B' => ['Б', 'Β', 'ब'],
        'C' => ['Ç', 'Ć', 'Č', 'Ĉ', 'Ċ'],
        'D' => ['Ď', 'Ð', 'Đ', 'Ɖ', 'Ɗ', 'Ƌ', 'ᴅ', 'ᴆ', 'Д', 'Δ'],
        'E' => ['É', 'È', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ', 'Ë', 'Ē', 'Ę', 'Ě', 'Ĕ', 'Ė', 'Ε', 'Έ', 'Ἐ', 'Ἑ', 'Ἒ', 'Ἓ', 'Ἔ', 'Ἕ', 'Έ', 'Ὲ', 'Е', 'Ё', 'Э', 'Є', 'Ə'],
        'F' => ['Ф', 'Φ'],
        'G' => ['Ğ', 'Ġ', 'Ģ', 'Г', 'Ґ', 'Γ'],
        'H' => ['Η', 'Ή', 'Ħ'],
        'I' => ['Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị', 'Î', 'Ï', 'Ī', 'Ĭ', 'Į', 'İ', 'Ι', 'Ί', 'Ϊ', 'Ἰ', 'Ἱ', 'Ἳ', 'Ἴ', 'Ἵ', 'Ἶ', 'Ἷ', 'Ῐ', 'Ῑ', 'Ὶ', 'Ί', 'И', 'І', 'Ї', 'Ǐ', 'ϒ'],
        'K' => ['К', 'Κ'],
        'L' => ['Ĺ', 'Ł', 'Л', 'Λ', 'Ļ', 'Ľ', 'Ŀ', 'ल'],
        'M' => ['М', 'Μ'],
        'N' => ['Ń', 'Ñ', 'Ň', 'Ņ', 'Ŋ', 'Н', 'Ν'],
        'O' => ['Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ố', 'Ồ', 'Ổ', 'Ỗ', 'Ộ', 'Ơ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ', 'Ø', 'Ō', 'Ő', 'Ŏ', 'Ο', 'Ό', 'Ὀ', 'Ὁ', 'Ὂ', 'Ὃ', 'Ὄ', 'Ὅ', 'Ὸ', 'Ό', 'О', 'Θ', 'Ө', 'Ǒ', 'Ǿ'],
        'P' => ['П', 'Π'],
        'R' => ['Ř', 'Ŕ', 'Р', 'Ρ', 'Ŗ'],
        'S' => ['Ş', 'Ŝ', 'Ș', 'Š', 'Ś', 'С', 'Σ'],
        'T' => ['Ť', 'Ţ', 'Ŧ', 'Ț', 'Т', 'Τ'],
        'U' => ['Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ', 'Ư', 'Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự', 'Û', 'Ū', 'Ů', 'Ű', 'Ŭ', 'Ų', 'У', 'Ǔ', 'Ǖ', 'Ǘ', 'Ǚ', 'Ǜ'],
        'V' => ['В'],
        'W' => ['Ω', 'Ώ', 'Ŵ'],
        'X' => ['Χ', 'Ξ'],
        'Y' => ['Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ', 'Ÿ', 'Ῠ', 'Ῡ', 'Ὺ', 'Ύ', 'Ы', 'Й', 'Υ', 'Ϋ', 'Ŷ'],
        'Z' => ['Ź', 'Ž', 'Ż', 'З', 'Ζ'],
        'AE' => ['Ä', 'Æ', 'Ǽ'],
        'CH' => ['Ч'],
        'DJ' => ['Ђ'],
        'DZ' => ['Џ'],
        'GX' => ['Ĝ'],
        'HX' => ['Ĥ'],
        'IJ' => ['Ĳ'],
        'JX' => ['Ĵ'],
        'KH' => ['Х'],
        'LJ' => ['Љ'],
        'NJ' => ['Њ'],
        'OE' => ['Ö', 'Œ'],
        'PS' => ['Ψ'],
        'SH' => ['Ш'],
        'SHCH' => ['Щ'],
        'SS' => ['ẞ'],
        'TH' => ['Þ'],
        'TS' => ['Ц'],
        'UE' => ['Ü'],
        'YA' => ['Я'],
        'YU' => ['Ю'],
        'ZH' => ['Ж'],
        ' ' => ["\xC2\xA0", "\xE2\x80\x80", "\xE2\x80\x81", "\xE2\x80\x82", "\xE2\x80\x83", "\xE2\x80\x84", "\xE2\x80\x85", "\xE2\x80\x86", "\xE2\x80\x87", "\xE2\x80\x88", "\xE2\x80\x89", "\xE2\x80\x8A", "\xE2\x80\xAF", "\xE2\x81\x9F", "\xE3\x80\x80"],
    ];
}

/**
 * Transliterate a UTF-8 value to ASCII.
 *
 * @param  string $value
 * @return string
 */
function ascii($value)
{
    foreach (charsArray() as $key => $val) {
        $value = str_replace($val, $key, $value);
    }
    return preg_replace('/[^\x20-\x7E]/u', '', $value);
}

/**
 * Generate a URL friendly "slug" from a given string.
 *
 * @param  string $title
 * @param  string $separator
 * @return string
 */
function str_slug($title, $separator = '-')
{
    $title = ascii($title);
    // Convert all dashes/underscores into separator
    $flip = $separator == '-' ? '_' : '-';
    $title = preg_replace('![' . preg_quote($flip) . ']+!u', $separator, $title);
    // Remove all characters that are not the separator, letters, numbers, or whitespace.
    $title = preg_replace('![^' . preg_quote($separator) . '\pL\pN\s]+!u', '', mb_strtolower($title));
    // Replace all separator characters and whitespace by a single separator
    $title = preg_replace('![' . preg_quote($separator) . '\s]+!u', $separator, $title);
    return trim($title, $separator);
}

/**
 * Check if in production based on domains.
 *
 * @return bool
 */
function inProduction()
{
    $productionDomains = ['axicom.com', 'www.axicom.com'];
    if (!isset($_SERVER['HTTP_HOST']) || !in_array($_SERVER['HTTP_HOST'], $productionDomains)) {
        return false;
    }
    return true;
}


function endsWith($haystack, $needle)
{
    $length = strlen($needle);

    return $length === 0 ||
        (substr($haystack, -$length) === $needle);
}

/**
 *
 * quick fix, check if the current page is in local home page
 */
function isLocalHomePage() {

    global $homePageUris;

    $parts = explode('?', $_SERVER['REQUEST_URI']);
    $checkUrl = $parts[0];


    foreach($homePageUris as $uri) {
        if (endsWith($checkUrl, $uri)) {
            return true;
        }
    }
    return false;
}

function getOgImageTag() {
    global $post;

    if (is_single() && $post->post_type === 'news') {

        $imageFullSizeUrl = get_field('image', $post->ID);
        $imageUrl = Image::getSize($imageFullSizeUrl, 'medium_large');
        $imageMeta = Image::getSizeMeta($imageFullSizeUrl, 'medium_large');

        if (!$imageUrl) {
            return '';
        }

        $metaTags = [
            '<meta property="og:image" content="' . htmlentities($imageUrl) . '" />',
        ];

        if (isset($imageMeta['width']) && $imageMeta['width']) {
            $metaTags[] = '<meta property="og:image:width" content="' . htmlentities($imageMeta['width']) . '" />';
        }

        if (isset($imageMeta['height']) && $imageMeta['height']) {
            $metaTags[] = '<meta property="og:image:height" content="' . htmlentities($imageMeta['height']) . '" />';
        }

        return implode("\n", $metaTags);
    } else if (is_single() && $post->post_type === 'work') {


        $imageUrl = asset('axicom-icon-large.png');

        if(has_post_thumbnail( $post->ID )){
            $imageUrl = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' )[0];
        }

        $metaTags = [
            '<meta property="og:image" content="' . $imageUrl . '" />',
            '<meta property="og:image:width" content="134" />',
            '<meta property="og:image:height" content="123" />',
            '<meta name="twitter:image" content="' . $imageUrl . '" />',
        ];
    } else {

        $imageUrl = asset('axicom-icon-large.png');

        $metaTags = [
            '<meta property="og:image" content="' . $imageUrl . '" />',
            '<meta property="og:image:width" content="134" />',
            '<meta property="og:image:height" content="123" />',
            '<meta name="twitter:image" content="' . $imageUrl . '" />',
        ];

    }

    return implode("\n", $metaTags);

}

function customTitle($sep) {
    $title = wp_title($sep, false);
    return strpos($title, 'AxiCom ') === 0 ? $title : 'AxiCom ' . $title;
}

function getHomePage() {

    global $blog_id;
    global $homePageIdMap;

    $locale = get_locale();

    $homePageId = $homePageIdMap["{$blog_id}|{$locale}"];

    $post = get_post($homePageId);

    return !is_null($post) ? $post : null;

}