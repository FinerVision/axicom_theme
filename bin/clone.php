#!/usr/bin/env php
<?php

use Ifsnop\Mysqldump\Mysqldump;

require_once __DIR__ . '/../cli/bootstrap.php';

$directory = $argv[1];

if (is_null($directory)) {
    echo "You need to specify a directory path to download into.\n";
    exit(1);
}

if (!file_exists($directory)) {
    echo "Directory '{$directory}' does not exist.\n";
    exit(1);
}

$directory = rtrim($directory, '/');
$dbHost = getenv('REMOTE_DB_HOST');
$dbDatabase = getenv('REMOTE_DB_DATABASE');
$dbUsername = getenv('REMOTE_DB_USERNAME');
$dbPassword = getenv('REMOTE_DB_PASSWORD');
$rewriteBase = getenv('REWRITE_BASE');
$fileNamePrefix = 'axicom_ms';

// Download and unzip
echo "Downloading WordPress files. The will take a few moments.\n";

Zip::get("https://apps.finervision.com/{$fileNamePrefix}.zip", "{$directory}/{$fileNamePrefix}.zip");
$extracted = Zip::open("{$fileNamePrefix}.zip", $directory);

$htaccess = file_get_contents("{$directory}/{$fileNamePrefix}/wp-content/themes/axicom/data/htaccess.txt");
$htaccess = str_replace(':rewrite_base:', $rewriteBase, $htaccess);
file_put_contents("{$directory}/{$fileNamePrefix}/.htaccess", $htaccess);

if (!$extracted) {
    echo "Failed to extract zip file axicom_ms.zip\n";
    exit(1);
}

echo "{$fileNamePrefix}.zip downloaded and extracted to {$directory}/{$fileNamePrefix}.\n\n";

// Download SQL dump
echo "Downloading the SQL file. The will take a few moments.\n";

try {
    $dump = new Mysqldump("mysql:host={$dbHost};dbname={$dbDatabase}", $dbUsername, $dbPassword);
    $dump->start("{$directory}/{$fileNamePrefix}.sql");
} catch (\Exception $e) {
    echo 'mysqldump-php error: ' . $e->getMessage();
}

echo "{$fileNamePrefix}.sql downloaded to {$directory}/{$fileNamePrefix}.sql.\n\n";

shell_exec("./bin/sql.php {$directory}");

// Show user manual instructions
echo "Next steps:\n";
echo "• Create a DB called 'axicom_ms' with an encoding of 'UTF-8 Unicode utf8mb4'\n";
echo "• Update your wp-config.php file and change the 'DB_*', 'DOMAIN_CURRENT_SITE' and 'PATH_CURRENT_SITE' defines.\n";
echo "• Import 'axicom_ms.sql' file into your DB\n";
echo "• Run the 'axicom_ms_update.sql' script\n";
