#!/usr/bin/env php
<?php

use Ifsnop\Mysqldump\Mysqldump;

require_once __DIR__ . '/../cli/bootstrap.php';

$directory = $argv[1];

if (is_null($directory)) {
    echo "You need to specify a directory path to download into.\n";
    exit(1);
}

if (!file_exists($directory)) {
    echo "Directory '{$directory}' does not exist.\n";
    exit(1);
}

$dbHost = getenv('REMOTE_DB_HOST');
$dbDatabase = getenv('REMOTE_DB_DATABASE');
$dbUsername = getenv('REMOTE_DB_USERNAME');
$dbPassword = getenv('REMOTE_DB_PASSWORD');

// Download SQL dump
echo "Downloading the SQL file. The will take a while.\n";

try {
    $dump = new Mysqldump("mysql:host={$dbHost};dbname={$dbDatabase}", $dbUsername, $dbPassword);
    $dump->start("{$directory}/{$dbDatabase}.sql");
} catch (\Exception $e) {
    echo 'mysqldump-php error: ' . $e->getMessage();
}

echo "{$dbDatabase}.sql downloaded to {$directory}/{$dbDatabase}.sql.\n\n";

shell_exec("./bin/sql.php {$directory}");
echo "• Run the '{$dbDatabase}_update.sql' script\n";
