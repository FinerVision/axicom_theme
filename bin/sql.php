#!/usr/bin/env php
<?php

require_once __DIR__ . '/../cli/bootstrap.php';

$directory = $argv[1];

if (is_null($directory)) {
    echo "You need to specify a directory path to download into.\n";
    exit(1);
}

if (!file_exists($directory)) {
    echo "Directory '{$directory}' does not exist.\n";
    exit(1);
}

$directory = rtrim($directory, '/');
$dbDatabase = getenv('DB_DATABASE');
$protocol = getenv('DOMAIN_PROTOCOL');
$domain = getenv('DOMAIN_NAME');
$path = getenv('DOMAIN_PATH');
$host = $protocol . $domain . $path;
$originalDomain = 'apps.finervision.com';
$originalUrl = "https://{$originalDomain}/axicom_ms";
$updateOptionsSql = '';
$updatePostmetaSql = '';
$updatePostsSql = '';

for ($i = 1; $i < 8; $i++) {
    $number = $i + 1;
    $tablePrefix = "wp_{$number}";
    $updateOptionsSql .= "update `{$tablePrefix}_options` set `option_value` = replace(`option_value`, '{$originalUrl}', '{$host}') where `option_name` in ('siteurl', 'home', '_transient_pll_languages_list');\n";
    $updatePostmetaSql .= "update `{$tablePrefix}_postmeta` set `meta_value` = replace(`meta_value`, '{$originalUrl}', '{$host}');\n";
    $updatePostsSql .= "update `{$tablePrefix}_posts` set `post_content` = replace(`post_content`, '{$originalUrl}', '{$host}'), `guid` = replace(`guid`, '{$originalUrl}', '{$host}');\n";
}

$sql = <<<EOF
{$updateOptionsSql}
{$updatePostmetaSql}
{$updatePostsSql}
update `wp_options` set `option_value` = replace(`option_value`, '{$originalUrl}', '{$host}') where `option_name` in ('siteurl', 'home', '_transient_pll_languages_list');
update `wp_postmeta` set `meta_value` = replace(`meta_value`, '{$originalUrl}', '{$host}');
update `wp_posts` set `post_content` = replace(`post_content`, '{$originalUrl}', '{$host}'), `guid` = replace(`guid`, '{$originalUrl}', '{$host}');
update `wp_site` set `domain` = replace(`domain`, '{$originalDomain}', '{$domain}'), `path` = replace(`path`, '/axicom_ms/', '{$path}');
update `wp_usermeta` set `meta_value` = replace(`meta_value`, '{$originalDomain}', '{$domain}') where `meta_key` = 'meta_value';
update `wp_sitemeta` set `meta_value` = replace(`meta_value`, '{$originalUrl}', '{$host}') where `meta_key` = 'siteurl';
update `wp_blogs` set `domain` = replace(`domain`, '{$originalDomain}', '{$domain}'), `path` = replace(`path`, '{$path}', '{$path}');
EOF;

file_put_contents("{$directory}/{$dbDatabase}_update.sql", $sql);

echo "SQL update script saved to '{$directory}/{$dbDatabase}_update.sql'\n";
