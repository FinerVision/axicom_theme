#!/usr/bin/env bash

if [[ -z $1 ]]
then
    echo "No download directory given. Specify a download directory to download the uploads.zip file to."
    exit 1
fi

# Remove trailing slash
directory=$(sed "s/\/$//" <<< $1)

echo "Zipping uploads directory. This may take a while."
echo ""

ssh -i ~/.ssh/samantha.pem ubuntu@52.38.225.26 "cd /var/www/apps/public/axicom_ms/wp-content/; zip -r uploads.zip uploads >/dev/null" >/dev/null

echo "Downloading uploads.zip to $directory/uploads.zip"
echo ""

curl https://apps.finervision.com/axicom_ms/wp-content/uploads.zip > "$directory/uploads.zip"

echo "Cleaning up..."
echo ""

ssh -i ~/.ssh/samantha.pem ubuntu@52.38.225.26 "cd /var/www/apps/public/axicom_ms/wp-content/; rm -rf uploads.zip >/dev/null" >/dev/null
