<?php

global $config;

$config = [
    'weather' => 'd34c191394f6f7ce0fcf7c7a4d650ff2'
];

for ($i = 1; $i <= 8; $i++) {
    add_blog_option($i, 'CountrySlug', '');
    add_blog_option($i, 'CountryCity', '');
}

function getConfig($name)
{
    global $config;
    return isset($config[$name]) ? $config[$name] : null;
}

if (!function_exists('dd')) {
    function dd()
    {
        echo '<pre>';
        foreach (func_get_args() as $arg) {
            var_dump($arg);
        }
        echo '</pre>';
        exit;
    }
}

////////////////////////////////////////////////////////////////////////
/// POST TYPES
////////////////////////////////////////////////////////////////////////
include __DIR__ . '/post_types/common.php';

////////////////////////////////////////////////////////////////////////
/// SHORTCODES
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
/// THEME SUPPORT
////////////////////////////////////////////////////////////////////////
@ini_set('upload_max_size', '64M');
@ini_set('post_max_size', '64M');
@ini_set('max_execution_time', '300');

function register_main_menu()
{
    register_nav_menu('main-menu', __('Main Menu'));
    register_nav_menu('global-menu', __('Global Menu'));
}

add_action('init', 'register_main_menu');
//add_filter( 'pre_option_link_manager_enabled', '__return_true' );

////////////////////////////////////////////////////////////////////////
/// SPECIAL SETTINGS
////////////////////////////////////////////////////////////////////////

// disable admin bar for all users (on preview)
show_admin_bar(false);

// disable automatic updates in wp-config.php
// define( 'AUTOMATIC_UPDATER_DISABLED', true );

// disable auto updates for plugins
add_filter('auto_update_plugin', '__return_false');

// disable auto updates for themes
add_filter('auto_update_theme', '__return_false');

// disable menu points
function remove_menus()
{
    $user = wp_get_current_user();

    if (!in_array('finervision', (array)$user->data->user_login)) {
        remove_menu_page('edit.php');
        remove_menu_page('themes.php');
        remove_menu_page('options-general.php');
        remove_menu_page('tools.php');
        remove_menu_page('edit-comments.php');
        remove_menu_page('plugins.php');
        remove_menu_page('edit.php?post_type=acf-field-group');
    }

    if (!in_array('administrator', (array)$user->roles)
        && (in_array('editor', (array)$user->roles) || in_array('contributor', (array)$user->roles))
    ) {
        remove_menu_page('edit.php?post_type=office');
        remove_menu_page('edit.php?post_type=person');
        remove_menu_page('edit.php?post_type=service');
        remove_menu_page('edit.php?post_type=expertise');
        remove_menu_page('edit.php?post_type=resource');
        remove_menu_page('edit.php?post_type=page');
        remove_menu_page('edit.php?post_type=work');
//        remove_menu_page( 'upload.php' );
        remove_menu_page('index.php');
    }
}


///*** Remove Default Post Type ***/

add_filter('register_post_type_args', function($args, $postType){
    if ($postType === 'post') {
        $args['capabilities'] = [
            'edit_post' => false,
            'read_post' => false,
            'delete_post' => false,
            'edit_posts' => false,
            'edit_others_posts' => false,
            'publish_posts' => false,
            'read' => false,
            'delete_posts' => false,
            'delete_private_posts' => false,
            'delete_published_posts' => false,
            'delete_others_posts' => false,
            'edit_private_posts' => false,
            'edit_published_posts' => false,
            'create_posts' => false,
        ];
    }

    return $args;
}, 0, 2);

function remove_default_post_type($args, $postType) {
    if ($postType === 'post') {
        $args['public']                = false;
        $args['show_ui']               = false;
        $args['show_in_menu']          = false;
        $args['show_in_admin_bar']     = false;
        $args['show_in_nav_menus']     = false;
        $args['can_export']            = false;
        $args['has_archive']           = false;
        $args['exclude_from_search']   = true;
        $args['publicly_queryable']    = false;
        $args['show_in_rest']          = false;
    }

    return $args;
}
add_filter('register_post_type_args', 'remove_default_post_type', 0, 2);

function dashboard_redirect($url)
{
    $user = wp_get_current_user();

    if (!in_array('administrator', (array)$user->roles)
        && (in_array('editor', (array)$user->roles) || in_array('contributor', (array)$user->roles))
    ) {
        $url = 'wp-admin/edit.php?post_type=news';
    }
    return $url;
}

add_filter('login_redirect', 'dashboard_redirect');

function remove_admin_bar_links()
{
    global $wp_admin_bar;

    $user = wp_get_current_user();

    $wp_admin_bar->remove_menu('new-post');

    if (!in_array('administrator', (array)$user->roles)
        && (in_array('editor', (array)$user->roles) || in_array('contributor', (array)$user->roles))
    ) {
        $wp_admin_bar->remove_node('wp-logo');
        $wp_admin_bar->remove_node('my-sites');
        $wp_admin_bar->remove_node('comments');
        $wp_admin_bar->remove_node('new-content');
        $wp_admin_bar->remove_node('wpseo-menu');
    }
}

add_action('wp_before_admin_bar_render', 'remove_admin_bar_links');
add_action('admin_menu', 'remove_menus');

// change login image and design
function my_login_logo_one()
{
    ?>
    <style type="text/css">
        body.login {
            background-color: #33c5ac;
        }

        body #login {
            padding-top: 15%;
        }

        body.login #loginform {
            margin-top: 0;
        }

        body.login form {
            border-radius: 12px;
        }

        body.login div#login h1 a {
            background-size: 100%;
            width: 100%;
            background-image: url('<?= get_bloginfo('template_url') ?>/login_logo.png');
            margin: 0;
        }

        body.login div#login h1 {
            margin: 0;
        }

        body.login p#backtoblog {
            display: none;
        }
    </style>
    <?php
}

add_action('login_enqueue_scripts', 'my_login_logo_one');

function change_howdy($translated, $text, $domain)
{

    if (!is_admin() || 'default' != $domain)
        return $translated;

    if (false !== strpos($translated, 'Hi'))
        return str_replace('Hi,', 'Welcome,', $translated);

    if (false !== strpos($translated, 'Howdy'))
        return str_replace('Howdy,', 'Welcome,', $translated);

    return $translated;
}

// Replace Howdy text
add_filter('gettext', 'change_howdy', 10, 3);

// remove useless dashboard elements

function remove_dashboard_meta()
{
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
    remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
    remove_meta_box('dashboard_primary', 'dashboard', 'side');
    remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
//    remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
    remove_meta_box('dashboard_activity', 'dashboard', 'normal');
}

add_action('admin_init', 'remove_dashboard_meta');

/////////////////////////////////////////////////////////////////////////
/// CACHE
////////////////////////////////////////////////////////////////////////
function createCacheTable()
{
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    global $wpdb;

    $tableName = $wpdb->prefix . "fv_cached";

    $sql = "CREATE TABLE IF NOT EXISTS $tableName (
		id int NOT NULL AUTO_INCREMENT, 
		url TEXT,
		response TEXT, 
		created_at TIMESTAMP,
		PRIMARY KEY (id) 
	)";

    dbDelta($sql);
}

function getCachedRequest($url, $expiredInMin)
{
    global $wpdb;
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    $tableName = $wpdb->prefix . "fv_cached";
    $sql = "SELECT * FROM {$tableName} WHERE url='{$url}' AND created_at > NOW() - INTERVAL {$expiredInMin} MINUTE limit 1";
    $result = $wpdb->get_row($sql);

    if (!$result) {
        $wpdb->delete($tableName, ['url' => $url]);

        $json = file_get_contents($url);
        $wpdb->insert($tableName, [
            'url' => $url,
            'response' => $json,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        $data = $json;
    } else {
        $data = $result->response;
    }

    return json_decode($data, true);
}

/**
 *
 * Fetch a single word from the custom field, show error log if the content has not been stored
 *
 * @param $key
 * @param null $fallbackWord
 * @return string|null
 *
 */
function cf($key, $fallbackWord = null)
{
    $value = Field::get($key);
    $errorMessage = null;

    if (is_array($value)) {
        $stackTrace = current(debug_backtrace());
        $errorMessage = "Warning : translation should be string text instead of array, key'{$key}' ({$stackTrace['file']}:{$stackTrace['line']})";
    }
    if ($value === null) {
        $stackTrace = current(debug_backtrace());
        $errorMessage = "Translation not found with key'{$key}' ({$stackTrace['file']}:{$stackTrace['line']})";
    }

    if ($errorMessage !== null) {
        error_log($errorMessage);
        return $fallbackWord;
    } else {
        return $value;
    }
}

function echoCF($key, $fallbackWord = null)
{
    $value = Field::get($key);
    $errorMessage = null;

    if (is_array($value)) {
        $stackTrace = current(debug_backtrace());
        $errorMessage = "Warning : translation should be string text instead of array, key'{$key}' ({$stackTrace['file']}:{$stackTrace['line']})";
    }
    if ($value === null) {
        $stackTrace = current(debug_backtrace());
        $errorMessage = "Translation not found with key'{$key}' ({$stackTrace['file']}:{$stackTrace['line']})";
    }

    if ($errorMessage !== null) {
        error_log($errorMessage);
        echo $fallbackWord;
    } else {
        echo $value;
    }
}

add_action('init', 'createCacheTable');

require_once __DIR__ . '/translations.php';
/////////////////////////////////////////////////////


add_filter('title_save_pre', 'updateSavePostTitle');

function updateSavePostTitle($title)
{

    if ($_POST['post_type'] == 'work') {

        $currentLanguage = pll_current_language();
        $defaultLanguage = pll_default_language();

        if ($currentLanguage != $defaultLanguage) {
            $title .= ' - en';
            $title = str_replace(' - en - en', ' - en', $title);
        }
    }

    return $title;
}

if ( $GLOBALS['pagenow'] === 'wp-login.php' ) {

    ob_start();
}

add_action('login_form', function($args) {
    $login = ob_get_contents();
    ob_clean();
    $login = str_replace('id="user_pass"', 'id="user_pass" autocomplete="off"', $login);
    echo $login;
}, 9999);

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
}

add_filter( 'query_vars', 'wpa66452_query_vars' );
function wpa66452_query_vars( $query_vars ){
    $query_vars[] = 'news_search';
    return $query_vars;
}

function theme_xyz_header_metadata() {
    if($_SERVER['SERVER_NAME'] == 'axicom.local'){
        ?>
        <!-- OneTrust Cookies Consent Notice start for axicom.com -->
        <script type="text/javascript" src=https://cdn.cookielaw.org/consent/7734adf4-62ae-4f1d-a309-04df36e593a0-test/OtAutoBlock.js ></script>
        <script src=https://cdn.cookielaw.org/scripttemplates/otSDKStub.js type="text/javascript" charset="UTF-8" data-domain-script="7734adf4-62ae-4f1d-a309-04df36e593a0-test" ></script>
        <script type="text/javascript">
            function OptanonWrapper() { }
        </script>
        <!-- OneTrust Cookies Consent Notice end for axicom.com -->
        <?php
    }else{
        ?>

        <!-- OneTrust Cookies Consent Notice start for axicom.com -->
        <script type="text/javascript" src=https://cdn.cookielaw.org/consent/7734adf4-62ae-4f1d-a309-04df36e593a0/OtAutoBlock.js ></script>
        <script src=https://cdn.cookielaw.org/scripttemplates/otSDKStub.js type="text/javascript" charset="UTF-8" data-domain-script="7734adf4-62ae-4f1d-a309-04df36e593a0" ></script>
        <script type="text/javascript">
            function OptanonWrapper() { }
        </script>
        <!-- OneTrust Cookies Consent Notice end for axicom.com -->

        <?php
    }

}
add_action( 'wp_head', 'theme_xyz_header_metadata' );

function wptexlnk_plugin_row_meta($links, $file) {
    if ( strpos( $file, 'extend-link.php' ) !== false ) {
        $new_links = array('');

        $links = array_merge( $links, $new_links );
    }
    return $links;
}
add_filter( 'plugin_row_meta', 'wptexlnk_plugin_row_meta', 10, 2 );


function wptexlnk_add_tinymce_button($buttons) {
    array_push($buttons, 'wptp_extend_link_tinymce');
    return $buttons;
}
add_filter('mce_buttons', 'wptexlnk_add_tinymce_button');


function wptexlnk_register_tinymce_js($plugin_array) {

    $plugin_array['wptp_extend_link_tinymce'] = get_template_directory_uri().'/public/js/extend-link-tinymce.js';
    return $plugin_array;
}
add_filter('mce_external_plugins', 'wptexlnk_register_tinymce_js');


function wptexlnk_tinymce_button_icon(){
    ?>
    <style type="text/css">
        .mce-i-extendlink-mce-icon:before{
            font-family: 'dashicons' !important;
            content: "\f111" !important;
            font-size: 20px !important;
            width: 20px !important;
            height: 20px !important;
        }

        .mce-i-extendlink-mce-icon{
            padding-right: 2px !important;
        }
    </style>
    <?php
}
add_action('admin_head', 'wptexlnk_tinymce_button_icon');